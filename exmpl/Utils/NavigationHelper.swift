//
//  NavigationHelper.swift
//  exmpl
//
//  Created by Prima Jatnika on 23/11/21.
//

import Foundation

class NavigationHelper: ObservableObject {
    @Published var selection: String? = nil
}
