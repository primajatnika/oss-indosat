//
//  NetworkError.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import Foundation

enum NetworkError: Error {
    case badUrl
    case decodingError
    case noData
    case notFound
}
