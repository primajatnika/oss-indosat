//
//  AppState.swift
//  exmpl
//
//  Created by Prima Jatnika on 11/11/21.
//

import Foundation
import SwiftUI

class AppState: ObservableObject {
    
    @Published var back2MainScreen: Bool = false
    @Published var back2Profil: Bool = false
    
    var navigationController: UINavigationController?
}
