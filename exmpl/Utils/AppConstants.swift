//
//  AppConstants.swift
//  exmpl
//
//  Created by Prima Jatnika on 11/11/21.
//

import Foundation

class AppConstants {
    
    var BASE_URL = "https://api-stg.oss.go.id"
    var BASE_URL_PRD = "https://api-prd-m.oss.go.id"
    
    var AUTHORIZATION = "Basic T1NTMDAwOlV4NEJYVkJXVzJWZmFGRXhVenRVWmxKakFHdFFPbEZx"
    var USERKEY = "f9c53f291ab3b47251ef5b001b4f6dcc"
}

struct defaultsKeys {
    static let keyToken = "access_token"
    static let keyUser = "user_key"
    static let keyIsLogin = "is_login"
    static let keyIsHighlight = "is_highlight"
    static let keySkipOnboarding = "is_skip_onboarding"
    static let keyPassword = "is_password"
}
