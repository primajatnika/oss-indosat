//
//  ContentView.swift
//  exmpl
//
//  Created by Prima Jatnika on 11/11/21.
//

import SwiftUI

struct ContentView: View {
    
    let appState = AppState()
    var defaults = UserDefaults.standard
    
    init() {
        let appearance = UINavigationBarAppearance()
        
        appearance.configureWithTransparentBackground()
        appearance.backgroundColor = #colorLiteral(red: 0.6078431373, green: 0.1215686275, blue: 0.08235294118, alpha: 1)
        
        appearance.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor : UIColor.white
        ]
        
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        UINavigationBar.appearance().standardAppearance = appearance
        
        UINavigationBar.appearance().tintColor = .white
    }
    
    var body: some View {
        NavigationView {
            
            if defaults.string(forKey: defaultsKeys.keySkipOnboarding) == "true" {
                DashboardScreen()
                    .environmentObject(appState)
                    .navigationBarHidden(true)
                    .edgesIgnoringSafeArea(.all)
                
            } else {
                MainScreen()
                    .environmentObject(appState)
                    .navigationBarHidden(true)
                    .edgesIgnoringSafeArea(.all)
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
        .addPartialSheet()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
