//
//  UIApplication+Extensions.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import Foundation
import SwiftUI

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
