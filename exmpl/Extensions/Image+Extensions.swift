//
//  Image+Extensions.swift
//  exmpl
//
//  Created by Prima Jatnika on 22/11/21.
//

import Foundation
import SwiftUI

extension Image {
    
    func data(url:URL) -> Self {
        
        if let data = try? Data(contentsOf: url) {
            
            return Image(uiImage: UIImage(data: data)!)
                
                .resizable()
            
        }
        
        return self
            
            .resizable()
        
    }
    
}
