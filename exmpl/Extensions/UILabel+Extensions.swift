//
//  UILabel+Extensions.swift
//  exmpl
//
//  Created by Prima Jatnika on 23/11/21.
//

import Foundation
import UIKit

extension UILabel {
    func setHighlighted(_ text: String, with search: String) {
        let attributedText = NSMutableAttributedString(string: text) // 1
        let range = NSString(string: text).range(of: search, options: .caseInsensitive) // 2
        let highlighColor = traitCollection.userInterfaceStyle == .light ? UIColor.systemYellow : UIColor.systemOrange // 3
        let highlightedAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.backgroundColor: highlighColor] // 4
        
        attributedText.addAttributes(highlightedAttributes, range: range) // 5
        self.attributedText = attributedText // 6
    }
}
