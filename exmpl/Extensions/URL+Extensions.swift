//
//  URL+Extensions.swift
//  exmpl
//
//  Created by Prima Jatnika on 11/11/21.
//

import Foundation

extension URL {
    
    static func urlAuthLogin() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/sso/users/login")
    }
    
    static func urlAllFaq() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/faq/data-faq-all/01")
    }
    
    static func urlKategoriFaq() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/faq/kategori")
    }
    
    static func urlAllKbli() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/informasi/kbli")
    }
    
    static func urlSearchKbli() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/informasi/kbli/search")
    }
    
    static func urlKbliUmku() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/informasi/kbli-umku")
    }
    
    static func urlKbliResiko() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/informasi/kbli-resiko")
    }
    
    static func urlAllPanduan() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/informasi/panduan/data")
    }
    
    static func urlVideoBanner() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/banner/data")
    }
    
    static func urlSendRatting() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/faq/rating")
    }
    
    static func urlSendAudit() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/portal/faq/insert-audit")
    }
    
    static func urlGetOtp() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/reg/pelaku_usaha/send_otp")
    }
    
    static func urlVerifOtp() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/reg/pelaku_usaha/verify_otp")
    }
    
    static func urlGetProvince() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/ref/region/list/provinsi")
    }
    
    static func urlGetKabupatenKota() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/ref/region/list/kota")
    }
    
    static func urlGetKecamatan() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/ref/region/list/kecamatan")
    }
    
    static func urlGetKelurahan() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/ref/region/list/kelurahan")
    }
    
    static func urlRegisterPelakuUsaha() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/reg/pelaku_usaha")
    }
    
    static func urlProfil() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/sso/users/userinfo-token")
    }
    
    static func urlUbahProfil() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/reg/profile/ubah_profile")
    }
    
    static func urlUbahPassword() -> URL? {
        return URL(string: AppConstants().BASE_URL + "/stg/v1/reg/profile/ubah_password")
    }
}

extension URL {
    
    // MARK: Add Query Param
    func appending(_ queryItem: String, value: String?) -> URL {

        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }

        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        let queryItem = URLQueryItem(name: queryItem, value: value)
        queryItems.append(queryItem)
        urlComponents.queryItems = queryItems

        return urlComponents.url!
    }
}
