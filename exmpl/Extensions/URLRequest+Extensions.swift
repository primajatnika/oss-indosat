//
//  URLRequest+Extensions.swift
//  exmpl
//
//  Created by Prima Jatnika on 11/11/21.
//

import Foundation

extension URLRequest {
    
    init(_ url: URL) {
        let defaults = UserDefaults.standard
        
        self.init(url: url)
        self.timeoutInterval = 60
        self.setValue("*/*", forHTTPHeaderField: "accept")
        self.setValue("application/json", forHTTPHeaderField: "Content-Type")
        self.setValue(defaults.string(forKey: defaultsKeys.keyToken), forHTTPHeaderField: "Authorization")
        self.setValue(defaults.string(forKey: defaultsKeys.keyUser), forHTTPHeaderField: "user_key")
    }
}
