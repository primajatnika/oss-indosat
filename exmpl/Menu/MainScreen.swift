//
//  MainScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 11/11/21.
//

import SwiftUI

struct MainScreen: View {
    
    var defaults = UserDefaults.standard
    @State private var currentTab: Int = 0
    @State private var title: String = ""
    
    @State private var skipRoute: Bool = false
    
    // Offset
    @State private var offset: CGFloat = 0
    
    var body: some View {
        
        let onboardingItem: [AnyView] = [
            AnyView(OnboardingOne(onNext: { next(page: 1) })),
            AnyView(OnboardingTwo(onNext: { next(page: 2) }, onPrev: { prev(page: 0) })),
            AnyView(OnboardingThree(onNext: { next(page: 3) }, onPrev: { prev(page: 1) })),
            AnyView(OnboardingFour(onNext: { next(page: 4) }, onPrev: { prev(page: 2) })),
            AnyView(OnboardingFive(onPrev: { prev(page: 3) }))
        ]
        
        VStack(alignment: .leading) {
            
            // Navigation Link
            NavigationLink(
                destination: switchPage(title: title),
                isActive: self.$skipRoute,
                label: {})
            
            HStack {
                Spacer()
                if (currentTab == 4) {
                    
                    Button(action: {
                        skipBtn()
                    }, label: {
                        Text("Lewati")
                            .foregroundColor(Color("gray"))
                    })
                    
                } else {
                    
                    Button(action: {}, label: {
                        Text("Lewati")
                            .foregroundColor(Color("gray"))
                    })
                    .hidden()
                    
                }
            }
            .padding(.horizontal, 20)
            .padding(.top, 60)
            
            TabView(selection: $currentTab) {
                ForEach(onboardingItem.indices, id: \.self) { index in
                    onboardingItem[index]
                        .tag(index)
                        .animation(.spring())
                }
            }
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
            .overlay(
                HStack(spacing: 15) {
                    ForEach(onboardingItem.indices, id: \.self) { index in
                        Capsule()
                            .fill(Color(getIndex() == index ? "red" : "light_gray"))
                            .frame(width: getIndex() == index ? 20 : 7, height: 7)
                            .animation(.spring())
                    }
                }
                ,alignment: .bottom
            )
            
            
            if (currentTab == 4) {
                HStack {
                    // Button Daftar
                    Button(
                        action: {
                            self.title = "Daftar"
                            self.skipRoute = true
                        },
                        label: {
                            Text("Daftar")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(.white)
                                .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                            
                        })
                        .background(Color("blue"))
                        .cornerRadius(10)
                        .padding(.top, 10)
                        .padding(.bottom, 10)
                        .padding(.leading, 20)
                        .padding(.trailing, 10)
                    
                    // Button Masuk
                    Button(
                        action: {
                            self.title = "Masuk"
                            self.skipRoute = true
                        },
                        label: {
                            Text("Masuk")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(Color("gray"))
                                .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                        })
                        .background(Color("light_gray"))
                        .cornerRadius(10)
                        .padding(.top, 10)
                        .padding(.bottom, 10)
                        .padding(.trailing, 20)
                        .padding(.leading, 10)
                    
                }
                .padding(.top, 30)
                .padding(.bottom, 15)
            } else {
                HStack {
                    // Button Daftar
                    Button(action: {}, label: {
                        Text("Masuk")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                        
                    })
                    .cornerRadius(10)
                    .background(Color(.systemBlue))
                    .padding(.horizontal, 20)
                    .padding(.top, 10)
                    .padding(.bottom, 10)
                }
                .padding(.bottom, 15)
                .padding(.top, 30)
                .hidden()
            }
        }
        .navigationBarHidden(true)
        .navigationBarTitle(title)
        .onAppear() {
            setupAppearance()
        }
    }
}

struct MainScreen_Previews: PreviewProvider {
    static var previews: some View {
        MainScreen()
    }
}

extension MainScreen {
    
    func switchPage(title: String) -> AnyView {
        switch title {
        case "Daftar":
            return AnyView(DaftarScreen())
        case "Masuk":
            return AnyView(LoginScreen(isFromRegister: .constant(false)))
        case "Dashboard":
            return AnyView(DashboardScreen())
        default:
            return AnyView(EmptyView())
        }
        
        return AnyView(EmptyView())
    }
    
    func getIndex() -> Int {
        let index = currentTab
        
        print(index)
        return index
    }
    
    func getOffset() -> CGFloat {
        let progress = offset / getWidth()
        return 22 * progress
    }
    
    func setupAppearance() {
        UIPageControl.appearance().currentPageIndicatorTintColor = #colorLiteral(red: 0.6078431373, green: 0.1215686275, blue: 0.08235294118, alpha: 1)
        UIPageControl.appearance().pageIndicatorTintColor = UIColor.black.withAlphaComponent(0.2)
    }
    
    func skipBtn() {
        self.defaults.set(false, forKey: defaultsKeys.keyIsLogin)
        self.defaults.set("true", forKey: defaultsKeys.keySkipOnboarding)
        self.title = "Dashboard"
        self.skipRoute = true
    }
    
    func next(page: Int) {
        self.currentTab = page
    }
    
    func prev(page: Int) {
        self.currentTab = page
    }
    
}

extension View {
    func getWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
}
