//
//  OnboardingTwo.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI

struct OnboardingTwo: View {
    
    let onNext: ()-> Void
    let onPrev: ()-> Void
    
    var body: some View {
        HStack {
            
            Button(action: onPrev, label: {
                Image(systemName: "chevron.left")
                    .padding(.leading, 15)
                    .foregroundColor(Color("gray"))
            })
            
            VStack(spacing: 20) {
                
                Image("slide2")
                
                Text("Mengurus Nomor Induk Berusaha (NIB)\nMudah dan Cepat")
                    .font(.title)
                    .foregroundColor(Color("red"))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 20)
                
                Text("Yuk proses perizinan berusaha Anda sampai terbit NIB hanya\ndalam hitungan menit.")
                    .font(.subheadline)
                    .foregroundColor(Color("gray"))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 20)
                
            }
            
            Button(action: onNext, label: {
                Image(systemName: "chevron.right")
                    .padding(.trailing, 15)
                    .foregroundColor(Color("gray"))
            })
        }
    }
}

struct OnboardingTwo_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingTwo(onNext: {}, onPrev: {})
    }
}
