//
//  ProfilViewModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 19/11/21.
//

import Foundation

class ProfilViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    @Published var message: String = ""
    
    @Published var idProfile: Int = 0
    @Published var noTeleponCtrl: String = ""
    @Published var emailCtrl: String = ""
    @Published var passwordCtrl: String = ""
    @Published var nikCtrl: String = ""
    @Published var namaLengkapCtrl: String = ""
    @Published var tanggalLahirCtrl: String = ""
    @Published var jenisKelaminCtrl: String = ""
    @Published var alamatCtrl: String = ""
    @Published var provinsiCtrl: String = ""
    @Published var kabupatenCtrl: String = ""
    @Published var kecamatanCtrl: String = ""
    @Published var kelurahanCtrl: String = ""
}

extension ProfilViewModel {
    
    func profil(completion: @escaping (Bool) -> Void) {
        
        ProfilService.shared.getProfil { result in
            switch result {
            case .success(let response):
                
                if (response.status == 200) {
                    DispatchQueue.main.async {
                        self.isLoading = false
                    }
                    
                    let noTelepon = response.data.telp ?? ""
                    
                    if let range = noTelepon.range(of: "+62") {
                        let id = noTelepon[range.upperBound...]
                        print(id)
                        self.noTeleponCtrl = String(id)
                    }
                    
                    self.idProfile = response.data.idProfile ?? 0
                    self.nikCtrl = response.data.nomorIdentitas ?? ""
                    self.emailCtrl = response.data.email ?? ""
                    self.passwordCtrl = ""
                    self.namaLengkapCtrl = response.data.nama ?? ""
                    self.tanggalLahirCtrl = ""
                    self.jenisKelaminCtrl = response.data.jenisIdentitas ?? ""
                    self.alamatCtrl = response.data.alamat ?? ""
                    self.provinsiCtrl = ""
                    self.kabupatenCtrl = ""
                    self.kecamatanCtrl = ""
                    self.kelurahanCtrl = ""
                    
                    completion(true)
                } else {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.showAlert = true
                        self.message = response.message
                    }
                    
                    completion(false)
                }
                
                break
            
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
            
            }
        }
    }
    
    func ubahProfil(email: String, noTelepon: String, idProfil: Int, completion: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            self.isLoading = true
        }
        
        ProfilService.shared.ubahProfil(email: email, noTelepon: noTelepon, idProfil: idProfil) { result in
            switch result {
            case .success(let response):
                
                if (response.kode == 200) {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.showAlert = true
                        self.message = response.desc ?? ""
                    }
                    
                    completion(true)
                } else {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.showAlert = true
                        self.message = response.desc ?? ""
                    }
                    
                    completion(false)
                }
                
                break
            
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
            
            }
        }
    }
    
    func ubahPassword(oldPassword: String, password: String, idProfil: Int, completion: @escaping (Bool) -> Void) {
        
        ProfilService.shared.ubahPassword(oldPassword: oldPassword, password: password, idProfil: idProfil) { result in
            switch result {
            case .success(let response):
                
                if (response.kode == 200) {
                    DispatchQueue.main.async {
                        self.isLoading = false
                    }
                    
                    completion(true)
                } else {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.showAlert = true
                        self.message = response.keterangan ?? ""
                    }
                    
                    completion(false)
                }
                
                break
            
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
            
            }
        }
    }
}
