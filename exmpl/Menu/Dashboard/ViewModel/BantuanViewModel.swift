//
//  BantuanViewModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import Foundation
import Combine

class BantuanViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    @Published var message: String = ""
    
    @Published var uraian: String = ""
    @Published var noTiket: Int = 0
    @Published var faqList = [Datum]()
    @Published var kategoriFaqList = [ListKategoriFAQResponseModelElement]()
    @Published var kbliList = [KbliModel]()
    @Published var kbliListSearch = [KbliModel]()
    @Published var kbliParentList = [KbliParentModel]()
    @Published var kbliUmkuList = [KbliUmku]()
    @Published var kbliInfoKkSyarat = [KbliKkSyarat]()
    @Published var kbliResikoList = [KbliResiko]()
}

extension BantuanViewModel {
    
    func faq(searchParam: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BantuanService.shared.postAllFaq(search: searchParam) { result in
            
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.faqList = response.data.filter({ (data: Datum) -> Bool in
                    
                    if searchParam.isEmpty {
                        self.faqList = response.data
                        return true
                    }
                    
                    return data.parameter.localizedStandardContains(searchParam)
                })
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
    
    func kategoriFaq(completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BantuanService.shared.getKategoriFaq { result in
            
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.kategoriFaqList = response
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
    
    func kbli(searchParam: String, kategori: String, kode: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BantuanService.shared.postAllKbli(search: searchParam, kategori: kategori, kode: kode) { result in
            
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.kbliList = response.data
                self.kbliParentList = response.parent ?? []
                self.uraian = response.deskripsi ?? ""
                self.kbliInfoKkSyarat = response.info?.kbliKkSyarat ?? []
                
                if (!searchParam.isEmpty) {
                    self.kbliList = response.data.filter({ (data: KbliModel) -> Bool in
                        return data.kode.localizedStandardContains(searchParam) || data.judul.localizedStandardContains(searchParam)
                    })
                }
                
                if (!kategori.isEmpty) {
                    self.kbliList = response.data.filter({ (data: KbliModel) -> Bool in
                        return data.kategori.localizedStandardContains(kategori)
                    })
                }
                
                if (!kode.isEmpty) {
                    self.kbliList = response.data.filter({ (data: KbliModel) -> Bool in
                        return data.kode.localizedStandardContains(kode)
                    })
                }
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
    
    func kbliSearch(searchParam: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BantuanService.shared.postAllKbli(search: searchParam, kategori: "", kode: "") { result in
            
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.kbliListSearch = response.data.filter({ (data: KbliModel) -> Bool in
                    return data.kode.localizedStandardContains(searchParam) || data.judul.localizedStandardContains(searchParam)
                })
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
    
    func kbliUmku(searchParam: String, kbli: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BantuanService.shared.postKbliUmku(search: searchParam, kbli: kbli) { result in
            
            switch result {
            case .success(let response):
                print("GET KBLI UMKU SUCCESS")
                print(response.data.count)
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.kbliUmkuList = response.data
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
    
    func kbliResiko(kbli: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BantuanService.shared.postKbliResiko(kbli: kbli) { result in
            
            switch result {
            case .success(let response):
                print("GET KBLI RESIKO SUCCESS")
                print(response.data.count)
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.kbliResikoList = response.data
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
    
    func sendRating(tanggal: String, idFaq: Int, bintang: Int, idFeedback: Int, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BantuanService.shared.postRatingFaq(
            tanggal: tanggal,
            idFaq: idFaq,
            bintang: bintang,
            idFeedback: idFeedback) { result in
            
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.showAlert = true
                self.message = response.message
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
    
    func sendAudit(idFaq: Int, email: String, nama: String, nik: String, notelp: String, pertanyaan: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BantuanService.shared.postAuditFaq(
            idFaq: idFaq,
            email: email,
            nama: nama,
            nik: nik,
            notelp: notelp,
            pertanyaan: pertanyaan) { result in
            
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.showAlert = true
                self.message = response.message
                self.noTiket = response.data.notiket
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
        }
    }
}
