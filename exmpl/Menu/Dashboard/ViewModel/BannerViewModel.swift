//
//  BannerViewModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import Foundation
import Combine

class BannerViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    @Published var message: String = ""
    
    @Published var videoList = [VideoBanner]()
    @Published var tutorialList = [VideoBanner]()
    @Published var infoList = [VideoBanner]()
}

extension BannerViewModel {
    
    func videoBanner(searchParam: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BannerService.shared.postVideoBanner(searchParam: searchParam) { result in
            
            self.videoList.removeAll()
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.videoList = response.data
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
    
    func videoTutorial(searchParam: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BannerService.shared.postVideoBanner(searchParam: searchParam) { result in
            
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.tutorialList = response.data
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
    
    func videoInfo(searchParam: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        BannerService.shared.postVideoBanner(searchParam: searchParam) { result in
            
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.infoList = response.data
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
}
