//
//  PanduanViewModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import Foundation

class PanduanViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    @Published var message: String = ""
    
    @Published var panduanList = ListPanduanResponseModel()
}

extension PanduanViewModel {
    
    func panduan(searchParam: String, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        PanduanService.shared.postAllPanduan(search: searchParam) { result in
            
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.panduanList = response
                
                completion(true)
                break
            case .failure(let error):
                
                print(error)
                completion(false)
                break
            }
            
        }
    }
}
