//
//  ProfilService.swift
//  exmpl
//
//  Created by Prima Jatnika on 19/11/21.
//

import Foundation

class ProfilService {
    private init() {}
    static let shared = ProfilService()
    
    // MARK:- GET
    func getProfil(completion: @escaping(Result<ProfilResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlProfil() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        var request = URLRequest(url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ProfilResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ProfilResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- POST
    func ubahProfil(email: String, noTelepon: String, idProfil: Int, completion: @escaping(Result<StatusOtpResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlUbahProfil() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "id_profile": String(idProfil),
            "email": email,
            "telp": noTelepon
        ]
        
        var request = URLRequest(url)
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- POST
    func ubahPassword(oldPassword: String, password: String, idProfil: Int, completion: @escaping(Result<StatusOtpResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlUbahPassword() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "id_profile": String(idProfil),
            "password_lama": oldPassword,
            "password": password
        ]
        
        var request = URLRequest(url)
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
}
