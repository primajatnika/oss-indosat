//
//  PanduanService.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import Foundation

class PanduanService {
    private init() {}
    static let shared = PanduanService()
    
    // MARK:- POST
    func postAllPanduan(search: String, completion: @escaping(Result<ListPanduanResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlAllPanduan() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "columns": [
                [
                    "data": "kode",
                    "searchable": true
                ],
                [
                    "data": "pelaku_usaha",
                    "searchable": true
                ],
                [
                    "data": "status"
                ],
                [
                    "data": "deskripsi"
                ],
                [
                    "data": "url"
                ],
                [
                    "data": "deskripsi"
                ],
                [
                    "data": "nourut"
                ]
            ],
            "order": [
                "column": "nourut",
                "dir": "asc"
            ],
            "page": 1,
            "perPage": 1000,
            "search": [
                "value": ""
            ]
        ]
        
        var request = URLRequest(url.appending("lang", value: "id"))
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListPanduanResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListPanduanResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
}
