//
//  BannerService.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import Foundation

class BannerService {
    private init() {}
    static let shared = BannerService()
    
    // MARK:- POST
    func postVideoBanner(searchParam: String, completion: @escaping(Result<ListVideoBannerResponseModel, ErrorResult>) -> Void) {
        
        print("VALUE SEARCH")
        print(searchParam)
        
        guard let url = URL.urlVideoBanner() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "columns": [
                [
                    "data": "id"
                ],
                [
                    "data": "judul"
                ],
                [
                    "data": "uraian"
                ],
                [
                    "data": "path_url"
                ],
                [
                    "data": "tipe"
                ]
            ],
            "order": [
                "column": "created",
                "dir": "desc"
            ],
            "filter": [
                [
                    "column": "tipe",
                    "value": "02"
                ],
                [
                    "column": "is_publish",
                    "value": 1
                ]
            ],
            "page": 1,
            "perPage": 1000
        ]
        
        let bodySearch: [String: Any] = [
            "columns": [
                [
                    "data": "id"
                ],
                [
                    "data": "judul",
                    "searchable": true
                ],
                [
                    "data": "uraian"
                ],
                [
                    "data": "path_url"
                ],
                [
                    "data": "tipe"
                ]
            ],
            "order": [
                "column": "created",
                "dir": "desc"
            ],
            "filter": [
                [
                    "column": "jenis",
                    "value": searchParam
                ]
            ],
            "page": 1,
            "perPage": 10,
            "search": [
                "value": "umk"
            ]
        ]
        
        var request = URLRequest(url.appending("lang", value: "id"))
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: searchParam.isEmpty ? body : bodySearch, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListVideoBannerResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListVideoBannerResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
}
