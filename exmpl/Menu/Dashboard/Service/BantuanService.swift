//
//  BantuanService.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import Foundation

class BantuanService {
    
    private init() {}
    static let shared = BantuanService()
    
    // MARK:- POST
    func postAllFaq(search: String, completion: @escaping(Result<ListFAQResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlAllFaq() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "columns": [
                [
                    "data": "id_faq"
                ],
                [
                    "data": "parameter",
                    "searchable": true
                ],
                [
                    "data": "jawaban"
                ],
                [
                    "data": "kode"
                ],
                [
                    "data": "kategori",
                    "searchable": true
                ]
            ],
            "order": [
                "column": "parameter",
                "dir": "asc"
            ],
            "search": [
                "value": search
            ],
            "page": 1,
            "perPage": 5
        ]
        
        var request = URLRequest(url.appending("lang", value: "id"))
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListFAQResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListFAQResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- GET
    func getKategoriFaq(completion: @escaping(Result<ListKategoriFAQResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlKategoriFaq() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        var request = URLRequest(url.appending("lang", value: "id"))
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListKategoriFAQResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListKategoriFAQResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- POST
    func postAllKbli(
        search: String,
        kategori: String,
        kode: String, completion: @escaping(Result<ListKbliResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlAllKbli() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        guard let urlSearch = URL.urlSearchKbli() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "columns": [
                [
                    "data": "kode",
                    "searchable": true,
                    "sortable": true
                ],
                [
                    "data": "judul",
                    "searchable": true,
                    "sortable": true
                ],
                [
                    "data": "uraian",
                    "searchable": false,
                    "sortable": true
                ],
                [
                    "data": "kategori",
                    "searchable": true,
                    "sortable": true
                ]
            ],
            "order": [
                "column": "kategori",
                "dir": "asc"
            ],
            "kategori": kategori,
            "kode": kode,
            "page": 1,
            "perPage": 1000
        ]
        
        let bodySearch: [String: Any] = [
            "columns": [
                [
                    "data": "kode",
                    "sortable": true
                ],
                [
                    "data": "judul",
                    "sortable": true
                ],
                [
                    "data": "uraian"
                ],
                [
                    "data": "kategori",
                    "searchable": true,
                    "sortable": true
                ]
            ],
            "order": [
                "column": "",
                "dir": ""
            ],
            "page": 1,
            "perPage": 100,
            "search": [
                "value": search
            ]
        ]
        
        var request = URLRequest(search.isEmpty ? url.appending("lang", value: "id") : urlSearch.appending("lang", value: "id"))
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            let searchJsonData = try JSONSerialization.data(withJSONObject: bodySearch, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = search.isEmpty ? jsonData : searchJsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListKbliResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListKbliResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- POST
    func postKbliUmku(
        search: String,
        kbli: String,
        completion: @escaping(Result<ListKbliUmkuResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlKbliUmku() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "kbli": kbli,
            "page": 1,
            "perPage": 1000,
            "search": [
                "value": search
            ]
        ]
        
        var request = URLRequest(url.appending("lang", value: "id"))
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListKbliUmkuResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListKbliUmkuResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- POST
    func postKbliResiko(
        kbli: String,
        completion: @escaping(Result<ListKbliResikoResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlKbliResiko() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "kbli": kbli
        ]
        
        var request = URLRequest(url.appending("lang", value: "id"))
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListKbliResikoResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListKbliResikoResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- POST
    func postRatingFaq(tanggal: String, idFaq: Int, bintang: Int, idFeedback: Int, completion: @escaping(Result<StatusResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlSendRatting() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "tgl": tanggal,
            "id_faq": idFaq,
            "bintang": bintang,
            "id_feedback": idFeedback
        ]
        
        var request = URLRequest(url.appending("lang", value: "id"))
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(StatusResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(StatusResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- POST
    func postAuditFaq(idFaq: Int, email: String, nama: String, nik: String, notelp: String, pertanyaan: String, completion: @escaping(Result<AuditResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlSendAudit() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "id_faq": String(idFaq),
            "status": "N",
            "email": email,
            "nama": nama,
            "no_identitas": nik,
            "notelp": notelp,
            "pertanyaan": pertanyaan
        ]
        
        var request = URLRequest(url.appending("lang", value: "id"))
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(AuditResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(AuditResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
}
