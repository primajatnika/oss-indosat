//
//  StatusResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import Foundation

// MARK: - StatusResponseModel
struct StatusResponseModel: Codable {
    let status, message: String
}
