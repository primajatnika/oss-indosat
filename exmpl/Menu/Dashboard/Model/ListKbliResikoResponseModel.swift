//
//  ListKbliResikoResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 16/11/21.
//

import Foundation

// MARK: - ListKbliResikoResponseModel
struct ListKbliResikoResponseModel: Codable {
    let data: [KbliResiko]
}

// MARK: - Datum
struct KbliResiko: Codable {
    let idRuangLingkup: Int
    let judulRuangLingkup: String
    let idReferensiRegulasi: Int
    let judulRegulasi: String
    let resiko: [Resiko]

    enum CodingKeys: String, CodingKey {
        case idRuangLingkup = "id_ruang_lingkup"
        case judulRuangLingkup = "judul_ruang_lingkup"
        case idReferensiRegulasi = "id_referensi_regulasi"
        case judulRegulasi = "judul_regulasi"
        case resiko
    }
}

// MARK: - Resiko
struct Resiko: Codable {
    let kode, skala: String?
    let tingkatResiko: String?
    let luasLahan: String?
    let jangkaWaktu: String?
    let masaBerlaku: String?
    let parameter: [String: String]?
    let kewenangan: [String: String]?
    let persyaratan: [String: Int]?
    let kewajiban: [String: Int]?
    let jangkaWaktuKewajiban: [String]?
    let jangkaWaktuPersyaratan: [String]?
    let perizinanBerusaha: [String]?

    enum CodingKeys: String, CodingKey {
        case kode, skala
        case tingkatResiko = "tingkat_resiko"
        case luasLahan = "luas_lahan"
        case jangkaWaktu = "jangka_waktu"
        case masaBerlaku = "masa_berlaku"
        case parameter, kewenangan, persyaratan
        case kewajiban
        case jangkaWaktuKewajiban = "jangka_waktu_kewajiban"
        case jangkaWaktuPersyaratan = "jangka_waktu_persyaratan"
        case perizinanBerusaha = "perizinan_berusaha"
    }
}

// MARK: - Kewenangan
struct KewenanganResiko: Codable {
    let bupatiWalikota: String
    let gubernur, menteriKepalaBadan: String?

    enum CodingKeys: String, CodingKey {
        case bupatiWalikota = "Bupati/Walikota"
        case gubernur = "Gubernur"
        case menteriKepalaBadan = "Menteri/Kepala Badan"
    }
}

// MARK: - Parameter
struct ParameterResiko: Codable {
    let lahanUsahaBerlokasiDiKabupatenKota, parameterLahanUsahaBerlokasiDiKabupatenKota, lahanUsahaBerlokasiDiLintasProvinsi, lahanUsahaBerlokasiDiLintasKabupatenKota: String?
    let pma, seluruh, wilayahKabupatenKota, kabupatenKota: String?
    let lintasKabupatenKota: String?

    enum CodingKeys: String, CodingKey {
        case lahanUsahaBerlokasiDiKabupatenKota = "Lahan usaha berlokasi di Kabupaten/ kota "
        case parameterLahanUsahaBerlokasiDiKabupatenKota = "Lahan usaha berlokasi di kabupaten/ kota"
        case lahanUsahaBerlokasiDiLintasProvinsi = "Lahan usaha berlokasi di lintas provinsi"
        case lahanUsahaBerlokasiDiLintasKabupatenKota = "Lahan usaha berlokasi di lintas kabupaten/ kota"
        case pma = "PMA"
        case seluruh = "Seluruh"
        case wilayahKabupatenKota = "Wilayah Kabupaten/ Kota"
        case kabupatenKota = "Kabupaten/ Kota"
        case lintasKabupatenKota = "Lintas Kabupaten / Kota"
    }
}
