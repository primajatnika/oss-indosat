//
//  ProfilResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 19/11/21.
//

import Foundation

// MARK: - ProfilResponseModel
struct ProfilResponseModel: Codable {
    let status: Int
    let message: String
    let data: ProfilData
}

// MARK: - DataClass
struct ProfilData: Codable {
    let idProfile: Int?
    let username: String?
    let kodeInstansi: String?
    let jenisIdentitas, nomorIdentitas, nama, email: String?
    let alamat, telp, status, role: String?
    let flagUmk: String?
    let foto, namaKota: String?
    let jenisPerseroan, flagMigrasi: String?
    let kantor, unitKerja: String?
    let npwpPerseroan: String?
    let dataNib: [String]?

    enum CodingKeys: String, CodingKey {
        case idProfile = "id_profile"
        case username
        case kodeInstansi = "kode_instansi"
        case jenisIdentitas = "jenis_identitas"
        case nomorIdentitas = "nomor_identitas"
        case nama, email, alamat, telp, status, role
        case flagUmk = "flag_umk"
        case foto
        case namaKota = "nama_kota"
        case jenisPerseroan = "jenis_perseroan"
        case flagMigrasi = "flag_migrasi"
        case kantor
        case unitKerja = "unit_kerja"
        case npwpPerseroan = "npwp_perseroan"
        case dataNib = "data_nib"
    }
}
