//
//  ListKategoriFaqResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import Foundation

// MARK: - ListKategoriFAQResponseModelElement
struct ListKategoriFAQResponseModelElement: Codable {
    let id: Int
    let kode, kategori, lang: String
    let image, idImage: String?
    let isPublish: Int

    enum CodingKeys: String, CodingKey {
        case id, kode, kategori, lang, image
        case idImage = "id_image"
        case isPublish = "is_publish"
    }
}

typealias ListKategoriFAQResponseModel = [ListKategoriFAQResponseModelElement]
