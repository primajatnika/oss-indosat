//
//  ListFaqResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import Foundation

// MARK: - ListFAQResponseModel
struct ListFAQResponseModel: Codable {
    let recordsTotal: Int
    let data: [Datum]
}

// MARK: - Datum
struct Datum: Codable {
    let idFAQ: Int
    let parameter, jawaban, kode: String
    let kategori: String

    enum CodingKeys: String, CodingKey {
        case idFAQ = "id_faq"
        case parameter, jawaban, kode, kategori
    }
}
