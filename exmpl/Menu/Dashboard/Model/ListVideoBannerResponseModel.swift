//
//  ListVideoBannerResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import Foundation

// MARK: - ListVideoBannerResponseModel
struct ListVideoBannerResponseModel: Codable {
    let recordsTotal, recordsTotalInfo, recordsTotalTutorial: Int?
    let data: [VideoBanner]
}

// MARK: - Datum
struct VideoBanner: Codable {
    let id: Int
    let judul, uraian: String
    let pathURL: String
    let tipe: String

    enum CodingKeys: String, CodingKey {
        case id, judul, uraian
        case pathURL = "path_url"
        case tipe
    }
}
