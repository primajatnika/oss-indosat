//
//  AuditResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import Foundation

// MARK: - AuditResponseModel
struct AuditResponseModel: Codable {
    let status, message: String
    let data: DataAudit
}

// MARK: - DataClass
struct DataAudit: Codable {
    let idFAQ, status, email, nama: String
    let noIdentitas, notelp, pertanyaan, tiket: String
    let notiket: Int

    enum CodingKeys: String, CodingKey {
        case idFAQ = "id_faq"
        case status, email, nama
        case noIdentitas = "no_identitas"
        case notelp, pertanyaan, tiket, notiket
    }
}
