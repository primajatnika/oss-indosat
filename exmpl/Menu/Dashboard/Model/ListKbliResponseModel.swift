import Foundation

// MARK: - ListKbliResponseModel
struct ListKbliResponseModel: Codable {
    let recordsTotal: Int
    let info: KbliInfo?
    let deskripsi: String?
    let data: [KbliModel]
    let parent: [KbliParentModel]?
}

// MARK: - Datum
struct KbliModel: Codable {
    let kode, judul, uraian: String
    let kategori: String
    let count: String?
}

// MARK: - Info
struct KbliInfo: Codable {
    let kbliKkSyarat: [KbliKkSyarat]

    enum CodingKeys: String, CodingKey {
        case kbliKkSyarat = "kbli_kk_syarat"
    }
}

// MARK: - KbliKkSyarat
struct KbliKkSyarat: Codable {
    let jnsBu, idSyarat, uraian: String

    enum CodingKeys: String, CodingKey {
        case jnsBu = "jns_bu"
        case idSyarat = "id_syarat"
        case uraian
    }
}

struct KbliParentModel: Codable {
    let kode, judul, uraian: String
}

