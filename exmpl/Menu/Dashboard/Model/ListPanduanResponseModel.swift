//
//  ListPanduanResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import Foundation

// MARK: - ListPanduanResponseModelElement
struct ListPanduanResponseModelElement: Codable {
    let kode: String
    let pelakuUsaha: String
    let status: String
    let deskripsi: String
    let url: String?
    let nourut: Int
    let detailPanduan: [DetailPanduan]

    enum CodingKeys: String, CodingKey {
        case kode
        case pelakuUsaha = "pelaku_usaha"
        case status, deskripsi, url, nourut
        case detailPanduan = "_detailPanduan"
    }
}

// MARK: - DetailPanduan
struct DetailPanduan: Codable {
    let id: Int
    let kode: String
    let status: String
    let deskripsi: String
    let nourut: Int
    let url: String
}

typealias ListPanduanResponseModel = [ListPanduanResponseModelElement]

