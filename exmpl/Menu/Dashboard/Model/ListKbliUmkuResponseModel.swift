//
//  ListKbliUmkuResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 16/11/21.
//

import Foundation

// MARK: - ListKbliUmkuResponseModel
struct ListKbliUmkuResponseModel: Codable {
    let recordsTotal: Int
    let data: [KbliUmku]
}

// MARK: - Datum
struct KbliUmku: Codable {
    let idLic, namaDokumen: String
    let parameter: [String: String]?
    let kewenangan: [String: String]?
    let persyaratan: [String: Int]?
    let kewajiban: [String: Int]?
    let count: String

    enum CodingKeys: String, CodingKey {
        case idLic = "id_lic"
        case namaDokumen = "nama_dokumen"
        case parameter, kewenangan, persyaratan
        case kewajiban
        case count
    }
}
