//
//  VideoScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 17/11/21.
//

import SwiftUI

struct VideoScreen: View {
    
    @State private var segmentedIndex: Int = 0
    @State private var searchCtrl: String = ""
    
    @StateObject var bannerVM = BannerViewModel()
    @State var listVideo = [VideoBanner]()
    @State var listTutorial = [VideoBanner]()
    @State var listInfo = [VideoBanner]()
    @State var listSearch = [VideoBanner]()
    
    @State var listThumbnailVideo = [String]()
    @State var listThumbnailTutorial = [String]()
    @State var listThumbnailInfo = [String]()
    @State var listThumbnailSearch = [String]()
    
    @State var isLoading: Bool = true
    
    var body: some View {
        VStack {
            
            if isLoading {
                LoadingView()
            }
            
            searchCard
            
            if searchCtrl.isEmpty {
                Picker("FQA", selection: $segmentedIndex) {
                    Text("Semua Video").tag(0)
                    Text("Tutorial OSS").tag(1)
                    Text("Info OSS").tag(2)
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding(.horizontal, 20)
                .padding(.vertical, 10)
                
                if segmentedIndex == 0 {
                    if (listVideo.count > 2) {
                        ScrollView {
                            ForEach(0..<listVideo.count) { index in
                                Link(destination: URL(string: "\(listVideo[index].pathURL)")!, label: {
                                    VStack(alignment: .leading) {
                                        
                                        ZStack {
                                            Image(systemName: "")
                                                .data(url: URL(string: "\(listThumbnailVideo[index])")!)
                                                .resizable()
                                                .frame(width: getWidth() - 30, height: 200)
                                                .cornerRadius(10)
                                                .shadow(radius: 4)
                                            
                                            Image("ic_play")
                                        }
                                        
                                        Text("\(listVideo[index].judul)")
                                            .fontWeight(.semibold)
                                            .font(.caption)
                                            .foregroundColor(Color("gray"))
                                            .multilineTextAlignment(.leading)
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                    .padding(.top, 5)
                                    .padding(.bottom, 10)
                                    .padding(.horizontal, 10)
                                })
                            }
                        }
                    }
                }
                
                if segmentedIndex == 1 {
                    if (listTutorial.count > 0) {
                        ScrollView {
                            ForEach(0..<listTutorial.count) { index in
                                Link(destination: URL(string: "\(listTutorial[index].pathURL)")!, label: {
                                    VStack(alignment: .leading) {
                                        
                                        ZStack {
                                            Image(systemName: "")
                                                .data(url: URL(string: "\(listThumbnailTutorial[index])")!)
                                                .resizable()
                                                .frame(width: getWidth() - 30, height: 200)
                                                .cornerRadius(10)
                                                .shadow(radius: 4)
                                            
                                            Image("ic_play")
                                        }
                                        
                                        Text("\(listTutorial[index].judul)")
                                            .fontWeight(.semibold)
                                            .font(.caption)
                                            .foregroundColor(Color("gray"))
                                            .multilineTextAlignment(.leading)
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                    .padding(.top, 5)
                                    .padding(.bottom, 10)
                                    .padding(.horizontal, 10)
                                })
                            }
                        }
                    }
                }
                
                if segmentedIndex == 2 {
                    if (listInfo.count > 0) {
                        ScrollView {
                            ForEach(0..<listInfo.count) { index in
                                Link(destination: URL(string: "\(listInfo[index].pathURL)")!, label: {
                                    VStack(alignment: .leading) {
                                        
                                        ZStack {
                                            Image(systemName: "")
                                                .data(url: URL(string: "\(listThumbnailInfo[index])")!)
                                                .resizable()
                                                .frame(width: getWidth() - 30, height: 200)
                                                .cornerRadius(10)
                                                .shadow(radius: 4)
                                            
                                            Image("ic_play")
                                        }
                                        
                                        Text("\(listInfo[index].judul)")
                                            .fontWeight(.semibold)
                                            .font(.caption)
                                            .foregroundColor(Color("gray"))
                                            .multilineTextAlignment(.leading)
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                    .padding(.top, 5)
                                    .padding(.bottom, 10)
                                    .padding(.horizontal, 10)
                                })
                            }
                        }
                    }
                }
            } else {
                ScrollView {
                    ForEach(0..<listSearch.count) { index in
                        Link(destination: URL(string: "\(listSearch[index].pathURL)")!, label: {
                            VStack(alignment: .leading) {
                                
                                ZStack {
                                    Image(systemName: "")
                                        .data(url: URL(string: "\(listThumbnailSearch[index])")!)
                                        .resizable()
                                        .frame(width: getWidth() - 30, height: 200)
                                        .cornerRadius(10)
                                        .shadow(radius: 4)
                                    
                                    Image("ic_play")
                                }
                                
                                Text("\(listSearch[index].judul)")
                                    .fontWeight(.semibold)
                                    .font(.caption)
                                    .foregroundColor(Color("gray"))
                                    .multilineTextAlignment(.leading)
                                    .fixedSize(horizontal: false, vertical: true)
                            }
                            .padding(.top, 5)
                            .padding(.bottom, 10)
                            .padding(.horizontal, 10)
                        })
                    }
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationBarTitle("Video", displayMode: .inline)
        .onAppear() {
            getVideoBanner()
            getVideoTutorial()
            getVideoInfo()
        }
    }
    
    var searchCard: some View {
        HStack {
            HStack {
                TextField("", text: $searchCtrl) { changed in
                    print("changed in")
                    print("\($searchCtrl)")
                    searchVideo()
                } onCommit: {
                    print("\($searchCtrl)")
                    searchVideo()
                }.onChange(of: searchCtrl) { item in
                    print("onchange")
                    searchVideo()
                }
                .placeholder(when: searchCtrl.isEmpty) {
                        Text("Cari Video").foregroundColor(Color("gray"))
                }
                .foregroundColor(Color("gray"))
                
                Button(action: {
                    searchVideo()
                }, label: {
                    Image(systemName: "magnifyingglass")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(Color(hex: "#BE2812"))
                        .frame(width: 15, height: 15)
                })
                .frame(width: 15, height: 15)
                .padding(.horizontal, 10)
                .padding(.vertical, 10)
                .background(Color.white)
                .cornerRadius(5)
            }
            .frame(height: 10)
            .font(.subheadline)
            .padding(.vertical)
            .padding(.leading, 10)
            .padding(.trailing, 5)
            .background(Color("lighter_gray"))
            .cornerRadius(5)
            .padding(.horizontal, 20)
        }
        .padding([.bottom, .top], 10)
    }
    
}

struct VideoScreen_Previews: PreviewProvider {
    static var previews: some View {
        VideoScreen()
    }
}

extension VideoScreen {
    
    func getVideoBanner() {
        self.bannerVM.videoBanner(searchParam: searchCtrl) { success in
            
            if success {
                self.isLoading = false
                self.listVideo = self.bannerVM.videoList
                
                self.listVideo.forEach { (data: VideoBanner) in
                    if let range = data.pathURL.range(of: "embed/") {
                        let id = data.pathURL[range.upperBound...]
                        print(id)
                        self.listThumbnailVideo.append("https://img.youtube.com/vi/\(String(id))/0.jpg")
                    } else {
                        self.listThumbnailVideo.append("https://img.youtube.com/vi/2enHxEvwryw/0.jpg")
                    }
                }
            }
            
            if !success {
                
            }
        }
    }
    
    func getVideoTutorial() {
        self.bannerVM.videoTutorial(searchParam: "tutorial") { success in
            
            if success {
                self.listTutorial = self.bannerVM.tutorialList
                
                self.listTutorial.forEach { (data: VideoBanner) in
                    if let range = data.pathURL.range(of: "embed/") {
                        let id = data.pathURL[range.upperBound...]
                        print(id)
                        self.listThumbnailTutorial.append("https://img.youtube.com/vi/\(String(id))/0.jpg")
                    } else {
                        self.listThumbnailTutorial.append("https://img.youtube.com/vi/2enHxEvwryw/0.jpg")
                    }
                }
            }
            
            if !success {
                
            }
        }
    }
    
    func getVideoInfo() {
        self.bannerVM.videoInfo(searchParam: "info") { success in
            
            if success {
                self.listInfo = self.bannerVM.infoList
                
                self.listInfo.forEach { (data: VideoBanner) in
                    if let range = data.pathURL.range(of: "embed/") {
                        let id = data.pathURL[range.upperBound...]
                        print(id)
                        self.listThumbnailInfo.append("https://img.youtube.com/vi/\(String(id))/0.jpg")
                    } else {
                        self.listThumbnailInfo.append("https://img.youtube.com/vi/2enHxEvwryw/0.jpg")
                    }
                }
            }
            
            if !success {
                
            }
        }
    }
    
    func searchVideo() {
        self.listSearch = self.listVideo.filter({ (data: VideoBanner) -> Bool in
            return data.judul.localizedStandardContains(searchCtrl)
        })
        
        self.listSearch.forEach { (data: VideoBanner) in
            if let range = data.pathURL.range(of: "embed/") {
                let id = data.pathURL[range.upperBound...]
                print(id)
                self.listThumbnailSearch.append("https://img.youtube.com/vi/\(String(id))/0.jpg")
            } else {
                self.listThumbnailSearch.append("https://img.youtube.com/vi/2enHxEvwryw/0.jpg")
            }
        }
    }
    
}
