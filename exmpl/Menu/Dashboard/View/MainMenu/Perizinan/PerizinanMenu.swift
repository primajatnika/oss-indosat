//
//  PerizinanMenu.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI

struct PerizinanMenu: View {
    var body: some View {
        VStack {
            List {
                Text("Permohonan Baru")
                Text("Perubahan Data Pelaku Usaha")
                Text("Perubahan Data Usaha")
                Text("Perpanjangan")
                Text("Pengembangan")
                Text("Perluasan")
            }
        }
        .navigationBarTitle("Perizinan", displayMode: .inline)
    }
}

struct PerizinanMenu_Previews: PreviewProvider {
    static var previews: some View {
        PerizinanMenu()
    }
}
