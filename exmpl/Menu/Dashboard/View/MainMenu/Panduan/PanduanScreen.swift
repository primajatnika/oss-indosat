//
//  PanduanScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import SwiftUI

struct PanduanScreen: View {
    
    @StateObject var panduanVM = PanduanViewModel()
    @State var listPanduan = ListPanduanResponseModel()
    @State var listPanduanSearch = ListPanduanResponseModel()
    
    @State private var searchCtrl: String = ""
    
    var body: some View {
        VStack {
            
            if (self.panduanVM.isLoading) {
                LoadingView()
            }
            
            searchCard
            
            if searchCtrl.isEmpty {
                card
                    .padding(.top, 10)
                    .padding(.horizontal, 20)
            } else {
                cardSuggest
                    .padding(.top, 10)
                    .padding(.horizontal, 20)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationBarTitle("Panduan", displayMode: .inline)
        .onAppear() {
            getPanduan(searchParam: "")
        }
    }
    
    var searchCard: some View {
        HStack {
            HStack {
                TextField("", text: $searchCtrl) { changed in
                    print("changed in")
                    print("\($searchCtrl)")
                    searchPanduan(searchParam: self.searchCtrl)
                } onCommit: {
                    print("\($searchCtrl)")
                    searchPanduan(searchParam: self.searchCtrl)
                }.onChange(of: searchCtrl) { item in
                    print("onchange")
                    searchPanduan(searchParam: self.searchCtrl)
                }
                .placeholder(when: searchCtrl.isEmpty) {
                        Text("Cari Panduan").foregroundColor(Color("gray"))
                }
                .foregroundColor(Color("gray"))
                
                Button(action: {
                    searchPanduan(searchParam: self.searchCtrl)
                }, label: {
                    Image(systemName: "magnifyingglass")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(Color(hex: "#BE2812"))
                        .frame(width: 15, height: 15)
                })
                .frame(width: 15, height: 15)
                .padding(.horizontal, 10)
                .padding(.vertical, 10)
                .background(Color.white)
                .cornerRadius(5)
            }
            .frame(height: 10)
            .font(.subheadline)
            .padding(.vertical)
            .padding(.leading, 10)
            .padding(.trailing, 5)
            .background(Color("lighter_gray"))
            .cornerRadius(5)
            .padding(.horizontal, 20)
        }
        .padding([.bottom, .top], 10)
    }
    
    var card: some View {
        ForEach(listPanduan.prefix(2), id: \.nourut) { data in
            NavigationLink(
                destination: PanduanByKategoriScreen(title: .constant(data.pelakuUsaha), data: .constant(data.detailPanduan)),
                label: {
                    VStack {
                        HStack {
                            Text("\(data.pelakuUsaha)")
                                .font(.caption)
                                .foregroundColor(Color("gray"))
                                .fontWeight(.semibold)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                        }
                        .padding()
                    }
                    .background(Color("light_gray"))
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
    
    var cardSuggest: some View {
        ForEach(listPanduanSearch, id: \.nourut) { data in
            NavigationLink(
                destination: PanduanByKategoriScreen(title: .constant(data.pelakuUsaha), data: .constant(data.detailPanduan)),
                label: {
                    VStack {
                        HStack {
                            Text("\(data.pelakuUsaha)")
                                .font(.caption)
                                .foregroundColor(Color("gray"))
                                .fontWeight(.semibold)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                        }
                        .padding()
                    }
                    .background(Color("light_gray"))
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
}

struct PanduanScreen_Previews: PreviewProvider {
    static var previews: some View {
        PanduanScreen()
    }
}

extension PanduanScreen {
    
    func getPanduan(searchParam: String) {
        self.panduanVM.panduan(searchParam: searchParam) { success in
            
            if success {
                self.listPanduan = self.panduanVM.panduanList
            }
            
            if !success {
                
            }
        }
    }
    
    func searchPanduan(searchParam: String) {
        self.listPanduanSearch = listPanduan.filter({ (data:ListPanduanResponseModelElement)  in
            return data.pelakuUsaha.localizedStandardContains(searchParam)
        })
    }
    
}
