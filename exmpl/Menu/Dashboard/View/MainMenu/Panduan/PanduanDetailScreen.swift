//
//  PanduanDetailScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import SwiftUI

struct PanduanDetailScreen: View {
    
    @Binding var url: String
    
    var body: some View {
        VStack {
            WebView(request: URLRequest(url: URL(string: url)!))
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationBarTitle("Detail", displayMode: .inline)
    }
}

struct PanduanDetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        PanduanDetailScreen(url: .constant(""))
    }
}
