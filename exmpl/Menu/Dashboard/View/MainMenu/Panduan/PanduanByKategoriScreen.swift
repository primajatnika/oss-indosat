//
//  PanduanByKategoriScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import SwiftUI

struct PanduanByKategoriScreen: View {
    
    @State private var searchCtrl: String = ""
    
    @Binding var title: String
    @Binding var data: [DetailPanduan]
    
    @State var dataSearch = [DetailPanduan]()
    
    var body: some View {
        VStack {
            
            searchCard
            
            ScrollView {
                if searchCtrl.isEmpty {
                    card
                        .padding(.top, 10)
                        .padding(.horizontal, 20)
                } else {
                    cardSuggest
                        .padding(.top, 10)
                        .padding(.horizontal, 20)
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationBarTitle("\(title)", displayMode: .inline)
    }
    
    var searchCard: some View {
        HStack {
            HStack {
                TextField("", text: $searchCtrl) { changed in
                    print("changed in")
                    print("\($searchCtrl)")
                    filter()
                } onCommit: {
                    print("\($searchCtrl)")
                    filter()
                }.onChange(of: searchCtrl) { item in
                    print("onchange")
                    filter()
                }
                .placeholder(when: searchCtrl.isEmpty) {
                        Text("Cari Panduan").foregroundColor(Color("gray"))
                }
                .foregroundColor(Color("gray"))
                
                Button(action: {
                    filter()
                }, label: {
                    Image(systemName: "magnifyingglass")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(Color(hex: "#BE2812"))
                        .frame(width: 15, height: 15)
                })
                .frame(width: 15, height: 15)
                .padding(.horizontal, 10)
                .padding(.vertical, 10)
                .background(Color.white)
                .cornerRadius(5)
            }
            .frame(height: 10)
            .font(.subheadline)
            .padding(.vertical)
            .padding(.leading, 10)
            .padding(.trailing, 5)
            .background(Color("lighter_gray"))
            .cornerRadius(5)
            .padding(.horizontal, 20)
        }
        .padding([.bottom, .top], 10)
    }
    
    var card: some View {
        ForEach(data, id: \.id) { value in
            NavigationLink(
                destination: PanduanDetailScreen(url: .constant(value.url)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            Text("\(value.deskripsi)")
                                .font(.subheadline)
                                .foregroundColor(Color("gray"))
                                .fontWeight(.semibold)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                            Image(systemName: "chevron.right")
                                .foregroundColor(.secondary)
                        }
                        .padding()
                        
                        Divider()
                            .padding(.horizontal)
                    }
                })
        }
    }
    
    var cardSuggest: some View {
        ForEach(dataSearch, id: \.id) { value in
            NavigationLink(
                destination: PanduanDetailScreen(url: .constant(value.url)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            Text("\(value.deskripsi)")
                                .font(.subheadline)
                                .foregroundColor(Color("gray"))
                                .fontWeight(.semibold)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                            Image(systemName: "chevron.right")
                                .foregroundColor(.secondary)
                        }
                        .padding()
                        
                        Divider()
                            .padding(.horizontal)
                    }
                })
        }
    }
}

struct PanduanByKategoriScreen_Previews: PreviewProvider {
    static var previews: some View {
        PanduanByKategoriScreen(title: .constant(""), data: .constant([DetailPanduan(id: 0, kode: "", status: "", deskripsi: "", nourut: 0, url: "")]))
    }
}

extension PanduanByKategoriScreen {
    
    func filter() {
        self.dataSearch = self.data.filter { (value: DetailPanduan) -> Bool in
            
            return value.deskripsi.localizedStandardContains(searchCtrl)
            
        }
    }
    
}
