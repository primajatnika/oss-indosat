//
//  FaqKirimFormScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import SwiftUI

struct FaqKirimFormScreen: View {
    
    @StateObject var bantuanVM = BantuanViewModel()
    @Binding var idFaq: Int
    
    @State private var namaCtrl: String = ""
    @State private var emailCtrl: String = ""
    @State private var teleponCtrl: String = ""
    @State private var nikCtrl: String = ""
    @State private var pertanyaanCtrl: String = ""
    @State private var isChecked: Bool = false
    @State private var isLoading: Bool = false
    @State private var isSuccess: Bool = false
    
    @State private var nomorTiket: Int = 0
    
    var body: some View {
        VStack {
            
            if isLoading {
                LoadingView()
            }
            
            VStack(alignment: .leading, spacing: 20) {
                
                ScrollView {
                    
                    if isSuccess {
                        HStack {
                            Text("Nomor Tiket : ")
                                .font(.headline)
                                .foregroundColor(Color("gray"))
                                .fontWeight(.semibold)
                                .padding(.leading, 20)
                            
                            Text("\(self.bantuanVM.noTiket)")
                                .font(.headline)
                                .foregroundColor(Color("blue"))
                                .fontWeight(.semibold)
                            
                            Spacer()
                        }
                        .padding(.bottom, 20)
                        .padding(.top, 10)
                    }
                    
                    if !isSuccess {
                        HStack {
                            
                            if isSuccess {

                            } else {
                                Text("Anda Memerlukan Bantuan?")
                                    .font(.headline)
                                    .foregroundColor(Color("gray"))
                                    .fontWeight(.semibold)
                                    .padding(.horizontal, 20)
                                    .padding(.top, 10)
                            }
                            
                            Spacer()
                        }
                        .padding(.bottom, 20)
                        .padding(.top, 10)
                    }
                    
                    VStack(alignment: .leading) {
                        Text("Nama Lengkap")
                            .font(.callout)
                            .foregroundColor(Color("gray"))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 20)
                        
                        LabelTextFieldDefault(
                            value: self.$namaCtrl,
                            placeHolder: "Nama",
                            colorField: isSuccess ? Color("light_gray") : .white) { (Bool) in
                            
                        } onCommit: {
                            
                        }
                        .disabled(isSuccess)

                    }
                    .padding(.bottom, 10)
                    
                    VStack(alignment: .leading) {
                        Text("Email")
                            .font(.callout)
                            .foregroundColor(Color("gray"))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 20)
                        
                        LabelTextFieldDefault(value: self.$emailCtrl, placeHolder: "Email",                             colorField: isSuccess ? Color("light_gray") : .white) { (Bool) in
                            
                        } onCommit: {
                            
                        }
                        .disabled(isSuccess)
                    }
                    .padding(.bottom, 10)
                    
                    VStack(alignment: .leading) {
                        Text("Nomor Telepon Seluler")
                            .font(.callout)
                            .foregroundColor(Color("gray"))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 20)
                        
                        LabelTextFieldDefault(value: self.$teleponCtrl, placeHolder: "Nomor Telepon",                             colorField: isSuccess ? Color("light_gray") : .white) { (Bool) in
                            
                        } onCommit: {
                            
                        }
                        .disabled(isSuccess)
                    }
                    .padding(.bottom, 10)
                    
                    VStack(alignment: .leading) {
                        Text("Nomor Identitas(KTP)")
                            .font(.callout)
                            .foregroundColor(Color("gray"))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 20)
                        
                        LabelTextFieldDefault(value: self.$nikCtrl, placeHolder: "NIK",                             colorField: isSuccess ? Color("light_gray") : .white) { (Bool) in
                            
                        } onCommit: {
                            
                        }
                        .disabled(isSuccess)
                    }
                    .padding(.bottom, 10)
                    
                    VStack(alignment: .leading) {
                        Text("Pertanyaan")
                            .font(.callout)
                            .foregroundColor(Color("gray"))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 20)
                        
                        MultilineTextField("Pertanyaan", text: self.$pertanyaanCtrl, onCommit: {
                        })
                        .disabled(isSuccess)
                        .padding(.horizontal, 5)
                        .frame(height: 80)
                        .cornerRadius(10)
                        .background(
                            RoundedRectangle(cornerRadius: 8)
                                .strokeBorder(Color.gray, lineWidth: 1))
                        .background(isSuccess ? Color("light_gray") : .white)
                        .padding(.horizontal, 20)
                        
                    }
                    .padding(.bottom, 10)
                    
                    if !isSuccess {
                        Button(action: { isChecked.toggle() }) {
                            HStack(alignment: .top) {
                                Image(systemName: isChecked ? "checkmark.square": "square")
                                Text("Dengan ini menyatakan bahwa data tersebut adalah benar")
                                    .font(.callout)
                                    .foregroundColor(Color("gray"))
                                    .multilineTextAlignment(.leading)
                                Spacer()
                            }
                            .padding(.horizontal, 20)
                            .padding(.bottom, 5)
                        }
                        .padding(.bottom, 20)
                    }
                }
                
                VStack {
                    // Button
                    Button(action: {
                        if !isSuccess {
                            sendAudit(
                                idFaq: idFaq,
                                email: emailCtrl,
                                nama: namaCtrl,
                                nik: nikCtrl,
                                notelp: teleponCtrl,
                                pertanyaan: pertanyaanCtrl
                            )
                        }
                    }, label: {
                        Text( isSuccess ? "Cetak Tiket" : "Kirimkan")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(Color(disableButton ? "gray" : "white"))
                            .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                        
                    })
                    .background(Color(disableButton ? "light_gray" : "blue"))
                    .cornerRadius(10)
                    .padding(.top, 20)
                    .padding(.bottom, 10)
                    .padding(.horizontal, 20)
                    .disabled(disableButton)
                }
                .background(Color.white)
                .cornerRadius(10, corners: [.topLeft, .topRight])
                .shadow(color: .gray, radius: 10, x: 0, y: 0)
                .mask(Rectangle().padding(.top, -20))
                
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .navigationBarTitle("Bantuan", displayMode: .inline)
        .alert(isPresented: self.$bantuanVM.showAlert, content: {
            return Alert(
                title: Text("Pesan"),
                message: Text("\(self.bantuanVM.message)"),
                dismissButton: .default(Text("Ok"))
            )
        })
    }
}

struct FaqKirimFormScreen_Previews: PreviewProvider {
    static var previews: some View {
        FaqKirimFormScreen(idFaq: .constant(1))
    }
}

extension FaqKirimFormScreen {
    
    var disableButton: Bool {
        namaCtrl.isEmpty || emailCtrl.isEmpty || teleponCtrl.isEmpty || nikCtrl.isEmpty || pertanyaanCtrl.isEmpty || !isChecked || isLoading
    }
    
    func sendAudit(idFaq: Int, email: String, nama: String, nik: String, notelp: String, pertanyaan: String) {
        
        self.isLoading = true
        
        self.bantuanVM.sendAudit(
            idFaq: idFaq,
            email: email,
            nama: nama,
            nik: nik,
            notelp: notelp,
            pertanyaan: pertanyaan) { success in
            
            if success {
                self.isSuccess = true
                self.isLoading = false
            }
            
            if !success {
                self.isSuccess = false
                self.isLoading = false
            }
            
        }
    }
    
}
