//
//  FaqScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import SwiftUI

struct FaqScreen: View {
    
    @StateObject var bantuanVM = BantuanViewModel()
    @State var listFaq = [Datum]()
    @State var listKategoriFaq = [
        ListKategoriFAQResponseModelElement(
            id: 0,
            kode: "00",
            kategori: "AKUN",
            lang: "",
            image: "ic_hak_akses",
            idImage: "",
            isPublish: 0
        ),
        ListKategoriFAQResponseModelElement(
            id: 1,
            kode: "00",
            kategori: "PERSYARATAN DASAR",
            lang: "",
            image: "ic_persyaratan",
            idImage: "",
            isPublish: 0
        ),
        ListKategoriFAQResponseModelElement(
            id: 2,
            kode: "00",
            kategori: "PERIZINAN BERUSAHA",
            lang: "",
            image: "ic_perizinan",
            idImage: "",
            isPublish: 0
        ),
        ListKategoriFAQResponseModelElement(
            id: 3,
            kode: "00",
            kategori: "PB UMKU",
            lang: "",
            image: "ic_umku",
            idImage: "",
            isPublish: 0
        ),
        ListKategoriFAQResponseModelElement(
            id: 4,
            kode: "00",
            kategori: "FASILITAS BERUSAHA",
            lang: "",
            image: "ic_fasilitas",
            idImage: "",
            isPublish: 0
        ),
        ListKategoriFAQResponseModelElement(
            id: 5,
            kode: "00",
            kategori: "LKPM",
            lang: "",
            image: "ic_lkpm",
            idImage: "",
            isPublish: 0
        ),
    ]
    
    private var gridItemLayout = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())]
    
    var body: some View {
        VStack(alignment: .leading) {
            
            ScrollView {
                
                HStack {
                    Text("Yang sering ditanyakan")
                        .font(.headline)
                        .foregroundColor(Color("blue"))
                        .padding(.horizontal, 20)
                    
                    Spacer()
                }
                
                // FAQ Card
                VStack {
                    NavigationLink(
                        destination: KbliScreen(),
                        label: {
                            VStack {
                                HStack(spacing: 10) {
                                    Text("Bagaimana cara memilih Klasifikasi Baku Lapangan Usaha Indonesia (KBLI) sesuai dengan kegiatan usaha?")
                                        .font(.subheadline)
                                        .fontWeight(.semibold)
                                        .foregroundColor(Color("gray"))
                                        .multilineTextAlignment(.leading)
                                        .fixedSize(horizontal: false, vertical: true)
                                    
                                    Spacer()
                                    
                                    Image(systemName: "chevron.right")
                                        .foregroundColor(.secondary)
                                }
                                .padding()
                            }
                            .background(Color.white)
                            .overlay(
                                RoundedRectangle(cornerRadius: 10)
                                    .stroke(Color(.white), lineWidth: 1)
                            )
                            .cornerRadius(10)
                            .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                        })
                    
                    card
                }
                .padding(.horizontal, 20)
                
                HStack {
                    Text("Kategori")
                        .font(.headline)
                        .foregroundColor(Color("blue"))
                        .padding(.horizontal, 20)
                        .padding(.top, 20)
                    
                    Spacer()
                }
                
                kategoriMenu
            }
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .padding(.top, 10)
        .onAppear {
            getDummyFaq()
        }
    }
    
    var card: some View {
        ForEach(listFaq.prefix(4), id: \.idFAQ) { data in
            NavigationLink(
                destination: FaqDetailScreen(
                    data: .constant(data),
                    searchParam: .constant(""),
                    noCount: .constant(0)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            Text("\(data.parameter)")
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .foregroundColor(Color("gray"))
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                            Image(systemName: "chevron.right")
                                .foregroundColor(.secondary)
                        }
                        .padding()
                    }
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
    
    var kategoriMenu: some View {
        LazyVGrid(columns: gridItemLayout, spacing: 20, content: {
            ForEach(listKategoriFaq, id: \.id) { data in
                NavigationLink(
                    destination: FaqByKategoriScreen(data: .constant(data)),
                    label: {
                        VStack {
                            
                            Spacer()
                            
                            HStack {
                                Spacer()
                                Image(data.image!)
                                    .resizable()
                                    .frame(width: 50, height: 50)
                                Spacer()
                            }
                            
                            Spacer()
                            
                            Text("\(data.kategori)")
                                .font(.system(size: 10))
                                .foregroundColor(Color("gray"))
                                .multilineTextAlignment(.center)
                                .padding(.bottom, 10)
                                .padding(.horizontal, 10)
                            
                        }
                        .frame(height: 130)
                        .background(Color.white)
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(Color(.white), lineWidth: 1)
                        )
                        .cornerRadius(10)
                        .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                        .padding(.horizontal, 5)
                    })
            }
        })
        .padding(.horizontal, 10)
    }
}

struct FaqScreen_Previews: PreviewProvider {
    static var previews: some View {
        FaqScreen()
    }
}

extension FaqScreen {
    
    func getDummyFaq() {
        self.listFaq.append(
            Datum(
                idFAQ: 0,
                parameter: "Untuk badan usaha, apakah boleh satu data penanggung jawab yang sama digunakan untuk beberapa badan usaha?",
                jawaban: "-",
                kode: "",
                kategori: ""
            ))
        
        self.listFaq.append(
            Datum(
                idFAQ: 1,
                parameter: "Kenapa satu email hanya dapat digunakan untuk satu NIB?",
                jawaban: "-",
                kode: "",
                kategori: ""
            ))
        
        self.listFaq.append(
            Datum(
                idFAQ: 2,
                parameter: "Bagaimana kalau saya lupa password?",
                jawaban: "Apabila mengalami kejadian seperti ini, Anda dapat melakukan penggantian password dengan mengklik Lupa Password? pada halaman Login/Masuk. Silakan masukkan username atau email Anda, khusus untuk pelaku UMK perseorangan memasukkan nomor WhatsApp.",
                kode: "",
                kategori: ""
            ))
        
        self.listFaq.append(
            Datum(
                idFAQ: 3,
                parameter: "Kenapa saya tidak bisa login padahal sudah memasukkan username dan password dengan benar?",
                jawaban: "Apabila mengalami kejadian seperti ini, Anda dapat melakukan penggantian password dengan mengklik Lupa Password? pada halaman Login/Masuk. Silakan masukkan username atau email Anda, khusus untuk pelaku UMK perseorangan memasukkan nomor WhatsApp.",
                kode: "",
                kategori: ""
            ))
    }
    
    func getFaq(searchParam: String) {
        self.bantuanVM.faq(searchParam: searchParam) { success in
            
            if success {
                self.listFaq = self.bantuanVM.faqList
            }
            
            if !success {
                
            }
            
        }
    }
    
    func getKategoriFaq() {
        self.bantuanVM.kategoriFaq { success in
            
            if success {
                self.listKategoriFaq = self.bantuanVM.kategoriFaqList
            }
            
            if !success {
                
            }
        }
    }
    
}
