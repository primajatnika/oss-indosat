//
//  FaqByKategoriScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import SwiftUI

struct FaqByKategoriScreen: View {
    
    @StateObject var bantuanVM = BantuanViewModel()
    @State var listFaq = [Datum]()
    @State var listFaqSearch = [Datum]()
    
    @State private var searchCtrl: String = ""
    
    @State private var isLoading: Bool = false
    
    @Binding var data: ListKategoriFAQResponseModelElement
    
    var body: some View {
        VStack(alignment: .leading) {
            
            if isLoading {
                LoadingView()
            }
            
            searchCard
            
            ScrollView {
                
                HStack {
                    Text("\(data.kategori)")
                        .font(.headline)
                        .foregroundColor(Color("blue"))
                    
                    Spacer()
                }
                .padding(.horizontal, 20)
                .padding(.top, 10)
                
                // FAQ Card
                if searchCtrl.isEmpty {
                    card
                        .padding(.top, 10)
                        .padding(.horizontal, 20)
                } else {
                    cardSuggest
                        .padding(.top, 10)
                        .padding(.horizontal, 20)
                }
            }
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .navigationBarTitle("Bantuan", displayMode: .inline)
        .onAppear() {
            getDummyFaq()
        }
    }
    
    var card: some View {
        ForEach(listFaq, id: \.idFAQ) { data in
            NavigationLink(
                destination: FaqDetailScreen(
                    data: .constant(data),
                    searchParam: .constant(searchCtrl),
                    noCount: .constant(0)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            Text("\(data.parameter)")
                                .font(.subheadline)
                                .foregroundColor(Color("gray"))
                                .fontWeight(.semibold)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                            Image(systemName: "chevron.right")
                                .foregroundColor(.secondary)
                        }
                        .padding()
                    }
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
    
    var cardSuggest: some View {
        ForEach(listFaqSearch, id: \.idFAQ) { data in
            NavigationLink(
                destination: FaqDetailScreen(
                    data: .constant(data),
                    searchParam: .constant(searchCtrl),
                    noCount: .constant(0)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            Text("\(data.parameter)")
                                .font(.subheadline)
                                .foregroundColor(Color("gray"))
                                .fontWeight(.semibold)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                            Image(systemName: "chevron.right")
                                .foregroundColor(.secondary)
                        }
                        .padding()
                    }
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
    
    var searchCard: some View {
        HStack {
            HStack {
                TextField("", text: $searchCtrl) { changed in
                    print("changed in")
                    print("\($searchCtrl)")
                    searchFaq(searchParam: searchCtrl)
                } onCommit: {
                    print("\($searchCtrl)")
                    searchFaq(searchParam: searchCtrl)
                }.onChange(of: searchCtrl) { item in
                    print("onchange")
                    searchFaq(searchParam: searchCtrl)
                }
                .placeholder(when: searchCtrl.isEmpty) {
                        Text("Ketik Pertanyaan").foregroundColor(Color("gray"))
                }
                .foregroundColor(Color("gray"))
                
                Button(action: {
                    searchFaq(searchParam: searchCtrl)
                }, label: {
                    Image(systemName: "magnifyingglass")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(Color("red"))
                        .frame(width: 15, height: 15)
                })
                .frame(width: 15, height: 15)
                .padding(.horizontal, 10)
                .padding(.vertical, 10)
                .background(Color.white)
                .cornerRadius(5)
            }
            .frame(height: 10)
            .font(.subheadline)
            .padding(.vertical)
            .padding(.leading, 10)
            .padding(.trailing, 5)
            .background(Color("lighter_gray"))
            .cornerRadius(5)
            .padding(.horizontal, 20)
        }
        .padding([.top], 10)
    }
}

struct FaqByKategoriScreen_Previews: PreviewProvider {
    static var previews: some View {
        FaqByKategoriScreen(data: .constant(ListKategoriFAQResponseModelElement(id: 0, kode: "", kategori: "", lang: "", image: "", idImage: "", isPublish: 0)))
    }
}

extension FaqByKategoriScreen {
    
    func getDummyFaq() {
        if (data.kategori == "AKUN") {
            self.listFaq.append(
                Datum(
                    idFAQ: 0,
                    parameter: "Kenapa data saya tidak ditemukan dan diminta menghubungi ke Dukcapil?",
                    jawaban: "Pastikan KTP yang Anda gunakan adalah KTP elektronik dan data yang Anda masukkan sudah benar dan sesuai. Jika masih mengalami hal yang sama, Anda …",
                    kode: "",
                    kategori: "Registrasi Hak Akses"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 1,
                    parameter: "Kenapa saya tidak menerima kode verifikasi?",
                    jawaban: "Pastikan alamat email atau nomor WhatsApp Anda  benar dan aktif. Jika masih mengalami hal yang sama, Anda dapat mengulangi proses pendaftaran hak akses.",
                    kode: "",
                    kategori: "Registrasi Hak Akses"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 2,
                    parameter: "Untuk badan usaha, apakah boleh satu data  penanggung jawab yang sama digunakan untuk beberapa badan usaha?",
                    jawaban: "Boleh, Anda dapat menggunakan satu data  penanggung jawab yang sama untuk beberapa badan usaha.",
                    kode: "",
                    kategori: "Registrasi Hak Akses"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 3,
                    parameter: "Kenapa satu email hanya dapat digunakan untuk satu NIB?",
                    jawaban: "Pada sistem OSS yang baru memang diberlakukan aturan satu hak akses untuk satu pelaku usaha/ badan usaha. Pendekatan seperti ini lazim digunakan dalam berbagai layanan, seperti media sosial dan perdagangan elektronik (e-commerce). Khusus untuk pelaku UMK perseorangan, hak akses hanya membutuhkan nomor WhatsApp dan berlaku pendekatan yang sama, yaitu satu nomor WhatsApp untuk satu pelaku usaha.",
                    kode: "",
                    kategori: "Penggantian Hak Akses"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 4,
                    parameter: "Bagaimana kalau saya lupa password?",
                    jawaban: "Jangan khawatir, Anda dapat mengajukan penggantian password dengan mengklik Lupa Password? pada halaman Login/Masuk. Silakan masukkan username atau email Anda, khusus untuk pelaku UMK perseorangan memasukkan nomor WhatsApp.",
                    kode: "",
                    kategori: "Login"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 5,
                    parameter: "Kenapa saya tidak bisa login padahal sudah memasukkan username dan password yang benar?",
                    jawaban: "Apabila mengalami kejadian seperti ini, Anda dapat melakukan penggantian password dengan mengklik Lupa Password? pada halaman Login/Masuk. Silakan masukkan username atau email Anda, khusus untuk pelaku UMK perseorangan memasukkan nomor WhatsApp.",
                    kode: "",
                    kategori: "Login"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 6,
                    parameter: "Apakah email yang digunakan untuk hak akses dapat diganti?",
                    jawaban: "Tentu dapat diganti. Caranya dengan masuk/login ke sistem OSS, lalu klik nama Anda/ badan usaha di pojok kanan atas halaman beranda. Selanjutnya klik Lihat Profil dan pilih Ubah di bagian Email.",
                    kode: "",
                    kategori: "Profil"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 7,
                    parameter: "Bagaimana cara mengganti password?",
                    jawaban: "Caranya dengan masuk/login ke sistem OSS, lalu klik nama Anda/badan usaha di pojok kanan atas halaman beranda. Selanjutnya klik Lihat Profil dan pilih Ubah di bagian Password.",
                    kode: "",
                    kategori: "Profil"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 8,
                    parameter: "Bagaimana cara mengubah skala usaha?",
                    jawaban: "Caranya dengan masuk/login ke sistem OSS, lalu klik nama Anda/badan usaha di pojok kanan atas halaman beranda. Selanjutnya klik Lihat Profil dan pilih Ubah di bagian Skala Usaha.",
                    kode: "",
                    kategori: "Profil"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 9,
                    parameter: "Bagaimana apabila terjadi perubahan penanggung jawab badan usaha?",
                    jawaban: "Anda dapat mengubah data penanggung jawab badan usaha dengan cara masuk/login ke sistem OSS, lalu klik nama badan usaha di pojok kanan atas halaman beranda. Selanjutnya klik Lihat Profil dan pilih Ubah di bagian Nama Penanggung Jawab.",
                    kode: "",
                    kategori: "Profil"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 10,
                    parameter: "Apakah pelaku UMK perseorangan bisa menambahkan email sebagai hak akses, selain nomor WhatsApp?",
                    jawaban: "Bisa. Caranya dengan masuk/login ke sistem OSS, lalu klik nama/badan usaha Anda di pojok kanan atas halaman beranda. Selanjutnya klik Lihat Profil dan pilih Tambah di bagian Email.",
                    kode: "",
                    kategori: "Profil"
                ))
        }
        
        if (data.kategori == "PERSYARATAN DASAR") {
            self.listFaq.append(
                Datum(
                    idFAQ: 0,
                    parameter: "Apabila saya pelaku UMK dan telah memiliki perizinan berusaha, apakah usaha saya bebas berlokasi di mana saja?",
                    jawaban: "Sesuai dengan aturan yang berlaku, seluruh pelaku usaha diwajibkan untuk mengajukan Konfirmasi Kegiatan Pemanfaatan Ruang (dahulu namanya Izin Lokasi) sebelum memulai usaha. Namun, untuk Usaha Mikro dan Kecil, diberikan kemudahan dimana pelaku usaha cukup membuat surat pernyataan bahwa lokasi usahanya sudah sesuai dengan tata ruangnya. Jika di kemudian hari ditemukan bahwa terdapat ketidaksesuaian, maka pelaku usaha bersedia untuk dikenakan sanksi sesuai dengan peraturan yang berlaku.",
                    kode: "",
                    kategori: "KKPR Darat"
                ))
        }
        
        if (data.kategori == "PERIZINAN BERUSAHA") {
            self.listFaq.append(
                Datum(
                    idFAQ: 0,
                    parameter: "Saya sudah memiliki NIB dan izin usaha yang berlaku efektif, apakah perlu mengurus lagi?",
                    jawaban: "Tidak perlu melakukan penggantian karena izin yang sudah efektif pada OSS 1.1 tetap berlaku selama: \na. kegiatan usaha masih berjalan; atau \nb. belum kedaluwarsa (apabila izin memiliki masa berlaku).",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 1,
                    parameter: "Kenapa KBLI yang tercantum pada NIB tidak lengkap seperti pada sistem sebelumnya? Apakah masih bisa saya lihat?",
                    jawaban: "",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 2,
                    parameter: "Saya telah memiliki NIB pada sistem sebelumnya, kenapa hilang?",
                    jawaban: "",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 3,
                    parameter: "Saya sedang dalam proses pemenuhan komitmen pada sistem sebelumnya, bagaimana cara memprosesnya sekarang?",
                    jawaban: "Jika demikian, perizinan berusaha Anda belum berlaku efektif. Anda dapat memproses perizinan berusaha berbasis risiko untuk KBLI yang belum berlaku efektif melalui menu Perubahan Data Usaha pada dasbor pelaku usaha.",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 4,
                    parameter: "Bagaimana mengetahui produk/jasa yang wajib SNI dan/atau sertifikat halal?",
                    jawaban: "Setelah pelaku usaha melakukan pengisian data usaha (KBLI, skala usaha, lokasi usaha, dan daftar produk/jasa) maka sistem akan menampilkan informasi jika usaha tersebut wajib memiliki SNI dan/atau sertifikat halal",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 5,
                    parameter: "Bagaimana cara saya dapat mengetahui progres penerbitan perizinan berusaha yang sedang diajukan?",
                    jawaban: "Untuk mengetahui progres penerbitan perizinan berusaha yang sedang diajukan, pelaku usaha dapat menggunakan menu pelacakan pada dasbor setelah login. Di dalam menu pelacakan terdapat 3 sub-menu yaitu pelacakan perizinan berusaha, pelacakan perizinan dasar, dan pelacakan pengawasan (pencabutan dan pembatalan).",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 6,
                    parameter: "Apakah ada biaya untuk mengurus Perizinan Berusaha?",
                    jawaban: "Secara umum, tidak ada biaya yang dibebankan untuk pengurusan perizinan, namun ada beberapa perizinan berusaha, perizinan dasar, dan perizinan berusaha UMKU yang memerlukan pembayaran Pendapatan Negara Bukan Pajak (PNBP) yang nilai dan tata cara pembayarannya sudah ditetapkan oleh peraturan perundang-undangan yang berlaku",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 7,
                    parameter: "Apakah saya masih harus mengurus Izin Lokasi?",
                    jawaban: "Setelah disahkannya UU Cipta Kerja, Izin Lokasi sudah tidak ada dan diubah menjadi Konfirmasi Kesesuaian Pemanfaatan Ruang (KKPR) yang merupakan perizinan dasar sebelum mendapatkan perizinan berusaha. Namun untuk usaha Mikro dan Kecil mendapatkan kemudahan dimana KKPR-nya hanya berupa pernyataan mandiri saja.",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 8,
                    parameter: "Setelah mendapat UMK Risiko Rendah, apakah masih harus mengurus sertifikat halal? Bagaimana cara mengurusnya?",
                    jawaban: "Untuk UMK risiko rendah, NIB juga berlaku sebagai perizinan tunggal yang mencakup sertifikasi jaminan produk halal apabila produk/jasa termasuk kategori wajib halal sesuai ketentuan Peraturan perundang-undangan. NIB sudah dapat digunakan untuk kegiatan berusaha. Selanjutnya akan dilakukan pendampingan dari Badan Penyelenggara Jaminan Produk Halal untuk pengurusan Sertifikat Halal.",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 9,
                    parameter: "Kalau sudah memiliki NIB, apakah masih harus mengurus SIUP, TDP, dan SKU?",
                    jawaban: "Setelah disahkannya UU Cipta Kerja sudah tidak ada lagi SIUP, TDP, dan SKU. Seluruh perizinan berusaha diurus melalui sistem OSS.",
                    kode: "",
                    kategori: "Produk"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 10,
                    parameter: "Apakah ada jumlah maksimal KBLI yang dapat saya proses dalam 1 NIB?",
                    jawaban: "Tidak ada batasan jumlah KBLI yang dapat diproses dalam satu NIB, namun ada beberapa KBLI yang tidak boleh dimiliki secara bersamaan dalam satu NIB seperti KBLI perdagangan besar (KBLI dengan dua digit depan 46) dan perdagangan eceran (KBLI dengan dua digit depan 47). Selain itu ada KBLI yang hanya dapat diusahakan oleh satu badan usaha khusus untuk KBLI tersebut (KBLI single purpose) seperti KBLI 52297 (Jasa Keagenan Kapal/Agen Perkapalan Perusahaan Pelayaran)",
                    kode: "",
                    kategori: "Permohonan Baru"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 11,
                    parameter: "Bagaimana cara saya mengetahui tingkat risiko usaha saya?",
                    jawaban: "Pelaku usaha dapat melihat tingkat risiko usaha sebelum pengajuan perizinan berusaha melalui website https://oss.go.id/informasi/kbli-berbasis-risiko, kemudian lakukan pencarian melalui search box yang tersedia. Di dalam proses perizinan berusaha, tingkat risiko usaha akan otomatis muncul setelah pelaku usaha melakukan pengisian KBLI dan nilai modal usaha.",
                    kode: "",
                    kategori: "Permohonan Baru"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 12,
                    parameter: "Apakah perbedaan modal disetor dan modal usaha?",
                    jawaban: "Modal disetor merupakan modal yang disetorkan pemilik usaha ke dalam badan usaha untuk melakukan kegiatan usaha, sedangkan modal usaha adalah modal yang akan digunakan dalam kegiatan usaha, baik modal sendiri (perorangan atau badan usaha) maupun dari pembiayaan diluar tanah dan bangunan",
                    kode: "",
                    kategori: "Permohonan Baru"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 13,
                    parameter: "Untuk badan usaha, kenapa ada KBLI yang tidak dapat saya tambahkan?",
                    jawaban: "KBLI yang dapat ditambahkan dalam sistem OSS untuk badan usaha adalah KBLI yang sudah didaftarkan di dalam maksud dan tujuan oleh notaris ke dalam sistem AHU. Jika KBLI yang hendak dimasukkan oleh pelaku usaha tercantum di dalam akta namun tidak muncul di dalam sistem OSS, harap hubungi notaris karena kemungkinan besar KBLI tersebut tidak tercantum dalam sistem AHU",
                    kode: "",
                    kategori: "Permohonan Baru"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 14,
                    parameter: "Jika di akta masih menggunakan KBLI 2017 sedangkan di OSS saat ini menggunakan KBLI 2020, apakah harus mengganti akta?",
                    jawaban: "Tidak perlu dilakukan karena sistem OSS akan secara otomatis memetakan KBLI 2017 menjadi KBLI 2020. Apabila KBLI 2017 terbagi menjadi lebih dari 1 KBLI 2020, maka Anda dapat memilih salah satu atu lebih sesuai kegiatan usaha.",
                    kode: "",
                    kategori: "Permohonan Baru"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 15,
                    parameter: "Kenapa NIB saya belum terbit?",
                    jawaban: "Untuk usaha dengan skala non-Mikro dan Kecil, pelaku usaha wajib menyelesaikan perizinan dasar terlebih dahulu (KKPR dan persetujuan lingkungan) sebelum NIB dapat diterbitkan oleh sistem OSS. Pastikan pelaku usaha sudah mengunggah pemenuhan persyaratan perizinan dasar melalui sistem OSS untuk diverifikasi oleh Kementerian/Lembaga/Pemerintah Daerah/Administrator KEK/KPBP sesuai dengan kewenangannya",
                    kode: "",
                    kategori: "Permohonan Baru"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 16,
                    parameter: "Konversi KBLI saya tidak sesuai, apa yang harus saya lakukan?",
                    jawaban: "",
                    kode: "",
                    kategori: "Permohonan Baru"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 18,
                    parameter: "Apa perbedaan fitur pengembangan dan perluasan?",
                    jawaban: "Fitur pengembangan digunakan oleh pelaku usaha jika hendak menambahkan kegiatan usaha berupa penambahan KBLI atau penambahan lokasi usaha, sedangkan fitur perluasan digunakan jika ada penambahan kapasitas usaha untuk KBLI yang sama di lokasi yang sama",
                    kode: "",
                    kategori: "Pengembangan dan Perluasan"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 19,
                    parameter: "Bagaimana saya memperpanjang izin yang telah berakhir masa berlakunya?",
                    jawaban: "Untuk melakukan perpanjangan izin yang sudah habis masa berlakunya, pelaku usaha dapat menggunakan fitur perpanjangan yang tersedia di dalam dasbor sistem OSS pelaku usaha.",
                    kode: "",
                    kategori: "Perpanjangan"
                ))
        }
        
        if (data.kategori == "FASILITAS BERUSAHA") {
            self.listFaq.append(
                Datum(
                    idFAQ: 0,
                    parameter: "Data detail informasi apa saja yg harus disiapkan perusahaan untuk bisa mengajukan fasilitas impor?",
                    jawaban: "Dokumen atau data yang perlu disiapkan dalam pengajuan fasilitas adalah flow proses produksi, kalkulasi kapasitas produksi, layout mesin produksi dan rencana kerjasama dengan UMKM serta data-data mesin yang akan diimpor meliputi jenis barang, spesifikasi teknis, HS Code, jumlah, satuan, harga satuan dalam CIF US$, negara asal dan rencana pelabuhan bongkarnya. Untuk lebih lengkapnya dapat dilihat pada Peraturan BKPM No. 4 Tahun 2021",
                    kode: "",
                    kategori: "Fasilitas Impor/Masterlist"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 1,
                    parameter: "Apakah pengajuan fasilitas impor bisa diajukan tanpa melakukan proses perizinan berusaha, karena kami ada di luar kawasan industri dan validasi PKKPR cukup memakan waktu?",
                    jawaban: "Pengajuan fasilitas impor mengambil referensi dari perizinan berusaha sesuai tingkat risikonya. Untuk risiko rendah menggunakan NIB, menengah rendah menggunakan NIB + SS, menengah tinggi NIB + SS belum terverifikasi dan tinggi NIB + (Izin sementara untuk di kawasan). Untuk pengajuan fasilitas impor mesin agar menunggu persetujuan PKKPR terlebih dahulu.",
                    kode: "",
                    kategori: "Fasilitas Impor/Masterlist"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 2,
                    parameter: "Bagaimana cara membedakan kegiatan usaha mana yang akan diajukan fasilitas impor, mengingat kegiatan usaha kami lebih dari 1 (satu) dan kami kesulitan membedakan data kegiatan usaha satu dan lainnya?",
                    jawaban: "Di dalam menu fasilitas impor, ketika perusahaan mengajukan permohonan akan terlihat Nomor Kegiatan Usaha (NKU). NKU tersebut bisa dijadikan acuan bagi pelaku usaha untuk membedakan kegiatan usaha antara satu dan lainnya serta bisa digunakan untuk membedakan pengajuan fasilitas impornya.",
                    kode: "",
                    kategori: "Fasilitas Impor/Masterlist"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 3,
                    parameter: "Bagaimana proses pengajuan fasilitas impor bahan baku jika sebelumnya tidak menggunakan fasilitas impor mesin?",
                    jawaban: "Pelaku usaha dapat mengajukan fasilitas impor bahan baku jika menggunakan mesin produksi dengan TKDN mesin lebih dari 30% dibuktikan dengan surat keterangan dari Kementerian Perindustrian. Fasilitas impor bahan baku juga dapat diberikan jika pelaku usaha  menggunakan mesin produksi impor yang dibeli dari distributor lokal.",
                    kode: "",
                    kategori: "Fasilitas Impor/Masterlist"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 4,
                    parameter: "Apakah fasilitas impor/masterlist yang telah habis masa berlakunya dan masih terdapat mesin yang belum dilakukan pengimporan dapat diajukan kembali permohonannya agar dapat diberikan masterlist",
                    jawaban: "Pengajuan perpanjangan atas impor mesin dapat diajukan sepanjang pengajuan perpanjangan diajukan sebelum masa berlaku atas fasilitas impor tersebut berakhir. Adapun ketentuan kriteria perpanjangan mengacu pada Peraturan BKPM No. 4 Tahun 2021",
                    kode: "",
                    kategori: "Fasilitas Impor/Masterlist"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 5,
                    parameter: "Apakah nilai atas fasilitas impor/masterlist harus sesuai dengan nilai mesin yang akan dilakukan pengimporan?",
                    jawaban: "Nilai fasilitas impor/masterlist saat pengajuan permohonan sifatnya adalah perkiraan, namun nilai tersebut nantinya harus dapat mengcover nilai riil mesin yang diimpor yang tercantum dalam PIB. Nilai mesin masterlist yang sesuai dengan nilai impornya tersebut akan lebih baik dan memudahkan pelaku usaha saat melakukan clearence di Bea dan Cukai.",
                    kode: "",
                    kategori: "Fasilitas Impor/Masterlist"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 6,
                    parameter: "Bagaimana cara mengajukan fasilitas tax allowance?",
                    jawaban: "Pengajuan fasilitas tax allowance diajukan melalui sistem OSS dengan terlebih dahulu pelaku usaha melakukan proses perizinan berusaha dan setelah muncul notifikasi bahwa kegiatan usaha eligible fasilitas tax allowance, pelaku usaha dapat melanjutkan prosesnya pada menu fasilitas tax allowance. ",
                    kode: "",
                    kategori: "Tax Allowance"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 7,
                    parameter: "Dokumen apa saja yang perlu disiapkan dalam permohonan fasilitas tax allowance?",
                    jawaban: "Dokumen yang perlu disiapkan oleh pelaku usaha dalam pengajuan fasilitas tax allowance adalah rincian Aktiva tetap sesuai format dalam sistem OSS",
                    kode: "",
                    kategori: "Tax Allowance"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 8,
                    parameter: "Pada pemberian fasilitas tax allowance, selain pengurangan pajak penghasilan juga diberikan fasilitas lain seperti amortisasi dipercepat atau pengenaan pajak penghasilan atas deviden, terkait fasilitas ini apakah dapat kami manfaatkan semua atau harus dipilih salah satu?",
                    jawaban: "Semua fasilitas pada tax allowance dapat dimanfaatkan seluruhnya atau dapat dipilih oleh pelaku usaha",
                    kode: "",
                    kategori: "Tax Allowance"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 10,
                    parameter: "Bagaimana cara mengajukan fasilitas tax holiday?",
                    jawaban: "Pengajuan fasilitas tax holiday diajukan melalui sistem OSS dengan terlebih dahulu pelaku usaha melakukan proses perizinan berusaha dan setelah muncul notifikasi bahwa kegiatan usaha eligible fasilitas tax holiday, pelaku usaha dapat melanjutkan prosesnya pada menu fasilitas tax holiday.",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 11,
                    parameter: "Dokumen apa saja yang perlu disiapkan dalam permohonan fasilitas tax holiday?",
                    jawaban: "Dokumen yang perlu disiapkan oleh pelaku usaha dalam pengajuan fasilitas tax holiday adalah: \n1. Rincian Aktiva tetap sesuai format dalam sistem OSS \n2. Surat pernyataan Debt to equity ratio sesuai ketentuan perpajakan \n3. Perhitungan IRR dan Payback Period",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 12,
                    parameter: "Apakah kegiatan usaha yang kami lakukan bisa mengajukan fasilitas tax holiday?",
                    jawaban: "Kegiatan usaha dengan nilai investasi paling sedikit 100 Miliar dan termasuk dalam industri pionir berdasarkan lampiran Peraturan BKPM Nomor 7 Tahun 2020 tentang Tata Cara Pengajuan Fasilitas Penghasilan Badan dan Kriteria Industri Pionir dapat mengajukan fasilitas tax holiday",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 12,
                    parameter: "Kami sudah memilih bidang usaha yang termasuk dalam industri pionir berdasarkan lampiran Peraturan BKPM No 7 tahun 2020 tetapi mengapa kami tidak dapat mengajukan fasilitas tax holiday pada sistem OSS?",
                    jawaban: "Mohon dipastikan kembali data cakupan produk pada kegiatan usaha yang di input pada sistem OSS sudah sesuai dengan lampiran Peraturan BKPM No. 7 Tahun 2020 ",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 13,
                    parameter: "Apakah dapat diberikan lebih dari satu fasilitas tax holiday untuk perusahaan yang sama?",
                    jawaban: "Fasilitas tax holiday melekat kepada  kegiatan usaha yang dilakukan oleh perusahaan.",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 14,
                    parameter: "Apakah pelaku usaha dapat mengajukan permohonan fasilitas pajak penghasilan untuk Tax Allowance dan Tax Holiday secara bersamaan?",
                    jawaban: "Pelaku usaha hanya dapat memilih salah satu fasilitas pajak penghasilan yang ingin diajukan, Tax Allowance atau Tax Holiday saja",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 15,
                    parameter: "Bagaimana menentukan jangka waktu pemberian fasilitas tax holiday?",
                    jawaban: "Sesuai Peraturan Menteri Keuangan No 130 Tahun 2020, pemberian masa pemanfaatan tax holiday adalah berdasarkan realisasi atas nilai investasi aktiva tetap (tidak termasuk modal kerja), untuk skema pemberian masa pemanfaatan (Tahun) dapat dilihat pada Peraturan Menteri Keuangan No 130 Tahun 2020",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 16,
                    parameter: "Apakah ada batas pengajuan fasilitas tax holiday?",
                    jawaban: "Pengajuan fasilitas tax holiday dapat diajukan paling lambat 1 (satu) tahun sejak diterbitkannya perizinan berusaha berbasis risiko serta belum berproduksi komersial.",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 17,
                    parameter: "Apakah atas aset dengan status sewa kepada pihak lain dapat diajukan fasilitas tax holiday?",
                    jawaban: "Aset tetap yang diajukan fasilitas tax holiday harus atas milik perusahaan yang mengajukan fasilitas (bukan sewa)",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 18,
                    parameter: "Kami berencana untuk merelokasi pabrik dari luar negeri ke Indonesia, apakah atas kegiatan usaha ini kami dapat mengajukan fasilitas tax holiday?",
                    jawaban: "Atas kegiatan relokasi pabrik secara keseluruhan perusahaan dapat mengajukan fasilitas tax holiday sepanjang kegiatan usaha yang akan dilakukan memenuhi ketentuan dan kriteria pemberian fasilitas tax holiday",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 19,
                    parameter: "Apakah mesin bekas dapat diajukan fasilitas tax holiday?",
                    jawaban: "Mesin bekas tidak dapat diajukan fasilitas tax holiday kecuali perusahaan melakukan relokasi pabrik secara keseluruhan",
                    kode: "",
                    kategori: "Tax Holiday"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 20,
                    parameter: "Bagaimana  proses pengajuan fasilitas vokasi?",
                    jawaban: "Wajib Pajak melakukan penyampaian pemberitahuan melalui sistem OSS dengan melampirkan: \na. Perjanjian Kerja Sama; dan \nb. Surat Keterangan Fiskal yang masih berlaku.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 21,
                    parameter: "Apakah dengan mengikuti insentif super tax deduction indonesia maka Wajib Pajak akan menjadi obyek audit seperti jika mengajukan restitusi pajak?",
                    jawaban: "Tidak ada hubungan secara langsung antara pemeriksaan dengan pemanfaatan fasilitas. Pemeriksaan dilakukan dengan kriteria yang telah diatur dalam peraturan perundang-undangan antara lain adanya kelebihan bayar diatas jumlah tertentu.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 22,
                    parameter: "Kalau kita melakukan klaim apakah sudah pasti akan mendapat  memperoleh manfaat klaim dari biaya vokasi yang dikeluarkan?",
                    jawaban: "Wajib Pajak dapat diberikan pengurangan penghasilan bruto paling tinggi 200% (dua ratus persen) dari jumlah biaya yang dikeluarkan untuk kegiatan praktik kerja, pemagangan, dan/atau pembelajaran. Tambahan pengurang penghasilan bruto sebesar maksimal 100%, artinya dimungkinkan Wajib Pajak mendapatkan pengurang kurang dari 100% dari biaya vokasi yang dikeluarkan.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 23,
                    parameter: "Apakah prosedur untuk memperoleh pemotongan pajak 200% sama dengan pemotongan 100%?",
                    jawaban: "Prosedur pengajuan fasilitas Vokasi sama, adapun fasilitas dapat diberikan pengurangan penghasilan bruto paling tinggi 200% (dua ratus persen) dari jumlah biaya yang dikeluarkan untuk kegiatan praktik kerja, pemagangan, dan/atau pembelajaran.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 24,
                    parameter: "Perusahaan sudah melaksanakan kegiatan vokasi sejak lama, namun kompetensi untuk industri alas kaki tidak termasuk dalam PMK.",
                    jawaban: "Jika ada kompetensi yang belum masuk silahkan diusulkan melalui Kementerian teknis pembina sektornya.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 25,
                    parameter: "Penjelasan mengenai fasilitas yang dikaitkan dengan kegiatan pemagangan.",
                    jawaban: "Fasilitas Super Tax Deduction Vokasi dapat diberikan kepada Wajib Pajak yang telah melakukan kegiatan praktik kerja, pemagangan, dan/atau pembelajaran dalam rangka pembinaan dan pengembangan sumber daya manusia yang berbasis kompetensi tertentu;, Adapun syarat lainnya diatur di PMK 128/2019 pasal 2 ayat (3)",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 26,
                    parameter: "Di mana dapat ditemukan terkait daftar bidang keahlian yang dapat diajukan untuk insentif Super Tax Deduction kegiatan vokasi ini?",
                    jawaban: "Daftar kompetensi yang dapat diajukan mendapatkan fasilitas diatur Lampiran A PMK 128/2019 yang merupakan kompetensi yang terdaftar dan diajarkan oleh Lembaga vokasi dan atas kompetensi tersebut yang dilakukan kegiatan pemagangan dan /atau pembelajaran.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 27,
                    parameter: "Terkait kompetensi yang cukup banyak jumlahnya, apakah harus semua dimasukan dalam Perjanjian Kerja Sama?",
                    jawaban: "Kompetensi yang dimasukkan dalam perjanjian kerja sama adalah kompetensi yang tercantum pada PMK 128/2019 lampiran A dan merupakan kompetensi yang diajarkan pada Lembaga vokasinya.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 28,
                    parameter: "Perusahaan tidak masuk dalam sektor usaha yang bisa mengajukan fasilitas ini, bagaimana dengan hal ini?",
                    jawaban: "Sepanjang kompetensi kegiatan pemagangan/pembelajaran yang dilakukan sesuai dan syarat lainnya memenuhi , maka Wajib Pajak dapat memanfaatkan fasilitas tersebut.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 29,
                    parameter: "Tentang permohonan Super Tax Deduction, perusahaan harus diajukan kapan?",
                    jawaban: "dilakukan paling lambat sebelum dilakukannya kegiatan praktik kerja, pemagangan, dan/atau pembelajaran dalam rangka pembinaan dan pengembangan sumber daya manusia yang berbasis kompetensi tertentu dimulai.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 30,
                    parameter: "Apakah perusahaan perlu memberikan pemberitahuan ke direktorat jenderal pajak (DJP) saat perusahaan mengambil fasilitas tersebut?",
                    jawaban: "Wajib Pajak melakukan penyampaian pemberitahuan melalui sistem OSS dengan melampirkan: \na. Perjanjian Kerja Sama; dan \nb. Surat Keterangan Fiskal yang masih berlaku. dilakukan paling lambat sebelum dilakukannya kegiatan praktik kerja, pemagangan, dan/atau pembelajaran dalam rangka pembinaan dan pengembangan sumber daya manusia yang berbasis kompetensi tertentu dimulai.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 31,
                    parameter: "Apakah memerlukan surat pengajuan untuk melakukan aplikasi melalui Online Single Submission (OSS)?",
                    jawaban: "Wajib Pajak melakukan penyampaian pemberitahuan melalui sistem OSS dengan melampirkan: \na. Perjanjian Kerja Sama; dan \nb. Surat Keterangan Fiskal yang masih berlaku.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 32,
                    parameter: "Mohon penjelasan, apa yang dimaksud dengan Surat Keterangan Fiskal (SKF)?",
                    jawaban: "Surat Keterangan Fiskal adalah informasi yang diberikan oleh Direktorat Jenderal Pajak mengenai kepatuhan Wajib Pajak selama periode tertentu untuk memenuhi persyaratan memperoleh pelayanan atau dalam rangka pelaksanaan kegiatan tertentu. Adapun SKF bisa diperoleh secara online melalui akun DJPonline Wajib Pajak.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 33,
                    parameter: "Di mana kami bisa menemukan menu Online Single Submission (OSS)?",
                    jawaban: "Silahkan masuk website OSS (www.OSS.go.id), pilih menu fasilitas, kemudian pilih vokasi.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 34,
                    parameter: "Apakah ada surat yang diterima bahwa pengajuan kita disetujui?",
                    jawaban: "Tidak ada surat keputusan yang terbit. pemberitahuan dari direktorat jenderal pajak (DJP) bahwa Wajib Pajak dapat memanfaatkan fasilitas vokasi dalam bentuk notifikasi melalui online single submission (OSS).",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 35,
                    parameter: "Apabila perusahaan memilki beberapa mou/PKS, apakah bisa diajukan semua?",
                    jawaban: "Bisa semuanya diajukan asal PKS sudah memenuhi PMK no. 128/PMK.010/2019 pasal 7 ayat 3. namun pengajuan permohonan dilakukan untuk setiap PKS.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 36,
                    parameter: "Bagaimana jika perusahaan mempunyai kerja sama dengan beberapa SMK? apakah PKS nya sendiri sendiri atau bisa jadi satu?",
                    jawaban: "PKS harus dibuat terpisah untuk masing-masing SMK (Lembaga vokasi).",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 37,
                    parameter: "Apakah ada lembaga yang memvalidasi kegiatan vokasi ini?",
                    jawaban: "Kementerian dan/atau dinas terkait dapat melakukan evaluasi efektivitas pemberian pengurangan penghasilan bruto yang diterima Wajib Pajak. ketentuan tersebut diatur dalam PMK 128 pasal 10.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 38,
                    parameter: "Mitra yang bekerja sama siapa?",
                    jawaban: "Mitra Kerja samanya adalah SMK/MAK, Kampus Diploma atau Balai Latihan Kerja (BLK) yang ada di bawah Instansi Pemerintah.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 39,
                    parameter: "Kalau tidak ada balai latihan kerja (BLK) sekitar, apakah bisa membuat Perjanjian Kerja Sama (PKS) dilakukan dengan lembaga pelatihan keterampilan (LPK)?.",
                    jawaban: "Tidak bisa, karena tidak sesuai dengan PMK 128/PMK.010/2019, PKS harus dilakukan antara perusahaan dengan instansi yang menyelenggarakan urusan pemerintahan di bidang ketenagakerjaan atau dengan balai latihan milik pemerintah.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 40,
                    parameter: "Sertifikat dalam PMK no. 128/PMK.010/2019 ini apakah dapat dibuat oleh perusahaan? atau harus oleh lembaga sertifikat resmi?",
                    jawaban: "Biaya untuk kegiatan praktik kerja, pemagangan, dan/atau pembelajaran dalam rangka pembinaan dan pengembangan sumber daya manusia yang mendapatkan tambahan pengurangan penghasilan bruto \nmeliputi biaya: Biaya sertifikasi kompetensi bagi siswa, mahasiswa, peserta latih, perorangan yang tidak terikat hubungan kerja pihak manapun, pendidik/pelatih, tenaga kependidikan/kepelatihan, dan/atau instruktur yang merupakan peserta praktik kerja dan/atau pemagangan oleh lembaga yang memiliki kewenangan melakukan sertifikasi kompetensi sesuai peraturan perundang-undangan.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 41,
                    parameter: "Jika perusahaan bisa menjadi lembaga sertifikasi, dan muncul biaya sertifikasi apakah bisa disatukan dengan biaya vokasi?",
                    jawaban: "Bisa mendapatkan fasilitas Sepanjang bisa dibuktikan pembebanan biayanya dan  atas biaya tersebut sudah tercantum dalam PKS dan notifikasi.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 42,
                    parameter: "Apakah perlu dibuat kontrak antara perusahaan dengan badan sertifikasi?",
                    jawaban: "Diserahkan kepada Wajib Pajak, sepanjang bisa dibuktikan pengeluaran biaya sertifikasi sesuai PMK, maka biaya tersebut bisa mendapatkan fasilitas.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 43,
                    parameter: "Terkait biaya sertifikasi kompetensi peserta. apakah semua peserta harus mendapatkan sertifikasi?",
                    jawaban: "Diserahkan kepada Wajib Pajak yang menyelenggarakan kegiatan vokasi.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 44,
                    parameter: "Apa yang dimaksud dengan fasilitas pajak 200% ?",
                    jawaban: "Biaya yang timbul atas kegiatan vokasi yang sesuai dengan PMK no. 128/PMK.010/2019 dapat di bebankan secara fiskal sebesar maksimal 200%.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 45,
                    parameter: "Terkait biaya listrik dan air yang digunakan selama pemagangan bagaimana perhitungannya dan apakah bisa diajukan?",
                    jawaban: "Bisa diajukan selama biaya tersebut dapat dibuktikan penggunaannya untuk kegiatan vokasi",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 46,
                    parameter: "Untuk perkiraan jumlah peserta vokasi, perkiraan jumlah pegawai/ pihak lain yang ditugaskan dan perkiraan biaya, nantinya hanya dalam bentuk estimasi?",
                    jawaban: "Atas pengajuan kegiatan vokasi sifatnya estimasi namun pada pelaporan harus dilakukan secara riil.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 47,
                    parameter: "Dalam kegiatan pemagangan ada honorarium yang dikeluarkan perusahaan apakah dapat masuk dalam fasilitas?",
                    jawaban: "Biaya untuk kegiatan praktik kerja, pemagangan, dan/atau pembelajaran dalam rangka pembinaan dan pengembangan sumber daya manusia yang mendapatkan tambahan pengurangan penghasilan bruto sebagaimana dimaksud dalam Pasal 2 ayat (2) huruf b, meliputi biaya: honorarium atau pembayaran sejenis yang diberikan kepada siswa, mahasiswa, peserta latih, perorangan yang tidak terikat hubungan kerja pihak manapun, pendidik/pelatih, tenaga kependidikan/kepelatihan, dan/atau instruktur yang merupakan peserta praktik kerja dan/atau pemagangan;",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 48,
                    parameter: "Berapa jumlah maksimum dan minimum peserta yang diperbolehkan?",
                    jawaban: "Tidak diatur Batasan jumlah peserta ",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 49,
                    parameter: "Pemagangan dilakukan site/camp merauke, prakerin selama 2 bulan, pemagangan selama 6 bulan. untuk biaya seperti tempat tinggal, makan, masuk ke mana?",
                    jawaban: "Jenis biaya yang bisa mendapatkan fasilitas sudah diatur secara jelas di pasal 4.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 50,
                    parameter: "Perusahaan berencana membeli mesin khusus untuk kegiatan vokasi, apakah bisa biaya tersebut diklaim disertai dengan bukti pembeliannya?",
                    jawaban: "Pasal 4 huruf a mengatur : Biaya untuk kegiatan praktik kerja, pemagangan, dan/atau pembelajaran dalam rangka pembinaan dan pengembangan sumber daya manusia yang mendapatkan tambahan pengurangan penghasilan bruto sebagaimana dimaksud dalam Pasal 2 ayat (2) huruf b, meliputi biaya: penyediaan fasilitas fisik khusus berupa tempat pelatihan dan biaya penunjang fasilitas fisik khusus meliputi listrik, air, bahan bakar, biaya pemeliharaan, dan biaya terkait lainnya untuk keperluan pelaksanaan praktik kerja dan/atau kegiatan pemagangan; \nPasal 5 huruf a mengatur : Tambahan pengurangan penghasilan bruto sebagaimana dimaksud dalam Pasal 2 ayat (2) huruf b berlaku ketentuan sebagai berikut: Untuk biaya perolehan barang berwujud dan tidak berwujud terkait penyediaan fasilitas fisik sebagaimana dimaksud dalam Pasal 4 huruf a yang mempunyai masa manfaat lebih dari 1 (satu) tahun, tambahan pengurangan penghasilan bruto dihitung dari biaya penyusutan atau amortisasi barang berwujud dan tidak berwujud bersangkutan yang dibebankan pada saat bulan dilakukannya kegiatan praktik kerja dan/atau pemagangan sebagaimana dimaksud dalam Pasal 3 ayat (1). \nBiaya penyusutan mesin tersebut bisa diklaim sepanjang memenuhi pasal 4 huruf a dan pasal 5 huruf a.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 51,
                    parameter: "Bagaimana cara memperhitungan proporsional biaya untuk pelatihan/magang dan untuk produksi?",
                    jawaban: "Self Assesment Wajib Pajak",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 52,
                    parameter: "Bagaimana pelaporan pemanfaatan Super Tax Deduction (STD)?",
                    jawaban: "Pasal 8 ayat (1) PMK nomor 128/PMK.010/2019 mengatur : Wajib Pajak yang telah memanfaatkan tambahan pengurangan penghasilan bruto, wajib menyampaikan laporan biaya kegiatan praktik kerja, pemagangan, dan/atau pembelajaran dalam rangka pembinaan dan pengembangan sumber daya manusia yang berbasis kompetensi tertentu setiap tahun kepada Direktur Jenderal Pajak melalui Kepala Kantor Pelayanan Pajak tempat Wajib Pajak terdaftar paling lambat bersamaan dengan penyampaian Surat Pemberitahuan Tahunan Pajak Penghasilan badan Tahun Pajak pemanfaatan tambahan pengurangan penghasilan bruto. Pasal 8 ayat (2) mengatur : Laporan sebagaimana dimaksud pada ayat (1) disampaikan sesuai contoh format sebagaimana tercantum dalam Lampiran",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 53,
                    parameter: "Apakah pelaporan dilakukan di kantor pajak setempat? bagaimana dokumen pendukungnya?",
                    jawaban: "Pasal 8 ayat (1) mengatur : Wajib Pajak yang telah memanfaatkan tambahan pengurangan penghasilan bruto, wajib menyampaikan laporan biaya kegiatan praktik kerja, pemagangan, dan/atau pembelajaran dalam rangka pembinaan dan pengembangan sumber daya manusia yang berbasis kompetensi tertentu setiap tahun kepada Direktur Jenderal Pajak melalui Kepala Kantor Pelayanan Pajak tempat Wajib Pajak terdaftar paling lambat bersamaan dengan penyampaian Surat Pemberitahuan Tahunan Pajak Penghasilan badan Tahun Pajak pemanfaatan tambahan pengurangan penghasilan bruto. Adapun dokumen perusahaan disimpan sesuai ketentuan internal perusahaan.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 54,
                    parameter: "Apakah pelaporan fasilitas harus dilengkapi dengan dokumen pendukung?",
                    jawaban: "dokumen perusahaan disimpan sesuai ketentuan internal perusahaan.",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 55,
                    parameter: "Perihal penyampaian pelaporan, apakah ada format khusus yang bisa langsung diunduh terus dimasukkan dalam spt atau bagaimana?",
                    jawaban: "Perihal penyampaian pelaporan, apakah ada format khusus yang bisa langsung diunduh terus dimasukkan dalam spt atau bagaimana?",
                    kode: "",
                    kategori: "Vokasi/Super tax deduction "
                ))
        }
        
        if (data.kategori == "LKPM") {
            self.listFaq.append(
                Datum(
                    idFAQ: 0,
                    parameter: "Siapa saja yang wajib menyampaikan LKPM?",
                    jawaban: "Semua Pelaku Usaha, kecuali: \n1. Pelaku Usaha Mikro; \n2. Perusahaan di bidang usaha hulu migas, perbankan, Lembaga keuangan non bank dan asuransi; \n3. Perusahaan yang memiliki Izin Prinsip (IP), Pendaftaran Penanaman Modal (PI), dan/ atau Izin Usaha (IU) yang sudah tidak aktif atau sudah habis masa berlakunya;",
                    kode: "",
                    kategori: "Ketentuan"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 1,
                    parameter: "Apakah ada sanksi jika tidak menyampaikan LKPM?",
                    jawaban: "Pengenaan sanksi karena tidak menyampaikan LKPM tercantum pada Peraturan BKPM Nomor 5 Tahun 2021 tentang Pedoman dan Tata Cara Pengawasan Perizinan Berusaha Berbasis Risiko : Paragraf 1, (Peringatan Tertulis) pada Pasal 50-51",
                    kode: "",
                    kategori: "Ketentuan"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 2,
                    parameter: "Kapan saya harus melaporkan LKPM?",
                    jawaban: "Ketentuan penyampaian LKPM (Pasal 32 ayat (4) Peraturan BKPM No. 5 Tahun 2021: \n1. Bagi Pelaku Usaha kecil setiap 6 (enam) bulan dalam 1 (satu) tahun laporan; \n2. Bagi Pelaku Usaha menengah dan besar setiap 3 (tiga) bulan (triwulan). Jadwal bagi Pelaku Usaha menengah dan besar: Triwulan I Bulan Januari – Maret tanggal 1 – 10 April tahun berjalan Triwulan II Bulan April – Juni tanggal 1 – 10 Juli tahun berjalan Triwulan III Bulan Juli – September tanggal 1 – 10 Oktober tahun berjalan Triwulan IV Bulan Oktober – Desember tanggal 1 – 10 Januari tahun berikutnya",
                    kode: "",
                    kategori: "Ketentuan"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 3,
                    parameter: "Berapa banyak LKPM yang wajib disampaikan oleh Perusahaan?",
                    jawaban: "LKPM disampaikan pada setiap bidang usaha pada setiap lokasi proyek",
                    kode: "",
                    kategori: "Ketentuan"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 4,
                    parameter: "1. Apakah saya dapat menggunakan Hak Akses pada OSS Berbasis Risiko untuk melaporkan LKPM?",
                    jawaban: "Bisa, Anda dapat melaporkan LKPM melalui OSS Berbasis Risiko apabila anda sudah memiliki NIB. \nAnda dapat melaporkan LKPM melalui menu Pelaporan -> Laporan LKPM",
                    kode: "",
                    kategori: "Hak Akses"
                ))
            
            self.listFaq.append(
                Datum(
                    idFAQ: 5,
                    parameter: "2. Saya tidak bisa melaporkan LKPM melalui OSS Berbasis Risiko, apakah yang harus saya lakukan?",
                    jawaban: "Apabila Anda sudah memiliki hak akses lkpmonline.go.id Anda dapat langsung melaporkan LKPM melalui website tersebut. \nApabila Anda belum memiliki hak akses pada website tersebut, Anda dapat mengajukan hak akses LKPM Online dengan mengirimkan persyaratan berupa: \n1.   Akta Perusahaan \n2.   Surat kuasa (apabila dikuasakan) \n3.   Serta identitas pemberi kuasa (Direktur/Pimpinan Perusahaan) dan Penerima Kuasa  (KTP/Pasport). \n4.   NIB Semua dokumen dalam format pdf dan kirimkan via e-mail ke helpdesk.spipise@bkpm.go.id",
                    kode: "",
                    kategori: "Hak Akses"
                ))
        }
    }
    
    func searchFaq(searchParam: String) {
        self.listFaqSearch = self.listFaq.filter({ (data: Datum) -> Bool in
            return data.parameter.localizedStandardContains(searchCtrl)
        })
    }
}
