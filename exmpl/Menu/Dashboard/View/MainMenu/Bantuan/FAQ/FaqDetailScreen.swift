//
//  FaqDetailScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import SwiftUI
import AttributedText

struct FaqDetailScreen: View {
    
    @StateObject var bantuanVM = BantuanViewModel()
    @State var listFaq = [Datum]()
    
    @Binding var data: Datum
    @Binding var searchParam: String
    
    @State private var isYa: Bool = false
    @State private var isNo: Bool = false
    @State private var isLoading: Bool = false
    
    @State private var routeSendForm: Bool = false
    
    @Binding var noCount: Int
    @State private var noCountLocal: Int = 0
    @State private var rating: Int = 0
    
    let time = Date()
    let timeFormatter = DateFormatter()
    
    var body: some View {
        VStack(alignment: .leading) {
            
            if isLoading {
                LoadingView()
            }
            
            ScrollView {
                card
                    .padding(.horizontal, 20)
                    .padding(.top, 10)
                
                AttributedText("\(data.jawaban)")
                    .font(.subheadline)
                    .foregroundColor(Color("gray"))
                    .padding(.vertical, 20)
                    .padding(.horizontal, 20)
                
                likeButton
                
//                if isYa {
//                    HStack {
//                        VStack(alignment: .leading) {
//                            Text("Berikan penilaian Anda")
//                                .font(.subheadline)
//                                .foregroundColor(Color("gray"))
//                                .fontWeight(.semibold)
//                                .padding(.top, 10)
//                                .padding(.bottom, 5)
//
//                            RatingView(rating: $rating)
//                        }
//
//                        Spacer()
//                    }
//                    .padding(.horizontal, 20)
//                }
                
                if isNo {
                    if noCount >= 3 {
                        cardBantuan
                            .padding(.horizontal, 20)
                            .padding(.top, 10)
                    } else {
                        cardSuggest
                            .padding(.horizontal, 20)
                            .padding(.top, 10)
                    }
                }
                
                if (rating == 1 || rating == 2) {
                    VStack {
                        Button(action: {}, label: {
                            Text("Data perlu Diperbarui")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(Color("gray"))
                                .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                            
                        })
                        .disabled(true)
                        .background(Color("light_gray"))
                        .cornerRadius(5)
                        .padding(.top, 10)
                        .padding(.horizontal, 20)
                        
                        Button(action: {}, label: {
                            Text("Data tidak relevan")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(Color("gray"))
                                .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                            
                        })
                        .disabled(true)
                        .background(Color("light_gray"))
                        .cornerRadius(5)
                        .padding(.top, 10)
                        .padding(.bottom, 10)
                        .padding(.horizontal, 20)
                    }
                } else if (rating == 3 || rating == 4 || rating == 5) {
                    VStack {
                        Button(action: {}, label: {
                            Text("Data mudah dipahami")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(Color("gray"))
                                .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                            
                        })
                        .disabled(true)
                        .background(Color("light_gray"))
                        .cornerRadius(5)
                        .padding(.top, 10)
                        .padding(.horizontal, 20)
                        
                        Button(action: {}, label: {
                            Text("Data sangat membantu")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(Color("gray"))
                                .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                            
                        })
                        .disabled(true)
                        .background(Color("light_gray"))
                        .cornerRadius(5)
                        .padding(.top, 10)
                        .padding(.bottom, 10)
                        .padding(.horizontal, 20)
                    }
                }
            }
            
            if isYa {
                Spacer()
                // Button Kirim
                
                VStack {
                    // Button
                    Button(action: {
                        self.sendRating(
                            tanggal: getDate(),
                            idFaq: data.idFAQ,
                            bintang: 5
                        )
                    }, label: {
                        Text("Kirimkan")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(Color(disableButton ? "light_gray" : "white"))
                            .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                        
                    })
                    .background(Color(disableButton ? "gray" : "blue"))
                    .cornerRadius(5)
                    .padding(.top, 20)
                    .padding(.bottom, 10)
                    .padding(.horizontal, 20)
                    .disabled(disableButton)
                }
                .background(Color.white)
                .cornerRadius(10, corners: [.topLeft, .topRight])
                .shadow(color: .gray, radius: 10, x: 0, y: 0)
                .mask(Rectangle().padding(.top, -20))
                
            }

        }
        .navigationBarTitle("Bantuan", displayMode: .inline)
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .onAppear() {
            self.noCountLocal = self.noCount
        }
        .partialSheet(isPresented: self.$bantuanVM.showAlert) {
            VStack {
                
                Image("ic_success")
                
                Text("Terimakasih telah menggunakan Bantuan")
                    .foregroundColor(Color("gray"))
                    .font(.subheadline)
                    .fontWeight(.semibold)
                    .padding(.horizontal, 20)
                    .padding(.vertical, 20)
                
                // Button
                Button(action: {
                    self.bantuanVM.showAlert = false
                }, label: {
                    Text("Tutup")
                        .font(.subheadline)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                    
                })
                .background(Color("blue"))
                .cornerRadius(5)
                .padding(.top, 20)
                .padding(.bottom, 10)
                .padding(.horizontal, 20)
            }
        }
    }
    
    var cardSuggest: some View {
        ForEach(listFaq, id: \.idFAQ) { data in
            
            NavigationLink(
                destination: FaqDetailScreen(
                    data: .constant(data),
                    searchParam: .constant(""),
                    noCount: $noCountLocal),
                isActive: self.$routeSendForm) {
                
                EmptyView()
                
            }
            
            Button(
                action: {
                    print("Count", noCount)
                    self.noCountLocal += 1
                    self.routeSendForm = true
                },
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            Text("\(data.parameter)")
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .foregroundColor(Color("gray"))
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                            Image(systemName: "chevron.right")
                                .foregroundColor(.secondary)
                        }
                        .padding()
                    }
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
    
    var card: some View {
        VStack {
            HStack(spacing: 10) {
                Text("\(data.parameter)")
                    .font(.subheadline)
                    .fontWeight(.semibold)
                    .foregroundColor(Color("gray"))
                    .multilineTextAlignment(.leading)
                    .fixedSize(horizontal: false, vertical: true)
                
                Spacer()
            }
        }
    }
    
    var cardBantuan: some View {
        VStack(spacing: 10) {
            VStack {
                HStack(spacing: 10) {
                    
                    Image(systemName: "phone.fill")
                        .foregroundColor(Color("red"))
                    
                    Text("169")
                        .font(.subheadline)
                        .fontWeight(.semibold)
                        .foregroundColor(Color("gray"))
                        .multilineTextAlignment(.leading)
                        .fixedSize(horizontal: false, vertical: true)
                        .padding(.horizontal, 20)
                    
                    Spacer()
                    
                }
                .padding()
            }
            .background(Color.white)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color(.white), lineWidth: 1)
            )
            .cornerRadius(10)
            .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
            
            VStack {
                HStack(spacing: 10) {
                    
                    Image(systemName: "envelope.fill")
                        .foregroundColor(Color("red"))
                    
                    Text("kontak@oss.go.id")
                        .font(.subheadline)
                        .fontWeight(.semibold)
                        .foregroundColor(Color("gray"))
                        .multilineTextAlignment(.leading)
                        .fixedSize(horizontal: false, vertical: true)
                        .padding(.horizontal, 20)
                    
                    Spacer()
                    
                }
                .padding()
            }
            .background(Color.white)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color(.white), lineWidth: 1)
            )
            .cornerRadius(10)
            .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
            
            VStack {
                HStack(spacing: 10) {
                    
                    Image(systemName: "calendar")
                        .foregroundColor(Color("red"))
                    
                    Text("Konsultasi")
                        .font(.subheadline)
                        .fontWeight(.semibold)
                        .foregroundColor(Color("gray"))
                        .multilineTextAlignment(.leading)
                        .fixedSize(horizontal: false, vertical: true)
                        .padding(.horizontal, 20)
                    
                    Spacer()
                    
                }
                .padding()
            }
            .background(Color.white)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(Color(.white), lineWidth: 1)
            )
            .cornerRadius(10)
            .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
        }
    }
    
    var likeButton: some View {
        VStack(alignment: .leading) {
            Text("Apakah ini membantu ?")
                .font(.subheadline)
                .padding(.top, 10)
            
            HStack {
                // Button Ya
                Button(action: {
                    self.isYa = true
                    self.isNo = false
                    self.noCount = 0
                }, label: {
                    
                    Spacer()
                    
                    Image(isYa ? "ic_smile_white" : "ic_smile_red")
                    
                    Text("Ya")
                        .font(.subheadline)
                        .fontWeight(.bold)
                        .foregroundColor(isYa ? .white : Color("gray"))
                    
                    Spacer()
                    
                })
                .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                .background(Color(isYa ? "blue" : "white"))
                .cornerRadius(5)
                .padding(.top, 10)
                .padding(.bottom, 10)
                .padding(.trailing, 10)
                
                // Button Tidak
                Button(
                    action: {
                        getFaq(searchParam: searchParam)
                        self.isYa = false
                        self.isNo = true
                        self.rating = 0
                    },
                    label: {
                        Spacer()
                        Image(isNo ? "ic_unsmile_white" : "ic_unsmile_red")
                        
                        Text("Tidak")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(isNo ? .white : Color("gray"))
                        
                        Spacer()
                    })
                    .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                    .background(Color(isNo ? "blue" : "white"))
                    .cornerRadius(5)
                    .padding(.top, 10)
                    .padding(.bottom, 10)
                    .padding(.leading, 10)
                
            }
            .padding(.bottom, 10)
        }
        .padding(.horizontal, 20)
        .background(Color("light_gray"))
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.white), lineWidth: 1)
        )
        .cornerRadius(10)
        .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
        .padding(.horizontal, 20)
    }
}

struct FaqDetailScreen_Previews: PreviewProvider {
    
    static var previews: some View {
        FaqDetailScreen(data: .constant(Datum(idFAQ: 0, parameter: "Apa bedanya antara KPPA dan KP3A?", jawaban: "<p style=\"text-align:justify\">a.&nbsp;&nbsp; &nbsp;KPPA didirikan dalam rangka sebagai pengawas, penghubung, koordinator, dan mengurus kepentingan perusahaan atau perusahaan-perusahaan afiliasinya, serta mempersiapkan pendirian dan pengembangan usaha perusahaan PMA di Indonesia. (Keputusan Presiden Nomor 90 Tahun 2000 tentang Kantor Perwakilan Perusahaan Asing, dan Peraturan BKPM Nomor 4 Tahun 2021).</p>\r\n\r\n\r\n\r\n<p style=\"text-align:justify\">b.&nbsp;&nbsp; &nbsp;KP3A didirikan dalam rangka sebagai perwakilan perusahaan perdagangan asing yang dapat berbentuk Agen Penjualan (Selling Agent) dan/atau Agen Pabrik (Manufactures Agent) dan/atau Agen Pembelian (Buying Agent). (Peraturan Menteri Perdagangan No. 10/M-DAG/PER/3/2006).</p>", kode: "", kategori: "")), searchParam: .constant(""), noCount: .constant(0))
    }
}

extension FaqDetailScreen {
    
    var disableButton: Bool {
        self.isLoading
    }

    func getDate()-> String{
     let time = Date()
     let timeFormatter = DateFormatter()
     timeFormatter.dateFormat = "yyyy-MM-dd"
     let stringDate = timeFormatter.string(from: time)
     return stringDate
    }
    
    func sendRating(tanggal: String, idFaq: Int, bintang: Int) {
        self.bantuanVM.showAlert = true
//        var idFeedback: Int = 1
//
//        if (rating == 2 || rating == 3) {
//            idFeedback = 2
//        }
//
//        if (rating == 4) {
//            idFeedback = 3
//        }
//
//        if (rating == 5) {
//            idFeedback = 4
//        }
//
//        self.bantuanVM.sendRating(tanggal: tanggal, idFaq: idFaq, bintang: bintang, idFeedback: idFeedback) { success in
//
//            if success {
//                self.isLoading = false
//            }
//
//            if !success {
//                self.isLoading = false
//            }
//        }
        
    }
    
    func getFaq(searchParam: String) {
        self.bantuanVM.faq(searchParam: searchParam) { success in
            
            if success {
                self.listFaq = self.bantuanVM.faqList
            }
            
            if !success {
                
            }
            
        }
    }
}
