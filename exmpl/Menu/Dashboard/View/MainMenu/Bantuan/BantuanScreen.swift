//
//  BantuanScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import SwiftUI

struct BantuanScreen: View {
    
    @State private var searchCtrl: String = ""
    
    @StateObject var bantuanVM = BantuanViewModel()
    @State var listFaq = [Datum]()
    @State var listKbli = [KbliModel]()
    
    var body: some View {
        VStack {
            
            NavigationLink(destination: EmptyView()) {
                EmptyView()
            }
            
            searchCard
            
            if searchCtrl.isEmpty {
                FaqScreen()
            } else {
                ScrollView {
                    cardFaq
                        .padding(.horizontal, 20)
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationBarTitle("Bantuan", displayMode: .inline)
    }
    
    var searchCard: some View {
        HStack {
            HStack {
                TextField("", text: $searchCtrl) { changed in
                    print("changed in")
                    print("\($searchCtrl)")
                    getFilter()
                } onCommit: {
                    print("\($searchCtrl)")
                    getFilter()
                }.onChange(of: searchCtrl) { item in
                    print("onchange")
                    getFilter()
                }
                .placeholder(when: searchCtrl.isEmpty) {
                        Text("Ketik Pertanyaan").foregroundColor(Color("gray"))
                }
                .foregroundColor(Color("gray"))
                
                Button(action: {
                    getFilter()
                }, label: {
                    Image(systemName: "magnifyingglass")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(Color("red"))
                        .frame(width: 15, height: 15)
                })
                .frame(width: 15, height: 15)
                .padding(.horizontal, 10)
                .padding(.vertical, 10)
                .background(Color.white)
                .cornerRadius(5)
            }
            .frame(height: 10)
            .font(.subheadline)
            .padding(.vertical)
            .padding(.leading, 10)
            .padding(.trailing, 5)
            .background(Color("lighter_gray"))
            .cornerRadius(5)
            .padding(.horizontal, 20)
        }
        .padding([.top], 10)
    }
    
    var cardFaq: some View {
        ForEach(listFaq, id: \.idFAQ) { data in
            NavigationLink(
                destination: FaqDetailScreen(
                    data: .constant(data),
                    searchParam: .constant(searchCtrl),
                    noCount: .constant(0)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            Text("\(data.parameter)")
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .foregroundColor(Color("gray"))
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                            Image(systemName: "chevron.right")
                                .foregroundColor(.secondary)
                        }
                        .padding()
                    }
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
    
    var cardKbli: some View {
        ForEach(listKbli, id: \.kode) { data in
            NavigationLink(
                destination: KbliSecondDetailScreen(
                    kategori: .constant(data.kategori),
                    kode: .constant(data.kode),
                    title: .constant(data.judul),
                    uraian: .constant(data.uraian)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            
                            Text("\(data.kode)")
                                .foregroundColor(.white)
                                .fontWeight(.bold)
                                .padding()
                                .background(Color("red"))
                                .cornerRadius(5)
                            
                            Text("\(data.judul)")
                                .font(.headline)
                                .foregroundColor(.secondary)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                        }
                        .padding()
                    }
                    .multilineTextAlignment(.leading)
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
}

struct BantuanScreen_Previews: PreviewProvider {
    static var previews: some View {
        BantuanScreen()
    }
}

extension BantuanScreen {
    
    func getFaq(searchParam: String) {
        self.bantuanVM.faq(searchParam: searchParam) { success in
            
            if success {
                self.listFaq = self.bantuanVM.faqList
            }
            
            if !success {
                
            }
            
        }
    }
    
    func getKbli(searchParam: String) {
        self.bantuanVM.kbli(searchParam: searchParam, kategori: "", kode: "") { success in
            
            if success {
                self.listKbli = self.bantuanVM.kbliList
            }
            
            if !success {
                
            }
        }
    }
    
    func getFilter() {
        getFaq(searchParam: searchCtrl)
    }
}
