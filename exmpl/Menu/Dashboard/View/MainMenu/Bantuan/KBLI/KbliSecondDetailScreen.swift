//
//  KbliSecondDetailScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import SwiftUI
import AttributedText

struct KbliSecondDetailScreen: View {
    
    @StateObject var bantuanVM = BantuanViewModel()
    @State var listKbli = [KbliModel]()
    @State var listKbliUmku = [KbliUmku]()
    @State var listKbliResiko = [KbliResiko]()
    @State var listKbliParent = [KbliParentModel]()
    @State var listKbliKkSyarat = [KbliKkSyarat]()
    @State private var isLoading: Bool = true
    
    @Binding var kategori: String
    @Binding var kode: String
    @Binding var title: String
    @Binding var uraian: String
    
    var body: some View {
        VStack {
            if isLoading {
                LoadingView()
            }
            
            ScrollView {
                
                if listKbliKkSyarat.count > 0 {
                    cardKbliInfoSyarat
                }
                
                // Field Uraian
                VStack(alignment: .leading, spacing: 20) {
                    
                    HStack {
                        Text("\(kategori) - \(title)")
                            .font(.headline)
                            .foregroundColor(Color("gray"))
                        
                        Spacer()
                    }
                    .padding(.horizontal, 20)
                    
                    HStack {
                        Text("Uraian")
                            .font(.headline)
                            .foregroundColor(Color("blue"))
                        
                        Spacer()
                    }
                    .padding(.horizontal, 20)
                    
                    AttributedText(self.uraian)
                        .font(.subheadline)
                        .foregroundColor(Color("gray"))
                        .padding(.horizontal, 20)
                }
                .padding(.top, 10)
                .padding(.bottom, 20)
                
                // Field KBLI Resiko
                if (listKbliResiko.count > 0) {
                    VStack(alignment: .leading, spacing: 20) {
                        HStack {
                            Text("Ruang Lingkup")
                                .font(.headline)
                                .foregroundColor(Color("blue"))
                            
                            Spacer()
                        }
                        
                        cardKbliResiko
                    }
                    .padding(.horizontal, 20)
                    .padding(.bottom, 20)
                }
                
                // Field KBLI Umku
                if (listKbliUmku.count > 0) {
                    VStack(alignment: .leading, spacing: 20) {
                        HStack {
                            Text("UMKU")
                                .font(.headline)
                                .foregroundColor(Color("blue"))
                            
                            Spacer()
                        }
                        
                        cardKbliUmku
                    }
                    .padding(.horizontal, 20)
                    .padding(.bottom, 20)
                }
                
                // Field Parent
                VStack(alignment: .leading, spacing: 20) {
                    HStack {
                        Text("Sebelumnya")
                            .font(.headline)
                            .foregroundColor(Color("blue"))
                        
                        Spacer()
                    }
                    
                    cardParent
                }
                .padding(.horizontal, 20)
                .padding(.bottom, 20)
                
                // Field Turunan
                VStack(alignment: .leading, spacing: 20) {
                    
                    if (listKbli.count > 0) {
                        HStack {
                            Text("Turunan")
                                .font(.headline)
                                .foregroundColor(Color("blue"))
                            
                            Spacer()
                        }
                    }
                    
                    card
                }
                .padding(.horizontal, 20)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .navigationBarTitle("KBLI 2020 - \(kode)", displayMode: .inline)
        .onAppear() {
            getKbli(kategori: kategori, kode: kode)
            getKbliUmku(kbli: kode)
            getKbliResiko(kbli: kode)
        }
    }
    
    var cardKbliInfoSyarat: some View {
        ForEach(listKbliKkSyarat, id: \.jnsBu) { data in
            VStack {
                HStack(alignment: .top) {
                    Image(systemName: "info.circle.fill")
                        .foregroundColor(Color("blue"))
                        .padding(.vertical, 10)
                        .padding(.leading, 10)
                    
                    AttributedText("\(data.uraian)")
                        .foregroundColor(Color("gray"))
                        .font(.caption)
                        .multilineTextAlignment(.leading)
                        .fixedSize(horizontal: false, vertical: true)
                        .padding(.vertical, 10)
                    
                    Spacer()
                }
            }
            .background(Color("light_blue"))
            .cornerRadius(5)
            .padding(.horizontal, 10)
            .padding(.top, 20)
        }
    }
    
    var cardParent: some View {
        ForEach(listKbliParent, id: \.kode) { data in
            NavigationLink(destination: KbliFirstDetailScreen(
                            kategori: .constant(kategori),
                            title: .constant(data.judul),
                            uraian: .constant(data.uraian)) , label: {
                VStack {
                    HStack(spacing: 10) {
                        
                        Text("\(data.kode)")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                            .foregroundColor(Color("gray"))
                            .padding()
                        
                        Text("\(data.judul)")
                            .font(.subheadline)
                            .fontWeight(.semibold)
                            .foregroundColor(Color("gray"))
                            .multilineTextAlignment(.leading)
                            .fixedSize(horizontal: false, vertical: true)
                        
                        Spacer()
                        
                    }
                    .padding(5)
                }
                .background(Color.white)
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color(.white), lineWidth: 1)
                )
                .cornerRadius(10)
                .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
            })

        }
    }
    
    var card: some View {
        ForEach(listKbli, id: \.kode) { data in
            NavigationLink(
                destination: KbliSecondDetailScreen(
                    kategori: .constant(data.kategori),
                    kode: .constant(data.kode),
                    title: .constant(data.judul),
                    uraian: .constant(uraian)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            
                            Text("\(data.kode)")
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .foregroundColor(Color("gray"))
                                .padding()
                            
                            Text("\(data.judul)")
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .foregroundColor(Color("gray"))
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                        }
                        .padding(5)
                    }
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
    
    var cardKbliUmku: some View {
        ForEach(listKbliUmku, id: \.idLic) { data in
            Collapsible { () -> Text in
                Text("\(data.namaDokumen)")
            } content: {
                VStack {
                    HStack(alignment: .top) {
                        Text("•")
                            .foregroundColor(Color("gray"))

                        Group {
                            VStack(alignment: .leading, spacing: 5) {
                                VStack(alignment: .leading) {
                                    Text("Parameter")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))
                                    
                                    ForEach(dictionaryToAny(data: data.parameter ?? ["": ""]), id:\.self) { val in
                                        
                                        Text("\(val)")
                                            .font(.caption)
                                            .fontWeight(.semibold)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                                
                                VStack(alignment: .leading) {
                                    Text("Kewenangan")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))
                                    
                                    ForEach(dictionaryToAny(data: data.kewenangan ?? ["": ""]), id:\.self) { val in
                                        
                                        Text("\(val)")
                                            .font(.caption)
                                            .fontWeight(.semibold)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                                
                                VStack(alignment: .leading) {
                                    Text("Persyaratan")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))
                                    
                                    ForEach(dictionaryToAny(data: data.persyaratan ?? ["": ""]), id:\.self) { val in
                                        
                                        AttributedText(val)
                                            .font(.caption)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                                
                                VStack(alignment: .leading) {
                                    Text("Kewajiban")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))
                                    
                                    ForEach(dictionaryToAny(data: data.kewajiban ?? ["": ""]), id:\.self) { val in
                                        
                                        AttributedText(val)
                                            .font(.caption)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                            }
                        }

                        Spacer()
                    }
                    .padding(.top, 10)
                    
                    Divider()
                        .foregroundColor(Color("green"))
                }
                .padding(.horizontal, 20)
            }

        }
    }
    
    var cardKbliResiko: some View {
        ForEach(listKbliResiko, id: \.idRuangLingkup) { data in

            Collapsible { () -> Text in
                Text("\(data.judulRuangLingkup)")
            } content: {
                VStack {
                    VStack {
                        HStack(alignment: .top) {
                            Image(systemName: "info.circle.fill")
                                .foregroundColor(Color("blue"))
                                .padding(.vertical, 10)
                                .padding(.leading, 10)

                            AttributedText("REGULASI: \(data.judulRegulasi)")
                                .foregroundColor(Color("blue"))
                                .font(.caption)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                                .padding(.vertical, 10)

                            Spacer()
                        }
                    }
                    
                    .background(Color("light_blue"))
                    .cornerRadius(5)
                    .padding(.horizontal, 10)

                    HStack(alignment: .top) {
                        Text("•")
                            .foregroundColor(Color("gray"))

                        Group {
                            VStack(alignment: .leading, spacing: 5) {
                                VStack(alignment: .leading) {
                                    Text("Skala")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))
                                    
                                    Text("\(data.resiko.first?.skala ?? "-")")
                                        .font(.caption)
                                        .fontWeight(.semibold)
                                        .foregroundColor(Color("gray"))
                                }
                                VStack(alignment: .leading) {
                                    Text("Luas Lahan")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))
                                    
                                    ForEach(data.resiko.prefix(1), id:\.kode) { valResiko in
                                        Text("\(valResiko.luasLahan ?? "")")
                                            .font(.caption)
                                            .fontWeight(.semibold)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                                VStack(alignment: .leading) {
                                    Text("Tingkat Risiko")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))
                                    
                                    ForEach(data.resiko.prefix(1), id:\.kode) { valResiko in
                                        Text("\(valResiko.tingkatResiko ?? "")")
                                            .font(.caption)
                                            .fontWeight(.semibold)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                                VStack(alignment: .leading) {
                                    Text("Jangka Waktu")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))
                                    
                                    ForEach(data.resiko.prefix(1), id:\.kode) { valResiko in
                                        Text("\(valResiko.jangkaWaktu ?? "")")
                                            .font(.caption)
                                            .fontWeight(.semibold)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                                VStack(alignment: .leading) {
                                    Text("Masa Berlaku")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))

                                    ForEach(data.resiko.prefix(1), id:\.kode) { valResiko in
                                        Text("\(valResiko.masaBerlaku ?? "")")
                                            .font(.caption)
                                            .fontWeight(.semibold)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }

                                }
                                VStack(alignment: .leading) {
                                    Text("Kewenangan")
                                        .font(.caption)
                                        .foregroundColor(Color("gray"))
                                    
                                    ForEach(data.resiko, id:\.kode) { valResiko in
                                        ForEach(dictionaryToAny(data: valResiko.kewenangan ?? ["": ""]), id:\.self) { val in
                                            
                                            Text("\(val)")
                                                .font(.caption)
                                                .fontWeight(.semibold)
                                                .foregroundColor(Color("gray"))
                                                .fixedSize(horizontal: false, vertical: true)
                                        }
                                    }
                                }

                                VStack(alignment: .leading) {
                                    Text("Persyaratan perizinan berusaha:")
                                        .font(.caption)
                                        .fontWeight(.semibold)
                                        .foregroundColor(Color("blue"))
                                    
                                    ForEach(data.resiko, id:\.kode) { valResiko in
                                        ForEach(dictionaryToAny(data: valResiko.persyaratan ?? ["": ""]), id:\.self) { val in
                                            
                                            Text("\(val)")
                                                .font(.caption)
                                                .fontWeight(.semibold)
                                                .foregroundColor(Color("gray"))
                                                .fixedSize(horizontal: false, vertical: true)
                                        }
                                    }
                                }
                                .padding(.top, 10)
                                
                                VStack(alignment: .leading) {
                                    Text("Jangka waktu pemenuhan persyaratan:")
                                        .font(.caption)
                                        .fontWeight(.semibold)
                                        .foregroundColor(Color("blue"))
                                    
                                    ForEach(data.resiko.prefix(1), id:\.kode) { valResiko in
                                        Text("\(valResiko.jangkaWaktuPersyaratan?.first ?? "")")
                                            .font(.caption)
                                            .fontWeight(.semibold)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                                .padding(.top, 10)
                                
                                VStack(alignment: .leading) {
                                    Text("Kewajiban perizinan berusaha:")
                                        .font(.caption)
                                        .fontWeight(.semibold)
                                        .foregroundColor(Color("blue"))
                                    
                                    ForEach(data.resiko, id:\.kode) { valResiko in
                                        ForEach(dictionaryToAny(data: valResiko.kewajiban ?? ["": ""]), id:\.self) { val in
                                            
                                            Text("\(val)")
                                                .font(.caption)
                                                .fontWeight(.semibold)
                                                .foregroundColor(Color("gray"))
                                                .fixedSize(horizontal: false, vertical: true)
                                        }
                                    }
                                }
                                .padding(.top, 10)
                                
                                VStack(alignment: .leading) {
                                    Text("Jangka waktu pemenuhan kewajiban:")
                                        .font(.caption)
                                        .fontWeight(.semibold)
                                        .foregroundColor(Color("blue"))
                                    
                                    ForEach(data.resiko.prefix(1), id:\.kode) { valResiko in
                                        Text("\(valResiko.jangkaWaktuKewajiban?.first ?? "")")
                                            .font(.caption)
                                            .fontWeight(.semibold)
                                            .foregroundColor(Color("gray"))
                                            .fixedSize(horizontal: false, vertical: true)
                                    }
                                }
                                .padding(.top, 10)
                            }
                        }

                        Spacer()
                    }
                    .padding(.top, 10)
                    .padding(.horizontal, 20)
                    
                    Divider()
                        .foregroundColor(Color("green"))
                }
            }
        }
    }
}

struct KbliSecondDetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        KbliSecondDetailScreen(kategori: .constant("A"), kode: .constant("01"), title: .constant(""), uraian: .constant(""))
    }
}

extension KbliSecondDetailScreen {
    
    func dictionaryToAny(data: [String: Any]) -> [String] {
        
        data.compactMap({ (key, value) -> String in
            return "\(key)"
        })
    }
    
    func getKbli(kategori: String, kode: String) {
        self.bantuanVM.kbli(searchParam: "", kategori: kategori, kode: kode) { success in
            
            if success {
                self.listKbli = self.bantuanVM.kbliList
                self.listKbliParent = self.bantuanVM.kbliParentList
                self.listKbliKkSyarat = self.bantuanVM.kbliInfoKkSyarat
                self.isLoading = false
            }
            
            if !success {
                self.isLoading = false
            }
            
        }
    }
    
    func getKbliUmku(kbli: String) {
        self.bantuanVM.kbliUmku(searchParam: "", kbli: kbli) { success in
            
            if success {
                self.listKbliUmku = self.bantuanVM.kbliUmkuList
            }
            
            if !success {
                
            }
        }
    }
    
    func getKbliResiko(kbli: String) {
        self.bantuanVM.kbliResiko(kbli: kbli) { success in
            
            if success {
                self.listKbliResiko = self.bantuanVM.kbliResikoList
            }
            
            if !success {
                
            }
        }
    }
}
