//
//  KbliFirstDetailScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 14/11/21.
//

import SwiftUI
import AttributedText

struct KbliFirstDetailScreen: View {
    
    @StateObject var bantuanVM = BantuanViewModel()
    @State var listKbli = [KbliModel]()
    @State private var isLoading: Bool = true
    
    @Binding var kategori: String
    @Binding var title: String
    @Binding var uraian: String
    
    var body: some View {
        VStack {
            
            if isLoading {
                LoadingView()
            }
            
            ScrollView {
                // Field Uraian
                VStack(alignment: .leading, spacing: 20) {
                    
                    HStack {
                        Text("\(kategori) - \(title)")
                            .font(.headline)
                            .foregroundColor(Color("gray"))
                        
                        Spacer()
                    }
                    .padding(.horizontal, 20)
                    
                    HStack {
                        Text("Uraian")
                            .font(.headline)
                            .foregroundColor(Color("blue"))
                        
                        Spacer()
                    }
                    .padding(.horizontal, 20)
                    
                    AttributedText(self.uraian)
                        .font(.subheadline)
                        .foregroundColor(Color("gray"))
                        .padding(.horizontal, 20)
                }
                .padding(.top, 10)
                .padding(.bottom, 20)
                
                // Field Turunan
                VStack(alignment: .leading, spacing: 20) {
                    HStack {
                        Text("Turunan")
                            .font(.headline)
                            .foregroundColor(Color("blue"))
                        
                        Spacer()
                    }
                    
                    card
                }
                .padding(.horizontal, 20)
            }
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .navigationBarTitle("KBLI 2020 - \(kategori)", displayMode: .inline)
        .onAppear() {
            getKbli(kategori: kategori)
        }
    }
    
    var card: some View {
        ForEach(listKbli, id: \.kode) { data in
            NavigationLink(
                destination: KbliSecondDetailScreen(kategori: .constant(data.kategori), kode: .constant(data.kode), title: .constant(data.judul), uraian: .constant(data.uraian)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            
                            Text("\(data.kode)")
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .foregroundColor(Color("gray"))
                                .padding()
                            
                            Text("\(data.judul)")
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .foregroundColor(Color("gray"))
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                        }
                        .padding(5)
                    }
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
}

struct KbliFirstDetailScreen_Previews: PreviewProvider {
    static var previews: some View {
        KbliFirstDetailScreen(kategori: .constant("A"), title: .constant("Pertanian, Perhutanan"), uraian: .constant(""))
    }
}

extension KbliFirstDetailScreen {
    
    func getKbli(kategori: String) {
        self.bantuanVM.kbli(searchParam: "", kategori: kategori, kode: "") { success in
            
            if success {
                self.listKbli = self.bantuanVM.kbliList
                self.isLoading = false
            }
            
            if !success {
                self.isLoading = false
            }
            
        }
    }
    
}
