//
//  KbliScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 13/11/21.
//

import SwiftUI

struct KbliScreen: View {
    
    @State private var searchCtrl: String = ""
    
    @StateObject var bantuanVM = BantuanViewModel()
    @State var listKbli = [KbliModel]()
    @State var listKbliSearch = [KbliModel]()
    
    let testo : String = "There is a thunderstorm in the area. Added some testing long text to demo that wrapping works correctly!"
    
    var body: some View {
        VStack {
            
            searchCard
            
            ScrollView {
                
                HStack {
                    Text("KLASIFIKASI BAKU LAPANGAN USAHA INDONESIA (KBLI) 2020")
                        .font(.headline)
                        .foregroundColor(Color("blue"))
                        .padding(.horizontal, 20)
                    
                    Spacer()
                }
                .padding(.vertical, 10)
                
                // KBLI Card
                if searchCtrl.isEmpty {
                    card
                        .padding(.horizontal, 20)
                } else {
                    cardSuggest
                        .padding(.horizontal, 20)
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .navigationBarTitle("Bantuan", displayMode: .inline)
        .padding(.top, 10)
        .onAppear() {
            getKbli(searchParam: "")
        }
    }
    
    var searchCard: some View {
        HStack {
            HStack {
                TextField("", text: $searchCtrl) { changed in
                    print("changed in")
                    print("\($searchCtrl)")
                    getKbliSearch(searchParam: searchCtrl)
                } onCommit: {
                    print("\($searchCtrl)")
                    getKbliSearch(searchParam: searchCtrl)
                }.onChange(of: searchCtrl) { item in
                    print("onchange")
                    getKbliSearch(searchParam: searchCtrl)
                }
                .placeholder(when: searchCtrl.isEmpty) {
                        Text("Cari bidang usaha/kode KBLI 2020").foregroundColor(Color("gray"))
                }
                .foregroundColor(Color("gray"))
                
                Button(action: {
                    getKbliSearch(searchParam: searchCtrl)
                }, label: {
                    Image(systemName: "magnifyingglass")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(Color("red"))
                        .frame(width: 15, height: 15)
                })
                .frame(width: 15, height: 15)
                .padding(.horizontal, 10)
                .padding(.vertical, 10)
                .background(Color.white)
                .cornerRadius(5)
            }
            .frame(height: 10)
            .font(.subheadline)
            .padding(.vertical)
            .padding(.leading, 10)
            .padding(.trailing, 5)
            .background(Color("lighter_gray"))
            .cornerRadius(5)
            .padding(.horizontal, 20)
        }
        .padding([.top], 10)
    }
    
    var card: some View {
        ForEach(listKbli, id: \.kode) { data in
            NavigationLink(
                destination: KbliFirstDetailScreen(
                    kategori: .constant(data.kategori),
                    title: .constant(data.judul),
                    uraian: .constant(data.uraian)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            
                            Text("\(data.kode)")
                                .foregroundColor(.white)
                                .fontWeight(.bold)
                                .padding()
                                .background(Color(hex: "#BE2812"))
                                .cornerRadius(5)
                            
                            Text("\(data.judul)")
                                .font(.subheadline)
                                .foregroundColor(Color("gray"))
                                .fontWeight(.semibold)
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                            
                        }
                        .padding()
                    }
                    .background(Color("light_gray"))
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
    
    var cardSuggest: some View {
        
        ForEach(listKbliSearch, id: \.kode) { data in
            NavigationLink(
                destination: KbliSecondDetailScreen(
                    kategori: .constant(data.kategori),
                    kode: .constant(data.kode),
                    title: .constant(data.judul),
                    uraian: .constant(data.uraian)),
                label: {
                    VStack {
                        HStack(spacing: 10) {
                            
                            VStack(alignment: .leading, spacing: 20) {
                                hilightedText(str: "\(data.kode) - \(data.judul)", searched: searchCtrl)
                                    .font(.subheadline)
                                    .foregroundColor(Color("gray"))
                                    .fontWeight(.semibold)
                                    .multilineTextAlignment(.leading)
                                    .fixedSize(horizontal: false, vertical: true)
                                
                                hilightedText(str: data.uraian, searched: searchCtrl)
                                    .font(.subheadline)
                                    .foregroundColor(Color("gray"))
                                    .fontWeight(.semibold)
                                    .multilineTextAlignment(.leading)
                                    .fixedSize(horizontal: false, vertical: true)
                            }
                            
                            Spacer()
                            
                        }
                        .padding()
                    }
                    .background(Color("light_gray"))
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                })
        }
    }
}

struct KbliScreen_Previews: PreviewProvider {
    static var previews: some View {
        KbliScreen()
    }
}

extension KbliScreen {
    
    func getKbli(searchParam: String) {
        self.bantuanVM.kbli(searchParam: searchParam, kategori: "", kode: "") { success in
            
            if success {
                self.listKbli = self.bantuanVM.kbliList
            }
            
            if !success {
                
            }
        }
    }
    
    func getKbliSearch(searchParam: String) {
        self.bantuanVM.kbliSearch(searchParam: searchParam) { success in
            
            if success {
                print("KBLI SEARCH")
                print(self.bantuanVM.kbliListSearch.count)
                self.listKbliSearch = self.bantuanVM.kbliListSearch
            }
            
            if !success {
                
            }
        }
    }
    
    func hilightedText(str: String, searched: String) -> Text {
        print(searched)
        var result: Text!

        for word in str.split(separator: " ") {
            var text = Text(word)
            if word.lowercased() == searchCtrl.lowercased() {
                text = text.bold().foregroundColor(Color("red"))
            }
            result = (result == nil ? text : result + Text(" ") + text)
        }
        return result ?? Text(str)
    }
}
