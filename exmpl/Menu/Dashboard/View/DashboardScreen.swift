//
//  DashboardScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI
import AVKit

struct DashboardScreen: View {
    
    let defaults = UserDefaults.standard
    
    @StateObject var bannerVM = BannerViewModel()
    @State var listVideo = [VideoBanner]()
    @State var listThumbnail = [String]()
    
    @State private var showModalProgression: Bool = false
    @ObservedObject var onboard = ProgressiveOnboard.init(withJson: progressiveOnboardsJson)
    
    @State private var title: String = ""
    @State private var nextRoute: Bool = false
    @State private var isGetData: Bool = true
    
    var body: some View {
        ZStack(alignment: .top) {
            Color("red")
                .frame(height: 250)
                .edgesIgnoringSafeArea(.top)
            
            VStack {
                
                NavigationLink(
                    destination: switchPage(title: title),
                    isActive: self.$nextRoute,
                    label: { EmptyView() }
                )
                
                // Header
                header
                    .padding(.top, 60)
                
                ScrollView {
                    // Image Slider
                    tabView
                        .padding(.top, 20)
                    
                    // Main Menu
                    mainMenu
                    
                    // Video Slider
                    VStack {
                        HStack {
                            Text("Video")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(Color("gray"))
                            
                            Spacer()
                            
                            Button(
                                action: {
                                    self.title = "Video"
                                    self.isGetData = false
                                    self.nextRoute = true
                                },
                                label: {
                                    Text("Lihat Semua")
                                        .font(.caption)
                                })
                        }
                        .padding(.top, 10)
                        .padding(.horizontal, 20)
                    }
                    
                    if (listVideo.count > 2) {
                        TabView {
                            ForEach(0..<2) { index in
                                Link(destination: URL(string: "\(listVideo[index].pathURL)")!, label: {
                                    VStack {
                                        ZStack {
                                            Image(systemName: "")
                                                .data(url: URL(string: "\(listThumbnail[index])")!)
                                                .resizable()
                                                .frame(width: getWidth() - 40, height: 200)
                                                .cornerRadius(10)
                                                .shadow(radius: 4)
                                            
                                            Image("ic_play")
                                        }
                                        
                                        Text("\(listVideo[index].judul)")
                                            .fontWeight(.semibold)
                                            .frame(width: getWidth() - 40)
                                            .font(.caption)
                                            .foregroundColor(Color("gray"))
                                            .padding(.bottom, 40)
                                    }
                                })
                            }
                        }
                        .frame(height: 270)
                        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
                    }
                    
                }
                
            }
            
            if(onboard.showOnboardScreen) {
                ProgressiveOnboardView.init(withProgressiveOnboard: self.onboard)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .navigationBarHidden(true)
        .coordinateSpace(name: "OnboardSpace")
        .edgesIgnoringSafeArea(.all)
        .onAppear() {
            self.defaults.set(AppConstants().USERKEY, forKey: defaultsKeys.keyUser)
            if isGetData {
                getVideoBanner()
            }
        }
    }
    
    var header: some View {
        HStack {
            
            Image("logo_oss")
                .resizable()
                .frame(width: 80, height: 25)
            
            Spacer()
            
            if (defaults.bool(forKey: defaultsKeys.keyIsLogin)) {
                
                HStack(spacing: 20) {
                    
                    Button(
                        action: {
                            self.title = "Profil"
                            self.isGetData = false
                            self.nextRoute = true
                        },
                        label: {
                            VStack(spacing: 5) {
                                Image(systemName: "person")
                                    .foregroundColor(.white)
                                
                                Text("Profil Saya")
                                    .foregroundColor(.white)
                                    .font(.caption)
                                    .fontWeight(.bold)
                            }
                        })
                    
                    Button(action: {}, label: {
                        VStack(spacing: 5) {
                            Image(systemName: "bell")
                                .foregroundColor(.white)
                            
                            Text("Notifikasi")
                                .foregroundColor(.white)
                                .font(.caption)
                                .fontWeight(.bold)
                        }
                    })
                }
                
            } else {
                
                Button(
                    action: {
                        self.title = "Masuk"
                        self.isGetData = false
                        self.nextRoute = true
                    },
                    label: {
                        HStack {
                            Image("ic_login")
                                .foregroundColor(.white)
                            
                            Text("Masuk")
                                .foregroundColor(.white)
                                .font(.caption)
                                .fontWeight(.bold)
                        }
                        .padding(.all, 5)
                    })
                    .background(ProgressiveOnboardGeometry(withRect: $onboard.filterViews[6]))
            }
        }
        .padding(.horizontal, 15)
    }
    
    var tabView: some View {
        TabView {
            Image("banner1")
                .resizable()
                .frame(width: UIScreen.main.bounds.width - 30, height: 200)
                .cornerRadius(10)
                .shadow(radius: 4)
            
            Image("banner2")
                .resizable()
                .frame(width: UIScreen.main.bounds.width - 30, height: 200)
                .cornerRadius(10)
                .shadow(radius: 4)
        }
        .frame(height: 200)
        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
        .onAppear() {
            setupAppearance()
        }
    }
    
    var mainMenu: some View {
        VStack {
            HStack {
                
                Spacer()
                
                Button(action: {}, label: {
                    RoundedIconWithLabel(
                        imageName: "ic_perizinan",
                        label: "Perizinan"
                    )
                })
                .background(ProgressiveOnboardGeometry(withRect: $onboard.filterViews[0]))
                
                Spacer()
                
                Button(action: {}, label: {
                    RoundedIconWithLabel(
                        imageName: "ic_scan",
                        label: "Scan QR Code"
                    )
                })
                .background(ProgressiveOnboardGeometry(withRect: $onboard.filterViews[1]))
                
                Spacer()
                
                Button(action: {}, label: {
                    RoundedIconWithLabel(
                        imageName: "ic_lacak",
                        label: "Pelacakan"
                    )
                })
                .background(ProgressiveOnboardGeometry(withRect: $onboard.filterViews[2]))
                
                Spacer()
            }
            .padding(.top, 10)
            
            HStack {
                
                Spacer()
                
                Button(
                    action: {
                        self.title = "Bantuan"
                        self.isGetData = false
                        self.nextRoute = true
                    },
                    label: {
                        RoundedIconWithLabel(
                            imageName: "ic_bantuan",
                            label: "Bantuan"
                        )
                    })
                    .background(ProgressiveOnboardGeometry(withRect: $onboard.filterViews[3]))
                
                Spacer()
                
                Button(
                    action: {
                        self.title = "Panduan"
                        self.isGetData = false
                        self.nextRoute = true
                    },
                    label: {
                        RoundedIconWithLabel(
                            imageName: "ic_panduan",
                            label: "Panduan"
                        )
                    })
                    .background(ProgressiveOnboardGeometry(withRect: $onboard.filterViews[4]))
                
                Spacer()
                
                Button(action: {}, label: {
                    RoundedIconWithLabel(
                        imageName: "ic_simulasi",
                        label: "Simulasi"
                    )
                })
                .background(ProgressiveOnboardGeometry(withRect: $onboard.filterViews[5]))
                
                Spacer()
            }
            .padding(.top, 10)
        }
        .background(Color("white"))
    }
}

struct DashboardScreen_Previews: PreviewProvider {
    static var previews: some View {
        DashboardScreen()
    }
}

extension DashboardScreen {
    
    func switchPage(title: String) -> AnyView {
        switch title {
        case "Masuk":
            return AnyView(LoginScreen(isFromRegister: .constant(false)))
        case "Profil":
            return AnyView(ProfilScreen())
        case "Bantuan":
            return AnyView(BantuanScreen())
        case "Panduan":
            return AnyView(PanduanScreen())
        case "Video":
            return AnyView(VideoScreen())
        default:
            return AnyView(EmptyView())
        }
        
        return AnyView(EmptyView())
    }
    
    func setupAppearance() {
        UIPageControl.appearance().currentPageIndicatorTintColor = #colorLiteral(red: 0.6078431373, green: 0.1215686275, blue: 0.08235294118, alpha: 1)
        UIPageControl.appearance().pageIndicatorTintColor = UIColor.black.withAlphaComponent(0.2)
    }
    
    func getVideoBanner() {
        self.bannerVM.videoBanner(searchParam: "") { success in
            
            if success {
                if defaults.bool(forKey: defaultsKeys.keyIsLogin) {
                    self.onboard.showOnboardScreen = false
                } else {
                    if defaults.bool(forKey: defaultsKeys.keyIsHighlight) {
                        self.onboard.showOnboardScreen = false
                    } else {
                        self.onboard.showOnboardScreen = true
                    }
                }
                self.listVideo = self.bannerVM.videoList
                
                self.listVideo.forEach { (data: VideoBanner) in
                    if let range = data.pathURL.range(of: "embed/") {
                        let id = data.pathURL[range.upperBound...]
                        print(id)
                        self.listThumbnail.append("https://img.youtube.com/vi/\(String(id))/0.jpg")
                    }
                }
            }
            
            if !success {
                
            }
        }
    }
    
}

// Step 1: Set your data
let progressiveOnboardsJson = """
[
    {
        "description": "Ajukan Permohonan Perizinan Berusaha Usaha Mikro Kecil(UMK) Perseorangan di sini",
        "previousButtonTitle": "",
        "nextButtonTitle": "Selanjutnya"
    },
    {
        "description": "Scan QR Code untuk memastikan keaslian dokumen perizinan Anda",
        "previousButtonTitle": "",
        "nextButtonTitle": "Selanjutnya"
    },
    {
        "description": "Lihat perkembangan pemrosesan Perizinan Berusaha Anda",
        "previousButtonTitle": "",
        "nextButtonTitle": "Selanjutnya"
    },
    {
        "description": "Temukan jawaban atas pertanyaan Perizinan Berusaha di sini",
        "previousButtonTitle": "",
        "nextButtonTitle": "Selanjutnya"
    },
    {
        "description": "Temukan Panduan Perizinan Berusaha di sini",
        "previousButtonTitle": "",
        "nextButtonTitle": "Selanjutnya"
    },
    {
        "description": "Lakukan Simulasi Permohonan Perizinan Berusaha Usaha Mikro Kecil(UMK) Perseorangan di sini",
        "previousButtonTitle": "",
        "nextButtonTitle": "Selanjutnya"
    },
    {
        "description": "Pilih Masuk untuk melakukan Perizinan Berusaha Usaha Mikro Kecil(UMK) Perseorangan",
        "previousButtonTitle": "Back",
        "nextButtonTitle": "OK, Mengerti"
    }
]
"""
