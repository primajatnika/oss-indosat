//
//  UbahNomorTeleponScreen.swift
//  exmpl
//
//  Created by Tabeldata Informatika on 19/11/21.
//

import SwiftUI

struct UbahNomorTeleponScreen: View {
    
    var defaults = UserDefaults.standard
    
    @StateObject var otpVM = OtpViewModel()
    
    @Binding var email: String
    @Binding var idProfil: Int
    
    @State private var noTeleponCtrl: String = ""
    
    @State private var isLoading: Bool = false
    @State private var routeOtp: Bool = false
    
    var body: some View {
        VStack {
            
            NavigationLink(
                destination: UbahNomorTeleponOtpScreen(email: .constant(email), noTelepon: .constant(noTeleponCtrl), idProfil: .constant(idProfil)),
                isActive: self.$routeOtp,
                label: {
                    EmptyView()
                })
            
            if isLoading {
                LoadingView()
            }
            
            formNoTelepon
                .padding(.top)
            
            Button(action: {
                getOtp(
                    email: " ",
                    noTelp: "+62" + noTeleponCtrl,
                    isWhatsappsend: true
                )
            }, label: {
                Text("Kirim Kode Verifikasi melalui Whatsapp")
                    .font(.subheadline)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                
            })
            .background(Color(disableButton ? "gray" : "blue"))
            .cornerRadius(8)
            .padding(.top, 10)
            .padding(.bottom, 10)
            .padding(.horizontal, 20)
            .disabled(disableButton)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationBarTitle("Ubah Nomor Telepon Seluler", displayMode: .inline)
    }
    
    var formNoTelepon: some View {
        VStack(alignment: .leading) {
            Text("Nomor Telepon Seluler")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            HStack {
                
                HStack {
                    Image("ic_ina_flag")
                        .resizable()
                        .frame(width: 20, height: 20)
                    
                    Text("+62")
                        .font(.headline)
                        .foregroundColor(Color("gray"))
                }
                .padding()
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.gray, lineWidth: 1))
                .padding(.leading, 20)
                
                VStack (alignment: .leading) {
                    
                    HStack {
                        TextField("Nomor Telepon Seluler", text: $noTeleponCtrl, onEditingChanged: {_ in }, onCommit: {})
                            .keyboardType(.numberPad)
                            .frame(height: 40)
                            .font(Font.system(size: 14))
                            .padding(.horizontal, 10)
                            .onReceive(noTeleponCtrl.publisher.collect()) {
                                if String($0).hasPrefix("0") {
                                    self.noTeleponCtrl = String(String($0).substring(with: 1..<String($0).count).prefix(12))
                                } else {
                                    self.noTeleponCtrl = String($0.prefix(13))
                                }
                            }
                    }
                    .frame(height: 40)
                    .cornerRadius(10)
                    .background(
                        RoundedRectangle(cornerRadius: 8)
                            .strokeBorder(Color.gray, lineWidth: 1))
                }
                .padding(.trailing, 20)
            }
            
        }
        .padding(.bottom, 10)
    }
}

struct UbahNomorTeleponScreen_Previews: PreviewProvider {
    static var previews: some View {
        UbahNomorTeleponScreen(email: .constant(""), idProfil: .constant(1))
    }
}

extension UbahNomorTeleponScreen {
    
    var disableButton: Bool {
        noTeleponCtrl.isEmpty
    }
    
    func getOtp(email: String, noTelp: String, isWhatsappsend: Bool) {
        self.routeOtp = true
        //        self.isLoading = true
        //        self.otpVM.getOtp(
        //            email: email,
        //            noTelepon: noTelp,
        //            sendWhatsapp: isWhatsappsend) { success in
        //
        //            if success {
        //                self.daftarModel.email = email
        //                self.daftarModel.telepon = noTelp
        //                self.daftarModel.jenisPelakuUsaha = "02"
        //
        //                self.isLoading = false
        //                self.routeOtp = true
        //            }
        //
        //            if !success {
        //                self.isLoading = false
        //            }
        //        }
    }
    
}
