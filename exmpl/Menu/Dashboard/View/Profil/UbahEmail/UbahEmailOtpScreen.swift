//
//  UbahEmailOtpScreen.swift
//  exmpl
//
//  Created by Tabeldata Informatika on 19/11/21.
//

import SwiftUI

struct UbahEmailOtpScreen: View {
    
    @EnvironmentObject var appState: AppState
    
    @StateObject var otpVM = OtpViewModel()
    @StateObject var profilVM = ProfilViewModel()
    
    var maxDigits: Int = 6
    @State var pin: String = ""
    
    @Binding var email: String
    @Binding var noTelepon: String
    @Binding var idProfil: Int
    
    @State private var disableButtonResend: Bool = false
    @State private var isLoading: Bool = false
    @State private var showSuccess: Bool = false
    
    @State private var timeRemainingBtn = 120
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        VStack {
            
            if self.isLoading {
                LoadingView()
            }
            
            VStack {
                Text("Verifikasi")
                    .font(.headline)
                    .foregroundColor(Color("red"))
                    .padding(.bottom, 20)
                
                Text("Masukkan kode verifikasi yang dikirim ke Whatsapp Anda")
                    .font(.caption)
                    .foregroundColor(Color("gray"))
                    .fontWeight(.semibold)
                    .padding(.horizontal, 10)
                    .padding(.bottom, 10)
                    .multilineTextAlignment(.center)
                
                ZStack {
                    pinDots
                    backgroundField
                }
                
                if otpVM.isOtpInvalid {
                    VStack {
                        Text("Kode verifikasi salah")
                            .font(.caption)
                            .foregroundColor(Color("red"))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                    }
                    .padding(.horizontal, 20)
                    .padding(.top, 10)
                } else {
                    HStack {
                        Text("Mohon tunggu")
                            .font(.caption)
                            .foregroundColor(Color("gray"))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                        
                        Text("\(self.timeRemainingBtn.formatted(allowedUnits: [.second])!)")
                            .font(.caption)
                            .fontWeight(.bold)
                            .foregroundColor(Color("red"))
                            .multilineTextAlignment(.center)
                        
                        Text("detik untuk mengirim ulang")
                            .font(.caption)
                            .foregroundColor(Color("gray"))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                    }
                    .padding(.horizontal, 20)
                    .padding(.top, 10)
                }
                
                Spacer()
                
                HStack {
                    
                    // Button Kirim Ulang
                    Button(action: {
                        getOtp(
                            email: email,
                            noTelp: noTelepon,
                            isWhatsappsend: false)
                    }, label: {
                        Text("Kirim Ulang")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(Color(disableButton ? "gray" : "white"))
                            .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                        
                    })
                    .background(Color(disableButton ? "light_gray" : "green"))
                    .disabled(disableButton)
                    .cornerRadius(10)
                    .padding(.leading, 10)
                    .padding(.horizontal, 20)
                    .padding(.top, 20)
                    .padding(.bottom, 10)
                    

                }
                .background(Color.white)
                .cornerRadius(10, corners: [.topLeft, .topRight])
                .shadow(color: .gray, radius: 10, x: 0, y: 0)
                .mask(Rectangle().padding(.top, -20))
            }
            .padding(.top, 10)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationBarTitle("Ubah Alamat", displayMode: .inline)
        .partialSheet(isPresented: self.$showSuccess) {
            VStack {
                
                Image("ic_success")
                
                Text("Terimakasih Email berhasil diganti")
                    .foregroundColor(Color("gray"))
                    .font(.subheadline)
                    .fontWeight(.semibold)
                    .padding(.horizontal, 20)
                    .padding(.vertical, 20)
                
                // Button
                Button(action: {
                    self.showSuccess = false
                    self.appState.back2Profil = true
                }, label: {
                    Text("Tutup")
                        .font(.subheadline)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                    
                })
                .background(Color("blue"))
                .cornerRadius(5)
                .padding(.top, 20)
                .padding(.bottom, 10)
                .padding(.horizontal, 20)
            }
        }
        .alert(isPresented: self.$otpVM.showAlert, content: {
            return Alert(
                title: Text("Pesan"),
                message: Text("\(self.otpVM.message)"),
                dismissButton: .cancel()
            )
        })
        .onReceive(timer) { time in
            
            if self.timeRemainingBtn > 0 {
                self.timeRemainingBtn -= 1
            }
            
            if self.timeRemainingBtn < 1 {
                disableButtonResend = false
            }
        }
    }
    
    private var pinDots: some View {
        HStack {
            Spacer()
            ForEach(0..<maxDigits) { index in
                Text("\(self.getImageName(at: index))")
                    .font(.title)
                    .foregroundColor(.black)
                    .bold()
                    .frame(width: 40, height: 40)
                    .multilineTextAlignment(.center)
                    .cornerRadius(8)
                    .background(
                        RoundedRectangle(cornerRadius: 4)
                            .strokeBorder(Color.gray, lineWidth: 1))
            }
            Spacer()
        }
    }
    
    private var backgroundField: some View {
        let boundPin = Binding<String>(get: { self.pin }, set: { newValue in
            self.pin = newValue
            self.submitPin()
        })
        
        return TextField("", text: boundPin, onCommit: submitPin)
            .accentColor(.clear)
            .foregroundColor(.clear)
            .keyboardType(.numberPad)
    }
    
    private func submitPin() {
        if pin.count == maxDigits {
            submitOtp()
        }
        
        if pin.count > maxDigits {
            pin = String(pin.prefix(maxDigits))
            submitPin()
        }
    }
    
    private func getImageName(at index: Int) -> String {
        if index >= self.pin.count {
            return ""
        }
        
        return self.pin.digits[index].numberString
    }
    
    private func resetPin() {
        self.pin = ""
    }
}

struct UbahEmailOtpScreen_Previews: PreviewProvider {
    static var previews: some View {
        UbahEmailOtpScreen(email: .constant(""), noTelepon: .constant(""), idProfil: .constant(1))
    }
}

extension UbahEmailOtpScreen {
    
    var disableButton: Bool {
        timeRemainingBtn > 1
    }
    
    func submitOtp() {
        self.isLoading = true
        self.otpVM.verifOtp(
            email: email,
            otp: pin,
            verifyWhatsapp: noTelepon) { success in
            
            if success {
                self.isLoading = false
                ubahProfil()
            }
            
            if !success {
                self.isLoading = false
                resetPin()
            }
            
        }
    }
    
    func ubahProfil() {
        self.isLoading = true
        self.profilVM.ubahProfil(email: email, noTelepon: noTelepon, idProfil: idProfil) { success in
            
            if success {
                self.isLoading = false
                self.showSuccess = true
            }
            
            if !success {
                self.isLoading = false
                self.otpVM.showAlert = true
                self.otpVM.message = self.profilVM.message
            }
            
        }
    }
    
    func getOtp(email: String, noTelp: String, isWhatsappsend: Bool) {
        self.isLoading = true
        self.otpVM.getOtp(
            email: email,
            noTelepon: noTelp,
            sendWhatsapp: isWhatsappsend) { success in
            
            if success {
                self.isLoading = false
                self.timeRemainingBtn = 120
            }
            
            if !success {
                self.isLoading = false
            }
        }
    }
}
