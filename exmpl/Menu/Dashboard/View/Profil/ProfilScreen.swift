//
//  ProfilScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 18/11/21.
//

import SwiftUI

struct ProfilScreen: View {
    
    @EnvironmentObject var appState: AppState
    
    var defaults = UserDefaults.standard
    
    @StateObject var profilVM = ProfilViewModel()
    
    @State private var title: String = ""
    
    @State private var idProfile: Int = 0
    @State private var noTeleponCtrl: String = ""
    @State private var emailCtrl: String = ""
    @State private var passwordCtrl: String = ""
    @State private var nikCtrl: String = ""
    @State private var namaLengkapCtrl: String = ""
    @State private var tanggalLahirCtrl: String = ""
    @State private var jenisKelaminCtrl: String = ""
    @State private var alamatCtrl: String = ""
    @State private var provinsiCtrl: String = ""
    @State private var kabupatenCtrl: String = ""
    @State private var kecamatanCtrl: String = ""
    @State private var kelurahanCtrl: String = ""
    
    @State private var nextRoute: Bool = false
    @State private var isLoading: Bool = true
    @State private var showPassword: Bool = false
    
    var body: some View {
        VStack {
            
            NavigationLink(
                destination: switchPage(title: title),
                isActive: self.$nextRoute,
                label: {
                    EmptyView()
                })
                .isDetailLink(false)
            
            ScrollView {
                
                if isLoading {
                    LoadingView()
                }
                
                Group {
                    formNoTelepon
                    formEmail
                    formPassword
                    formNik
                    formNamaLengkap
                    formTanggalLahir
                }
                
                Group {
                    formJenisKelamin
                    formAlamat
                    formProvinsi
                    formKabupatenKota
                    formKecamatan
                    formKelurahan
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationBarTitle("Profil", displayMode: .inline)
        .navigationBarItems(trailing:
                                Button(
                                    action: {  },
                                    label: {
                                        HStack(spacing: 5) {
                                            Image(systemName: "arrow.right.square")
                                                .foregroundColor(.white)
                                            
                                            Text("Keluar")
                                                .foregroundColor(.white)
                                                .font(.caption)
                                                .fontWeight(.bold)
                                        }
                                    })
        )
        .onAppear() {
            self.passwordCtrl = defaults.string(forKey: defaultsKeys.keyPassword) ?? ""
        }
        .onReceive(self.appState.$back2Profil) { profile in
            if profile {
                print("Move to Profile: \(profile)")
                DispatchQueue.main.async {
                    self.nextRoute = false
                    self.appState.back2Profil = false
                }
            }
        }
        
        .onAppear() {
            getProfil()
        }
    }
    
    var formNoTelepon: some View {
        VStack(alignment: .leading) {
            
            HStack {
                Text("Nomor Telepon Seluler")
                    .font(.subheadline)
                    .foregroundColor(Color("gray"))
                    .fontWeight(.semibold)
                    .padding(.horizontal, 20)
                    .disabled(true)
                
                Spacer()
                
                Button(
                    action: {
                        self.title = "Ubah Telepon"
                        self.nextRoute = true
                    }, label: {
                        Text("Ubah")
                            .font(.subheadline)
                            .foregroundColor(Color("red"))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 20)
                    })
            }
            
            HStack {
                
                HStack {
                    Image("ic_ina_flag")
                        .resizable()
                        .frame(width: 20, height: 20)
                    
                    Text("+62")
                        .font(.headline)
                        .foregroundColor(Color("gray"))
                }
                .padding()
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.gray, lineWidth: 1))
                .padding(.leading, 20)
                
                VStack (alignment: .leading) {
                    
                    HStack {
                        TextField("Nomor Telepon Seluler", text: $noTeleponCtrl, onEditingChanged: {_ in }, onCommit: {})
                            .keyboardType(.numberPad)
                            .frame(height: 40)
                            .font(Font.system(size: 14))
                            .padding(.horizontal, 10)
                            .disabled(true)
                    }
                    .frame(height: 40)
                    .cornerRadius(10)
                    .background(
                        RoundedRectangle(cornerRadius: 8)
                            .strokeBorder(Color.gray, lineWidth: 1))
                }
                .padding(.trailing, 20)
            }
            
        }
        .padding(.vertical, 10)
    }
    
    var formEmail: some View {
        VStack(alignment: .leading) {
            
            HStack {
                Text("Alamat Email")
                    .font(.subheadline)
                    .foregroundColor(Color("gray"))
                    .fontWeight(.semibold)
                    .padding(.horizontal, 20)
                    .disabled(true)
                
                Spacer()
                
                Button(
                    action: {
                        self.title = "Ubah Email"
                        self.nextRoute = true
                    }, label: {
                        Text("Ubah")
                            .font(.subheadline)
                            .foregroundColor(Color("red"))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 20)
                    })
            }
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Alamat Email", text: $emailCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                        .disabled(true)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.gray, lineWidth: 1))
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formPassword: some View {
        VStack(alignment: .leading) {
            
            HStack {
                Text("Password")
                    .font(.subheadline)
                    .foregroundColor(Color("gray"))
                    .fontWeight(.semibold)
                    .padding(.horizontal, 20)
                
                Spacer()
                
                Button(
                    action: {
                        self.title = "Ubah Password"
                        self.nextRoute = true
                    }, label: {
                        Text("Ubah")
                            .font(.subheadline)
                            .foregroundColor(Color("red"))
                            .fontWeight(.semibold)
                            .padding(.horizontal, 20)
                    })
            }
            
            VStack (alignment: .leading) {
                
                HStack {
                    if showPassword {
                        TextField("Password", text: $passwordCtrl, onEditingChanged: {_ in }, onCommit: {})
                            .frame(height: 40)
                            .font(Font.system(size: 14))
                            .padding(.horizontal, 10)
                            .disabled(true)
                    } else {
                        SecureField("Password", text: $passwordCtrl, onCommit: {})
                            .frame(height: 40)
                            .font(Font.system(size: 14))
                            .padding(.horizontal, 10)
                            .disabled(true)
                    }
                    
                    Button(action: {
                        showPassword.toggle()
                    }, label: {
                        Image(systemName: showPassword ? "eye.fill" : "eye.slash")
                            .foregroundColor(Color.gray)
                    })
                    .padding(.horizontal)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.gray, lineWidth: 1))
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formNik: some View {
        VStack(alignment: .leading) {
            
            Text("Nomor Induk Kependudukan")
                .font(.subheadline)
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Nomor Induk Kependudukan", text: $nikCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(Color("light_gray"))
                .cornerRadius(10)
                .disabled(true)
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formNamaLengkap: some View {
        VStack(alignment: .leading) {
            
            Text("Nama Lengkap")
                .font(.subheadline)
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Nama Lengkap", text: $namaLengkapCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(Color("light_gray"))
                .cornerRadius(10)
                .disabled(true)
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formTanggalLahir: some View {
        VStack(alignment: .leading) {
            
            Text("Tanggal Lahir")
                .font(.subheadline)
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Tanggal Lahir", text: $tanggalLahirCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(Color("light_gray"))
                .cornerRadius(10)
                .disabled(true)
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formJenisKelamin: some View {
        VStack(alignment: .leading) {
            
            Text("Jenis Kelamin")
                .font(.subheadline)
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Jenis Kelamin", text: $jenisKelaminCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(Color("light_gray"))
                .cornerRadius(10)
                .disabled(true)
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formAlamat: some View {
        VStack(alignment: .leading) {
            
            Text("Alamat")
                .font(.subheadline)
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Alamat", text: $alamatCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(Color("light_gray"))
                .cornerRadius(10)
                .disabled(true)
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formProvinsi: some View {
        VStack(alignment: .leading) {
            
            Text("Provinsi")
                .font(.subheadline)
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Provinsi", text: $provinsiCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(Color("light_gray"))
                .cornerRadius(10)
                .disabled(true)
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formKabupatenKota: some View {
        VStack(alignment: .leading) {
            
            Text("Kabupaten/Kota")
                .font(.subheadline)
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Kabupaten/Kota", text: $kabupatenCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(Color("light_gray"))
                .cornerRadius(10)
                .disabled(true)
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formKecamatan: some View {
        VStack(alignment: .leading) {
            
            Text("Kecamatan")
                .font(.subheadline)
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Kecamatan", text: $kecamatanCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(Color("light_gray"))
                .cornerRadius(10)
                .disabled(true)
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formKelurahan: some View {
        VStack(alignment: .leading) {
            
            Text("Desa/Kelurahan")
                .font(.subheadline)
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Desa/Kelurahan", text: $kelurahanCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(Color("light_gray"))
                .cornerRadius(10)
                .disabled(true)
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
}

struct ProfilScreen_Previews: PreviewProvider {
    static var previews: some View {
        ProfilScreen()
    }
}

extension ProfilScreen {
    
    func switchPage(title: String) -> AnyView {
        switch title {
        case "Ubah Telepon":
            return AnyView(UbahNomorTeleponScreen(email: .constant(emailCtrl), idProfil: .constant(idProfile)))
        case "Ubah Email":
            return AnyView(UbahEmailScreen(noTelepon: .constant(noTeleponCtrl), idProfil: .constant(idProfile)))
        case "Ubah Password":
            return AnyView(UbahPasswordScreen(idProfil: .constant(idProfile)))
        default:
            return AnyView(EmptyView())
        }
        
        return AnyView(EmptyView())
    }
    
    func getProfil() {
        self.profilVM.profil { success in
            
            if success {
                self.isLoading = false
                self.nikCtrl = self.profilVM.nikCtrl
                self.namaLengkapCtrl = self.profilVM.namaLengkapCtrl
                self.emailCtrl = self.profilVM.emailCtrl
                self.noTeleponCtrl = self.profilVM.noTeleponCtrl
                self.idProfile = self.profilVM.idProfile
            }
            
            if !success {
                self.isLoading = false
            }
            
        }
    }
    
}
