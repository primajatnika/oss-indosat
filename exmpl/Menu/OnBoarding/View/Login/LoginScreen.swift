//
//  LoginScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI
import SSToastMessage

struct LoginScreen: View {
    
    var defaults = UserDefaults.standard
    
    @Binding var isFromRegister: Bool
    
    @StateObject var loginVM = LoginViewModel()
    
    @State private var usernameCtrl: String = ""
    @State private var passwordCtrl: String = ""
    @State private var captchaCtrl: String = ""
    
    @State private var captcha: String = "AZ1yXS"
    
    // Routing Variable
    @State private var nextRoute: Bool = false
    
    var body: some View {
        
        ZStack {
            
            
            VStack(alignment: .leading) {
                
                NavigationLink(
                    destination: DashboardScreen(),
                    isActive: self.$nextRoute,
                    label: {})
                
                if (self.loginVM.isLoading) {
                    LoadingView()
                }
                
                VStack {
                    HStack(alignment: .top) {
                        Image(systemName: "info.circle.fill")
                            .foregroundColor(Color("blue"))
                            .padding(.vertical, 10)
                            .padding(.leading, 10)
                        
                        Text("Bagi Pelaku Usaha yang telah memiliki Hak Akses di Sistem OSS 1.1, silahkan masuk menggunakan username/email dan password lama Anda")
                            .foregroundColor(Color("gray"))
                            .font(.caption)
                            .fontWeight(.semibold)
                            .padding(.vertical, 10)
                            .padding(.horizontal, 10)
                            .fixedSize(horizontal: false, vertical: true)
                        
                        Spacer()
                    }
                }
                .background(Color("light_blue"))
                .cornerRadius(5)
                .padding(.horizontal, 20)
                .padding(.top, 20)
                .padding(.bottom, 10)
                
                // Text Field Login
                VStack {
                    LabelTextFieldWithIcon(
                        value: self.$usernameCtrl,
                        placeHolder: "Nomor telepon seluler atau email",
                        iconName: "person") { bool in
                            print(self._usernameCtrl)
                        } onCommit: {
                            print(self._usernameCtrl)
                        }
                        .padding(.bottom, 5)
                    
                    LabelPasswordFieldWithIcon(
                        value: self.$passwordCtrl,
                        placeHolder: "Password",
                        iconName: "lock") { Bool in
                            print(self._passwordCtrl)
                        } onCommit: {
                            print(self._passwordCtrl)
                        } onForgotPassword: {
                            
                        }
                }
                
                HStack(alignment: .center) {
                    Spacer()
                    // Button Lupa Password
                    Button(
                        action: {},
                        label: {
                            Text("Lupa Password?")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(Color("blue"))
                        })
                    Spacer()
                }
                .padding(.top, 30)
                
                // Form Captcha
                VStack(alignment: .leading) {
                    Text(captcha)
                        .font(Font.custom("Sportrop", size: 22, relativeTo: .headline))
                        .italic()
                        .fontWeight(.bold)
                        .foregroundColor(Color("green"))
                        .padding(.horizontal, 30)
                        .padding(.vertical, 5)
                    
                    LabelTextField(
                        value: self.$captchaCtrl,
                        placeHolder: "Masukkan kode Captcha") { (Bool) in
                            print(self._captchaCtrl)
                        } onCommit: {
                            print(self._captchaCtrl)
                        }
                }
                .padding(.top, 15)
                
                // Button
                VStack(alignment: .center) {
                    
                    // Button Login
                    Button(action: {
                        submit()
                    }, label: {
                        Text("Masuk")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(Color(disableButton ? "gray" : "white"))
                            .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                        
                    })
                        .background(Color(disableButton ? "light_gray" : "blue"))
                        .disabled(disableButton)
                        .cornerRadius(5)
                        .padding(.horizontal, 20)
                        .padding(.top, 10)
                        .padding(.bottom, 10)
                    
                    // Button Daftar
                    NavigationLink(
                        destination: DaftarScreen(),
                        label: {
                            Text("Daftar")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(Color("blue"))
                        })
                    
                }
                
                Spacer()
                
            }
            
            if self.loginVM.showAlert {
                ModalOverlay(tapAction: { withAnimation {} })
                    .edgesIgnoringSafeArea(.all)
            }
        }
        .navigationBarTitle("Masuk", displayMode: .inline)
        .navigationBarBackButtonHidden(isFromRegister)
        .onAppear() {
            self.defaults.set(AppConstants().AUTHORIZATION, forKey: defaultsKeys.keyToken)
            self.defaults.set(AppConstants().USERKEY, forKey: defaultsKeys.keyUser)
            
            getRandomCaptcha()
        }
        .present(
            isPresented: self.$loginVM.showAlert,
            type: .alert,
            autohideDuration: 1000) {
                alert
            }
    }
    
    var alert: some View {
        VStack {
            HStack {
                Spacer()
                Button(action: {
                    self.loginVM.showAlert = false
                }, label: {
                    Image(systemName: "xmark")
                        .foregroundColor(Color("gray"))
                })
            }
            .padding(.horizontal, 20)
            .padding(.top, 20)
            .padding(.bottom, 20)
            
            Image("ic_warning")
                .resizable()
                .frame(width: 50, height: 50)
                .padding(.bottom, 10)
            
            Text("\(self.loginVM.message)")
                .font(.subheadline)
                .fontWeight(.semibold)
                .foregroundColor(Color("gray"))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 20)
            
            Spacer()
            
            VStack {
                HStack {
                    Text("")
                    
                    Spacer()
                }
            }
            .background(Color("red"))
        }
        .frame(width: getWidth() - 35, height: 230)
        .background(Color("white"))
        .cornerRadius(20)
    }
}

struct LoginScreen_Previews: PreviewProvider {
    static var previews: some View {
        LoginScreen(isFromRegister: .constant(false))
    }
}

extension LoginScreen {
    
    var disableButton: Bool {
        passwordCtrl.isEmpty || usernameCtrl.isEmpty || captchaCtrl.isEmpty || loginVM.isLoading
    }
    
    func getRandomString() {
        self.captcha = randomString(length: 5)
    }
    
    func getRandomCaptcha() {
        
        let random = Bundle.main.decode([String].self, from: "regions.json")
        
        self.captcha = random.randomElement()?.components(separatedBy: " ")[1] ?? ""
    }
    
    func submit() {
        
        if (captchaCtrl != captcha) {
            self.loginVM.showAlert = true
            self.loginVM.message = "Captcha Tidak Sesuai"
            getRandomCaptcha()
            return
        }
        
        self.loginVM.login(
            username: usernameCtrl,
            password: passwordCtrl) { success in
                
                if success {
                    self.defaults.set("Bearer " + self.loginVM.access_token, forKey: defaultsKeys.keyToken)
                    self.defaults.set(true, forKey: defaultsKeys.keyIsLogin)
                    self.defaults.set(passwordCtrl, forKey: defaultsKeys.keyPassword)
                    
                    self.nextRoute = true
                }
                
                if !success {
                    
                }
            }
    }
    
}
