//
//  DaftarViaEmailScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 16/11/21.
//

import SwiftUI

struct DaftarViaEmailScreen: View {
    
    @StateObject var otpVM = OtpViewModel()
    @EnvironmentObject var daftarModel: DaftarBindingModel
    
    @State private var emailCtrl: String = ""
    @State private var confirmEmailCtrl: String = ""
    
    @State private var isEmailValid: Bool = false
    @State private var isLoading: Bool = false
    @State private var routeOtp: Bool = false
    
    
    var body: some View {
        ZStack {
            VStack {
                
                NavigationLink(
                    destination: DaftarViaEmailOtpScreen().environmentObject(daftarModel),
                    isActive: self.$routeOtp,
                    label: {
                        EmptyView()
                    })
                
                if (self.otpVM.isLoading) {
                    LoadingView()
                }
                
                formEmail
                formKonfirmasiEmail
                
                Button(action: {
                    getOtp(
                        email: emailCtrl,
                        noTelp: daftarModel.telepon,
                        isWhatsappsend: false
                    )
                }, label: {
                    Text("Kirim Kode Verifikasi melalui Email")
                        .font(.subheadline)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                    
                })
                .background(Color(disableButton ? "gray" : "blue"))
                .cornerRadius(8)
                .padding(.top, 20)
                .padding(.bottom, 10)
                .padding(.horizontal, 20)
                .disabled(disableButton)
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
            
            if self.otpVM.showAlert {
                ModalOverlay(tapAction: { withAnimation {} })
                    .edgesIgnoringSafeArea(.all)
            }
        }
        .navigationBarTitle("Daftar", displayMode: .inline)
        .navigationBarItems(trailing:
                                NavigationLink(
                                    destination: BantuanScreen(),
                                    label: {
                                        Image(systemName: "questionmark.circle")
                                    })
                            )
        .present(
            isPresented: self.$otpVM.showAlert,
            type: .alert,
            autohideDuration: 1000) {
                alert
            }
    }
    
    var alert: some View {
        VStack {
            HStack {
                Spacer()
                Button(action: {
                    self.otpVM.showAlert = false
                }, label: {
                    Image(systemName: "xmark")
                        .foregroundColor(Color("gray"))
                })
            }
            .padding(.horizontal, 20)
            .padding(.top, 20)
            .padding(.bottom, 20)
            
            Image("ic_warning")
                .resizable()
                .frame(width: 50, height: 50)
                .padding(.bottom, 10)
            
            Text("\(self.otpVM.message)")
                .font(.subheadline)
                .fontWeight(.semibold)
                .foregroundColor(Color("gray"))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 20)
            
            Spacer()
            
            VStack {
                HStack {
                    Text("")
                    
                    Spacer()
                }
            }
            .background(Color("red"))
        }
        .frame(width: getWidth() - 35, height: 230)
        .background(Color("white"))
        .cornerRadius(20)
    }
    
    var formEmail: some View {
        VStack(alignment: .leading) {
            Text("Alamat Email")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Alamat Email", text: $emailCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                        .onReceive(emailCtrl.publisher.collect()) { it in
                            self.isEmailValid = self.textFieldValidatorEmail(String(it))
                        }
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color(isEmailValid ? "gray" : "red"), lineWidth: 1))
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.vertical, 10)
    }
    
    var formKonfirmasiEmail: some View {
        VStack(alignment: .leading) {
            Text("Konfirmasi Alamat Email")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Konfirmasi Alamat Email", text: $confirmEmailCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color("gray"), lineWidth: 1))
            }
            .padding(.horizontal, 20)
            
        }
    }
}

struct DaftarViaEmailScreen_Previews: PreviewProvider {
    static var previews: some View {
        DaftarViaEmailScreen()
    }
}

extension DaftarViaEmailScreen {
    
    var disableButton: Bool {
        emailCtrl.isEmpty || confirmEmailCtrl.isEmpty || !isEmailValid
    }
    
    func textFieldValidatorEmail(_ string: String) -> Bool {
        if string.count > 100 {
            return false
        }
        
        let emailFormat = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" + "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: string)
    }
    
    func getOtp(email: String, noTelp: String, isWhatsappsend: Bool) {
        
        if (emailCtrl != confirmEmailCtrl) {
            self.otpVM.showAlert = true
            self.otpVM.message = "Email dan Konfirmasi Email tidak sesuai"
            return
        }
        
//        self.routeOtp = true
        self.isLoading = true
        self.otpVM.getOtp(
            email: email,
            noTelepon: noTelp,
            sendWhatsapp: isWhatsappsend) { success in

            if success {
                self.daftarModel.email = email

                self.isLoading = false
                self.routeOtp = true
            }

            if !success {
                self.isLoading = false
            }
        }
    }
    
}
