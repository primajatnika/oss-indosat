//
//  DaftarCreatePasswordScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 16/11/21.
//

import SwiftUI

struct DaftarCreatePasswordScreen: View {
    
    @StateObject var otpVM = OtpViewModel()
    @EnvironmentObject var daftarModel: DaftarBindingModel
    
    @State private var passwordCtrl: String = ""
    @State private var confirmPasswordCtrl: String = ""
    
    @State private var showPassword: Bool = false
    @State private var showConfirmPassword: Bool = false
    @State private var isLoading: Bool = false
    @State private var nextRoute: Bool = false
    
    @State private var isPasswordValid : Bool = false
    @State private var haveLowercase: Bool = false
    @State private var haveUppercase: Bool = false
    @State private var haveNumber: Bool = false
    @State private var haveSpecialcase: Bool = false
    @State private var haveMin8Char: Bool = false
    
    @State var minimumPasswordLength: Int = 8
    @State var minimumUpperCaseLetterInPassword: Int = 1
    @State var minimumLowerCaseLetterInPassword: Int = 1
    @State var minimumNumericInPassword: Int = 1
    
    var body: some View {
        
        ZStack {
            
            VStack {
                
                NavigationLink(
                    destination: DaftarDataDiriScreen().environmentObject(daftarModel),
                    isActive: self.$nextRoute,
                    label: {
                        EmptyView()
                    })
                
                if (self.otpVM.isLoading) {
                    LoadingView()
                }
                
                Text("Atur Password Anda")
                    .font(.headline)
                    .foregroundColor(Color("red"))
                    .padding(.bottom, 20)
                    .padding(.top, 10)
                
                formPassword
                formKonfirmasiPassword
                
                HStack {
                    Text("Gunakan minimal 8 karakter, kombinasi huruf kapital, huruf kecil, angka, dan karakter spesial(!,#,@)")
                        .font(.subheadline)
                        .fontWeight(.semibold)
                        .foregroundColor(Color("gray"))
                        .fixedSize(horizontal: false, vertical: true)
                        .padding(.top, 10)
                        .padding(.horizontal, 20)
                    
                    Spacer()
                }
                
                Spacer()
                
                VStack {
                    // Button
                    Button(action: {
                        submit()
                    }, label: {
                        Text("Lanjut")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                        
                    })
                    .background(Color(disableButton ? "gray" : "blue"))
                    .cornerRadius(8)
                    .padding(.top, 20)
                    .padding(.bottom, 10)
                    .padding(.horizontal, 20)
                    .disabled(disableButton)
                }
                .background(Color.white)
                .cornerRadius(10, corners: [.topLeft, .topRight])
                .shadow(color: .gray, radius: 10, x: 0, y: 0)
                .mask(Rectangle().padding(.top, -20))
                
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
            
            
            if self.otpVM.showAlert {
                ModalOverlay(tapAction: { withAnimation {} })
                    .edgesIgnoringSafeArea(.all)
            }
        }
        .navigationBarTitle("Daftar", displayMode: .inline)
        .navigationBarItems(trailing:
                                NavigationLink(
                                    destination: BantuanScreen(),
                                    label: {
                                        Image(systemName: "questionmark.circle")
                                    })
                            )
        .present(
            isPresented: self.$otpVM.showAlert,
            type: .alert,
            autohideDuration: 1000) {
                alert
            }
    }
    
    var alert: some View {
        VStack {
            HStack {
                Spacer()
                Button(action: {
                    self.otpVM.showAlert = false
                }, label: {
                    Image(systemName: "xmark")
                        .foregroundColor(Color("gray"))
                })
            }
            .padding(.horizontal, 20)
            .padding(.top, 20)
            .padding(.bottom, 20)
            
            Image("ic_warning")
                .resizable()
                .frame(width: 50, height: 50)
                .padding(.bottom, 10)
            
            Text("\(self.otpVM.message)")
                .font(.subheadline)
                .fontWeight(.semibold)
                .foregroundColor(Color("gray"))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 20)
            
            Spacer()
            
            VStack {
                HStack {
                    Text("")
                    
                    Spacer()
                }
            }
            .background(Color("red"))
        }
        .frame(width: getWidth() - 35, height: 230)
        .background(Color("white"))
        .cornerRadius(20)
    }
    
    var formPassword: some View {
        VStack(alignment: .leading) {
            Text("Password")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            HStack {
                if showPassword {
                    TextField("Password", text: $passwordCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.leading, 10)
                        .onReceive(passwordCtrl.publisher.collect()) { it in
                            self.isPasswordValid = self.textFieldValidatorPassword(String(it))
                            self.haveUppercase = self.isHaveUppercase(String(it))
                            self.haveLowercase = self.isHaveLowercase(String(it))
                            self.haveNumber = self.isHaveNumber(String(it))
                            self.haveSpecialcase = self.isHaveSpecialChar(String(it))
                            self.haveMin8Char = self.isHave8Char(String(it))
                        }
                } else {
                    SecureField("Password", text: $passwordCtrl, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.leading, 10)
                        .onReceive(passwordCtrl.publisher.collect()) { it in
                            self.isPasswordValid = self.textFieldValidatorPassword(String(it))
                            self.haveUppercase = self.isHaveUppercase(String(it))
                            self.haveLowercase = self.isHaveLowercase(String(it))
                            self.haveNumber = self.isHaveNumber(String(it))
                            self.haveSpecialcase = self.isHaveSpecialChar(String(it))
                            self.haveMin8Char = self.isHave8Char(String(it))
                        }
                }
                
                Button(action: {
                    showPassword.toggle()
                }, label: {
                    Image(systemName: showPassword ? "eye.fill" : "eye.slash")
                        .foregroundColor(Color.gray)
                        .padding(.trailing, 10)
                })
                
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color(isPasswordValid ? "gray" : "red"), lineWidth: 1))
            .padding(.horizontal, 20)
            
            if (!haveMin8Char) {
                Text("Password kurang dari 8 karakter")
                    .font(.caption)
                    .foregroundColor(Color("red"))
                    .padding(.horizontal, 30)
            } else if (!haveUppercase) {
                Text("Password harus menggunakan 1 huruf besar")
                    .font(.caption)
                    .foregroundColor(Color("red"))
                    .padding(.horizontal, 30)
            } else if (!haveLowercase) {
                Text("Password harus menggunakan min 1 huruf kecil")
                    .font(.caption)
                    .foregroundColor(Color("red"))
                    .padding(.horizontal, 30)
            } else if (!haveNumber) {
                Text("Password harus menggunakan min 1 angka")
                    .font(.caption)
                    .foregroundColor(Color("red"))
                    .padding(.horizontal, 30)
            } else if (!haveSpecialcase) {
                Text("Password harus menggunakan min 1 spesial karakter")
                    .font(.caption)
                    .foregroundColor(Color("red"))
                    .padding(.horizontal, 30)
            }
        }
        .padding(.top, 10)
    }
    
    var formKonfirmasiPassword: some View {
        VStack(alignment: .leading) {
            Text("Konfirmasi Password")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            HStack {
                if showConfirmPassword {
                    TextField("Ulangi Password", text: $confirmPasswordCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.leading, 10)
                } else {
                    SecureField("Ulangi Password", text: $confirmPasswordCtrl, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.leading, 10)
                }
                
                Button(action: {
                    showConfirmPassword.toggle()
                }, label: {
                    Image(systemName: showConfirmPassword ? "eye.fill" : "eye.slash")
                        .foregroundColor(Color.gray)
                        .padding(.trailing, 10)
                })
                
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color("gray"), lineWidth: 1))
            .padding(.horizontal, 20)
            
        }
        .padding(.vertical, 10)
    }
}

struct DaftarCreatePasswordScreen_Previews: PreviewProvider {
    static var previews: some View {
        DaftarCreatePasswordScreen().environmentObject(DaftarBindingModel())
    }
}

extension DaftarCreatePasswordScreen {
    
    var disableButton: Bool {
        passwordCtrl.count < 8 || confirmPasswordCtrl.count < 8 || validatePwd
    }
    
    var validatePwd: Bool {
        !haveLowercase || !haveUppercase || !haveNumber || !haveSpecialcase || !haveMin8Char
    }
    
    func isHaveUppercase(_ string: String) -> Bool {
        
        var format = ".*[A-Z]+.*"
        
        if minimumUpperCaseLetterInPassword >= 2 {
            format = ".*[A-Z].*[A-Z].*"
        }
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", format)
        return predicate.evaluate(with: string)
    }
    
    func isHaveLowercase(_ string: String) -> Bool {
        
        var format = ".*[a-z]+.*"
        
        if minimumLowerCaseLetterInPassword >= 2 {
            format = ".*[a-z].*[a-z].*"
        }
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", format)
        return predicate.evaluate(with: string)
    }
    
    func isHaveSpecialChar(_ string: String) -> Bool {
        
        let format = ".*[!@#$%&*()_+=|<>?{}~-]+.*"
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", format)
        return predicate.evaluate(with: string)
    }
    
    func isHaveNumber(_ string: String) -> Bool {
        
        let format = ".*[0-9]+.*"
        
        let predicate = NSPredicate(format:"SELF MATCHES %@", format)
        return predicate.evaluate(with: string)
    }
    
    func isHave8Char(_ string: String) -> Bool {
        
        if string.count >= self.minimumPasswordLength {
            return true
        }
        
        return false
    }
    
    func textFieldValidatorPassword(_ string: String) -> Bool {
        if string.count > 100 {
            return false
        }
        
        let emailFormat = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$"
        
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: string)
    }
    
    func submit() {
        
        if (passwordCtrl != confirmPasswordCtrl) {
            self.otpVM.showAlert = true
            self.otpVM.message = "Password dan Konfirmasi Password tidak sesuai"
            return
        }
        
        self.daftarModel.password = passwordCtrl
        self.nextRoute = true
        
    }
    
}
