//
//  DaftarScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import SwiftUI

struct DaftarScreen: View {
    
    var defaults = UserDefaults.standard
    
    @StateObject var otpVM = OtpViewModel()
    var daftarModel = DaftarBindingModel()
    
    @State private var jenisPelakuUsahaCtrl: String = ""
    @State private var noTeleponCtrl: String = ""
    
    @State private var isLoading: Bool = false
    @State private var routeOtp: Bool = false
    
    private var _listJenisPelakuUsaha = ["Orang Perseorangan"]
    
    var body: some View {
        VStack(alignment: .leading) {
            
            NavigationLink(
                destination: DaftarOtpScreen().environmentObject(daftarModel),
                isActive: self.$routeOtp,
                label: {
                    EmptyView()
                })
            
            if isLoading {
                LoadingView()
            }
            
            VStack {
                HStack(alignment: .top) {
                    Image(systemName: "info.circle.fill")
                        .foregroundColor(Color("blue"))
                        .padding(.vertical, 10)
                        .padding(.leading, 10)
                    
                    Text("Pendaftaran hanya untuk pelaku Usaha Mikro dan Kecil(UMK) orang perseorangan. Bagi jenis pelaku usaha lainnya, silahkan mendaftarkan melalui situs OSS")
                        .foregroundColor(Color("gray"))
                        .font(.caption)
                        .fontWeight(.semibold)
                        .padding(.vertical, 10)
                        .padding(.horizontal, 10)
                        .fixedSize(horizontal: false, vertical: true)
                }
            }
            .background(Color("light_blue"))
            .cornerRadius(5)
            .padding(.horizontal, 20)
            .padding(.top, 20)
            
            VStack {
                HStack(alignment: .top) {
                    Image(systemName: "info.circle.fill")
                        .foregroundColor(Color("blue"))
                        .padding(.vertical, 10)
                        .padding(.leading, 10)
                    
                    Text("Pastikan nomor telepon seluler Anda sudah terhubung dengan WhatsApp")
                        .foregroundColor(Color("gray"))
                        .font(.caption)
                        .fontWeight(.semibold)
                        .padding(.vertical, 10)
                        .padding(.horizontal, 10)
                        .fixedSize(horizontal: false, vertical: true)
                    
                    Spacer()
                }
            }
            .background(Color("light_blue"))
            .cornerRadius(5)
            .padding(.horizontal, 20)
            .padding(.bottom, 20)
            
            formNoTelepon
            
            Button(action: {
                getOtp(
                    email: " ",
                    noTelp: "+62" + noTeleponCtrl,
                    isWhatsappsend: true
                )
            }, label: {
                Text("Kirim Kode Verifikasi melalui Whatsapp")
                    .font(.subheadline)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                
            })
            .background(Color(disableButton ? "gray" : "blue"))
            .cornerRadius(8)
            .padding(.top, 10)
            .padding(.bottom, 10)
            .padding(.horizontal, 20)
            .disabled(disableButton)
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .navigationBarTitleDisplayMode(.inline)
        .onAppear() {
            self.defaults.set(AppConstants().AUTHORIZATION, forKey: defaultsKeys.keyToken)
            self.defaults.set(AppConstants().USERKEY, forKey: defaultsKeys.keyUser)
        }
        .alert(isPresented: self.$otpVM.showAlert, content: {
            return Alert(
                title: Text("Pesan"),
                message: Text("\(self.otpVM.message)"),
                dismissButton: .cancel()
            )
        })
    }
    
    var formJenisPelakuUsaha: some View {
        VStack(alignment: .leading) {
            
            Text("Jenis Pelaku Usaha")
                .fontWeight(.semibold)
                .foregroundColor(Color("gray"))
                .padding(.horizontal, 20)
            
            VStack {
                HStack {
                    VStack(alignment: .leading) {
                        Text(jenisPelakuUsahaCtrl)
                            .foregroundColor(jenisPelakuUsahaCtrl.isEmpty ? .gray : .black)
                    }
                    .padding()
                    
                    Spacer()
                    
                    Menu {
                        ForEach(self._listJenisPelakuUsaha, id: \.self) { data in
                            Button(action: {
                                self.jenisPelakuUsahaCtrl = data
                            }) {
                                Text("\(data)")
                                    .frame(width: UIScreen.main.bounds.width)
                            }
                        }
                    } label: {
                        Image(systemName: "chevron.down")
                            .padding()
                            .foregroundColor(.gray)
                    }
                }
            }
            .frame(height: 40)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color.gray, lineWidth: 1))
            .padding(.horizontal, 20)
        }
        .padding(.top, 20)
        .padding(.bottom, 10)
    }
    
    var formNoTelepon: some View {
        VStack(alignment: .leading) {
            Text("Nomor Telepon Seluler")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            HStack {
                
                HStack {
                    Image("ic_ina_flag")
                        .resizable()
                        .frame(width: 20, height: 20)
                    
                    Text("+62")
                        .font(.headline)
                        .foregroundColor(Color("gray"))
                }
                .padding()
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.gray, lineWidth: 1))
                .padding(.leading, 20)
                
                VStack (alignment: .leading) {
                    
                    HStack {
                        TextField("Nomor Telepon Seluler", text: $noTeleponCtrl, onEditingChanged: {_ in }, onCommit: {})
                            .keyboardType(.numberPad)
                            .frame(height: 40)
                            .font(Font.system(size: 14))
                            .padding(.horizontal, 10)
                            .onReceive(noTeleponCtrl.publisher.collect()) {
                                if String($0).hasPrefix("0") {
                                    self.noTeleponCtrl = String(String($0).substring(with: 1..<String($0).count).prefix(12))
                                } else {
                                    self.noTeleponCtrl = String($0.prefix(13))
                                }
                            }
                    }
                    .frame(height: 40)
                    .cornerRadius(10)
                    .background(
                        RoundedRectangle(cornerRadius: 8)
                            .strokeBorder(Color.gray, lineWidth: 1))
                }
                .padding(.trailing, 20)
            }
            
        }
        .padding(.bottom, 10)
    }
}

struct DaftarScreen_Previews: PreviewProvider {
    static var previews: some View {
        DaftarScreen()
    }
}

extension DaftarScreen {
    
    var disableButton: Bool {
        noTeleponCtrl.isEmpty
    }
    
    func getOtp(email: String, noTelp: String, isWhatsappsend: Bool) {
        self.daftarModel.email = email
        self.daftarModel.telepon = noTelp
        self.daftarModel.jenisPelakuUsaha = "02"
        self.routeOtp = true
        //        self.isLoading = true
        //        self.otpVM.getOtp(
        //            email: email,
        //            noTelepon: noTelp,
        //            sendWhatsapp: isWhatsappsend) { success in
        //
        //            if success {
        //                self.daftarModel.email = email
        //                self.daftarModel.telepon = noTelp
        //                self.daftarModel.jenisPelakuUsaha = "02"
        //
        //                self.isLoading = false
        //                self.routeOtp = true
        //            }
        //
        //            if !success {
        //                self.isLoading = false
        //            }
        //        }
    }
    
}
