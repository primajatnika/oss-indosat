//
//  DaftarViaEmailOtpScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 16/11/21.
//

import SwiftUI

struct DaftarViaEmailOtpScreen: View {
    
    @StateObject var otpVM = OtpViewModel()
    @EnvironmentObject var daftarModel: DaftarBindingModel
    
    var maxDigits: Int = 6
    @State var pin: String = ""
    
    @State private var disableButtonResend: Bool = false
    @State private var nextRoute: Bool = false
    
    @State private var timeRemainingBtn = 120
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
        VStack {
            
            NavigationLink(
                destination: DaftarCreatePasswordScreen().environmentObject(daftarModel),
                isActive: self.$nextRoute,
                label: {
                    EmptyView()
                })
            
            if (self.otpVM.isLoading) {
                LoadingView()
            }
            
            VStack {
                Text("Verifikasi")
                    .font(.headline)
                    .foregroundColor(Color("red"))
                    .padding(.bottom, 20)
                
                Text("Masukkan kode verifikasi yang dikirim ke Email Anda")
                    .font(.caption)
                    .foregroundColor(Color("gray"))
                    .fontWeight(.semibold)
                    .padding(.horizontal, 10)
                    .padding(.bottom, 10)
                    .multilineTextAlignment(.center)
                
                ZStack {
                    pinDots
                    backgroundField
                }
                
                if otpVM.isOtpInvalid {
                    VStack {
                        Text("Kode verifikasi salah")
                            .font(.caption)
                            .foregroundColor(Color("red"))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                    }
                    .padding(.horizontal, 20)
                    .padding(.top, 10)
                } else {
                    HStack {
                        Text("Mohon tunggu")
                            .font(.caption)
                            .foregroundColor(Color("gray"))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                        
                        Text("\(self.timeRemainingBtn.formatted(allowedUnits: [.second])!)")
                            .font(.caption)
                            .fontWeight(.bold)
                            .foregroundColor(Color("red"))
                            .multilineTextAlignment(.center)
                        
                        Text("detik untuk mengirim ulang")
                            .font(.caption)
                            .foregroundColor(Color("gray"))
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                    }
                    .padding(.horizontal, 20)
                    .padding(.top, 10)
                }
                
                Spacer()
                
                VStack {
                    // Button Kirim Ulang
                    Button(action: {
                        getOtp(
                            email: daftarModel.email,
                            noTelp: daftarModel.telepon,
                            isWhatsappsend: false)
                    }, label: {
                        Text("Kirim Ulang")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(Color(disableButton ? "gray" : "white"))
                            .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)

                    })
                    .background(Color(disableButton ? "light_gray" : "green"))
                    .disabled(disableButton)
                    .cornerRadius(10)
                    .padding(.horizontal, 20)
                    .padding(.top, 20)
                    .padding(.bottom, 10)
                }
                .background(Color.white)
                .cornerRadius(10, corners: [.topLeft, .topRight])
                .shadow(color: .gray, radius: 10, x: 0, y: 0)
                .mask(Rectangle().padding(.top, -20))
            }
            .padding(.top, 10)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
        .navigationBarTitle("Daftar", displayMode: .inline)
        .navigationBarItems(trailing:
                                NavigationLink(
                                    destination: BantuanScreen(),
                                    label: {
                                        Image(systemName: "questionmark.circle")
                                    })
                            )
        .alert(isPresented: self.$otpVM.showAlert, content: {
            return Alert(
                title: Text("Pesan"),
                message: Text("\(self.otpVM.message)"),
                dismissButton: .cancel()
            )
        })
        .onReceive(timer) { time in
            
            if self.timeRemainingBtn > 0 {
                self.timeRemainingBtn -= 1
            }
            
            if self.timeRemainingBtn < 1 {
                disableButtonResend = false
            }
        }
    }
    
    private var pinDots: some View {
        HStack {
            Spacer()
            ForEach(0..<maxDigits) { index in
                Text("\(self.getImageName(at: index))")
                    .font(.title)
                    .foregroundColor(.black)
                    .bold()
                    .frame(width: 40, height: 40)
                    .multilineTextAlignment(.center)
                    .cornerRadius(8)
                    .background(
                        RoundedRectangle(cornerRadius: 4)
                            .strokeBorder(Color.gray, lineWidth: 1))
            }
            Spacer()
        }
    }
    
    private var backgroundField: some View {
        let boundPin = Binding<String>(get: { self.pin }, set: { newValue in
            self.pin = newValue
            self.submitPin()
        })
        
        return TextField("", text: boundPin, onCommit: submitPin)
            .accentColor(.clear)
            .foregroundColor(.clear)
            .keyboardType(.numberPad)
    }
    
    private func submitPin() {
        if pin.count == maxDigits {
            submitOtp()
        }
        
        if pin.count > maxDigits {
            pin = String(pin.prefix(maxDigits))
            submitPin()
        }
    }
    
    private func getImageName(at index: Int) -> String {
        if index >= self.pin.count {
            return ""
        }
        
        return self.pin.digits[index].numberString
    }
    
    private func resetPin() {
        self.pin = ""
    }
}

struct DaftarViaEmailOtpScreen_Previews: PreviewProvider {
    static var previews: some View {
        DaftarViaEmailOtpScreen().environmentObject(DaftarBindingModel())
    }
}

extension DaftarViaEmailOtpScreen {
    
    var disableButton: Bool {
        timeRemainingBtn > 1
    }
    
    func submitOtp() {
//        self.nextRoute = true
        self.otpVM.verifOtp(
            email: daftarModel.email,
            otp: pin,
            verifyWhatsapp: daftarModel.telepon) { success in

            if success {
                self.daftarModel.otpToken = self.otpVM.otpToken
                self.nextRoute = true
            }

            if !success {
                resetPin()
            }

        }
    }
    
    func getOtp(email: String, noTelp: String, isWhatsappsend: Bool) {
        self.otpVM.getOtp(
            email: email,
            noTelepon: noTelp,
            sendWhatsapp: isWhatsappsend) { success in
            
            if success {
                self.timeRemainingBtn = 120
            }
            
            if !success {
                
            }
        }
    }
}
