//
//  DaftarDataDiriScreen.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import SwiftUI

struct DaftarDataDiriScreen: View {
    
    @StateObject var registerVM = RegisterViewModel()
    
    @EnvironmentObject var daftarModel: DaftarBindingModel
    
    @StateObject var mendagriVM = MendagriViewModel()
    @State var listProvince = ListMendagriResponseModel()
    @State var listKabupatenKota = ListMendagriResponseModel()
    @State var listKecamatan = ListMendagriResponseModel()
    @State var listKelurahan = ListMendagriResponseModel()
    
    @State private var nikCtrl: String = ""
    @State private var namaLengkapCtrl: String = ""
    @State private var jenisKelaminCtrl: String = ""
    @State private var tglLahirCtrl: String = ""
    @State private var alamatCtrl: String = ""
    @State private var provinceCtrl: String = ""
    @State private var kabupatenKotaCtrl: String = ""
    @State private var kecamatanCtrl: String = ""
    @State private var kelurahanCtrl: String = ""
    @State private var date: Date?
    
    private var _listJenisKelamin = ["Laki-Laki", "Perempuan"]
    
    @State private var isLoading: Bool = false
    @State private var isChecked: Bool = false
    @State private var nextRoute: Bool = false
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading) {
                
                NavigationLink(
                    destination: LoginScreen(isFromRegister: .constant(true)),
                    isActive: self.$nextRoute,
                    label: {
                        EmptyView()
                    })
                
                if isLoading {
                    LoadingView()
                }
                
                ScrollView {
                    VStack {
                        HStack(alignment: .top) {
                            Image(systemName: "info.circle.fill")
                                .foregroundColor(Color("blue"))
                                .padding(.vertical, 10)
                            
                            Text("Isilah data di bawah ini sesuai KTP Elektronik Anda")
                                .foregroundColor(Color("gray"))
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .padding(.vertical, 10)
                                .padding(.horizontal, 10)
                                .fixedSize(horizontal: false, vertical: true)
                            
                            Spacer()
                        }
                        .padding(.horizontal, 10)
                    }
                    .background(Color("light_blue"))
                    .cornerRadius(5)
                    .padding(.top, 20)
                    .padding(.bottom, 20)
                    .padding(.horizontal, 20)
                    
                    Group {
                        formNik
                        formNamaLengkap
                        formTglLahir
                        formJenisKelamin
                        formAlamat
                        formProvince
                        formKabupatenKota
                        formKecamatan
                        formKelurahan
                    }
                    
                    Button(action: { isChecked.toggle() }) {
                        HStack(alignment: .top) {
                            Image(systemName: isChecked ? "checkmark.square": "square")
                            Text("Dengan ini saya menyatakan bahwa data dan informasi yang saya isi adalah benar dan saya bertanggung jawab penuh atas data dan informasi tersebut, serta bersedia data pribadi tersebut disimpan oleh Lembaga OSS - Kementrian Investasi/BKPM untuk digunakan sesuai peruntukannya.")
                                .font(.system(size: 14))
                                .foregroundColor(Color("gray"))
                                .multilineTextAlignment(.leading)
                                .fixedSize(horizontal: false, vertical: true)
                        }
                        .padding(.horizontal, 20)
                        .padding(.bottom, 5)
                    }
                    .padding(.bottom, 20)
                }
                
                VStack {
                    // Button
                    Button(action: {
                        submitDaftar()
                    }, label: {
                        Text("Daftar")
                            .font(.subheadline)
                            .fontWeight(.bold)
                            .foregroundColor(Color(disableButton ? "gray" : "white"))
                            .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                        
                    })
                    .background(Color(disableButton ? "light_gray" : "blue"))
                    .disabled(disableButton)
                    .cornerRadius(10)
                    .padding(.horizontal, 20)
                    .padding(.top, 20)
                    .padding(.bottom, 10)
                }
                .background(Color.white)
                .cornerRadius(10, corners: [.topLeft, .topRight])
                .shadow(color: .gray, radius: 10, x: 0, y: 0)
                .mask(Rectangle().padding(.top, -20))
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
            
            if self.registerVM.showAlert {
                ModalOverlay(tapAction: { withAnimation {} })
                    .edgesIgnoringSafeArea(.all)
            }
        }
        .navigationBarTitle("Daftar", displayMode: .inline)
        .navigationBarItems(trailing:
                                NavigationLink(
                                    destination: BantuanScreen(),
                                    label: {
                                        Image(systemName: "questionmark.circle")
                                    })
                            )
        .onTapGesture() {
            UIApplication.shared.endEditing()
        }
        .onAppear() {
            getProvince()
        }
        .present(
            isPresented: self.$registerVM.showAlert,
            type: .alert,
            autohideDuration: 1000) {
            alert
        }
    }
    
    var formNik: some View {
        VStack(alignment: .leading) {
            Text("Nomor Induk Kependudukan")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Nomor Induk Kependudukan", text: $nikCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .keyboardType(.numberPad)
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                        .onReceive(nikCtrl.publisher.collect()) {
                            self.nikCtrl = String($0.prefix(16))
                        }
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.gray, lineWidth: 1))
            }
            .padding(.horizontal, 20)
            
            if (nikCtrl.count < 16) {
                Text("Karakter kurang dari 16")
                    .font(.caption)
                    .foregroundColor(Color("red"))
                    .padding(.horizontal, 30)
            }
            
            if self.registerVM.isNikAlready {
                VStack(alignment: .leading) {
                    Text("NIK sudah terdaftar. Silahkan masuk menggunakan nomor telepon atau email. \nJika Anda belum pernah mendaftar, kunjungi pusat bantuan")
                        .font(.caption)
                        .fontWeight(.semibold)
                        .foregroundColor(Color("maroon"))
                        .fixedSize(horizontal: false, vertical: true)
                    
                    HStack {
                        HStack {
                            // Button Daftar
                            Button(
                                action: {},
                                label: {
                                    Text("Pusat Bantuan")
                                        .font(.subheadline)
                                        .fontWeight(.bold)
                                        .foregroundColor(.white)
                                        .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                                    
                                })
                                .background(Color("blue"))
                                .cornerRadius(10)
                                .padding(.top, 10)
                                .padding(.bottom, 10)
                            
                            // Button Masuk
                            NavigationLink(
                                destination: LoginScreen(isFromRegister: .constant(true)),
                                label: {
                                    Text("Masuk")
                                        .font(.subheadline)
                                        .fontWeight(.bold)
                                        .foregroundColor(Color("white"))
                                        .frame(maxWidth: .infinity, minHeight: 40, maxHeight: 40)
                                })
                                .background(Color("green"))
                                .cornerRadius(10)
                                .padding(.top, 10)
                                .padding(.bottom, 10)
                            
                        }
                    }
                }
                .padding(.horizontal, 20)
                .padding(.bottom, 10)
            }
            
        }
        .padding(.bottom, 10)
    }
    
    var formNamaLengkap: some View {
        VStack(alignment: .leading) {
            Text("Nama Lengkap")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Nama Lengkap", text: $namaLengkapCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.gray, lineWidth: 1))
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formJenisKelamin: some View {
        VStack(alignment: .leading) {
            
            Text("Jenis Kelamin")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack {
                HStack {
                    VStack(alignment: .leading) {
                        Text(jenisKelaminCtrl)
                            .foregroundColor(jenisKelaminCtrl.isEmpty ? .gray : .black)
                    }
                    .padding()
                    
                    Spacer()
                    
                    Menu {
                        ForEach(self._listJenisKelamin, id: \.self) { data in
                            Button(action: {
                                self.jenisKelaminCtrl = data
                            }) {
                                Text("\(data)")
                                    .frame(width: UIScreen.main.bounds.width)
                            }
                        }
                    } label: {
                        Image(systemName: "chevron.down")
                            .padding()
                            .foregroundColor(.gray)
                    }
                }
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color.gray, lineWidth: 1))
            .padding(.horizontal, 20)
        }
        .padding(.bottom, 10)
    }
    
    var formTglLahir: some View {
        VStack(alignment: .leading) {
            Text("Tanggal Lahir")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    DatePickerTextField(placeholder: "Tanggal Lahir", date: $date)
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.gray, lineWidth: 1))
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formAlamat: some View {
        VStack(alignment: .leading) {
            Text("Alamat")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack (alignment: .leading) {
                
                HStack {
                    TextField("Alamat", text: $alamatCtrl, onEditingChanged: {_ in }, onCommit: {})
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                        .padding(.horizontal, 10)
                }
                .frame(height: 40)
                .cornerRadius(10)
                .background(
                    RoundedRectangle(cornerRadius: 8)
                        .strokeBorder(Color.gray, lineWidth: 1))
            }
            .padding(.horizontal, 20)
            
        }
        .padding(.bottom, 10)
    }
    
    var formProvince: some View {
        VStack(alignment: .leading) {
            
            Text("Provinsi")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack {
                HStack {
                    VStack(alignment: .leading) {
                        Text(provinceCtrl)
                            .foregroundColor(provinceCtrl.isEmpty ? .gray : .black)
                    }
                    .padding()
                    
                    Spacer()
                    
                    Menu {
                        ForEach(self.listProvince, id: \.regionID) { data in
                            Button(action: {
                                self.provinceCtrl = data.nama
                                getKabupatenKota(parent: data.regionID)
                            }) {
                                Text("\(data.nama)")
                                    .frame(width: UIScreen.main.bounds.width)
                            }
                        }
                    } label: {
                        Image(systemName: "chevron.down")
                            .padding()
                            .foregroundColor(.gray)
                    }
                }
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color.gray, lineWidth: 1))
            .padding(.horizontal, 20)
        }
        .padding(.bottom, 10)
    }
    
    var formKabupatenKota: some View {
        VStack(alignment: .leading) {
            
            Text("Kabupaten/Kota")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack {
                HStack {
                    VStack(alignment: .leading) {
                        Text(kabupatenKotaCtrl)
                            .foregroundColor(kabupatenKotaCtrl.isEmpty ? .gray : .black)
                    }
                    .padding()
                    
                    Spacer()
                    
                    Menu {
                        ForEach(self.listKabupatenKota, id: \.regionID) { data in
                            Button(action: {
                                self.kabupatenKotaCtrl = data.nama
                                getKecamatan(parent: data.regionID)
                            }) {
                                Text("\(data.nama)")
                                    .frame(width: UIScreen.main.bounds.width)
                            }
                        }
                    } label: {
                        Image(systemName: "chevron.down")
                            .padding()
                            .foregroundColor(.gray)
                    }
                }
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color.gray, lineWidth: 1))
            .padding(.horizontal, 20)
        }
        .padding(.bottom, 10)
    }
    
    var formKecamatan: some View {
        VStack(alignment: .leading) {
            
            Text("Kecamatan")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack {
                HStack {
                    VStack(alignment: .leading) {
                        Text(kecamatanCtrl)
                            .foregroundColor(kecamatanCtrl.isEmpty ? .gray : .black)
                    }
                    .padding()
                    
                    Spacer()
                    
                    Menu {
                        ForEach(self.listKecamatan, id: \.regionID) { data in
                            Button(action: {
                                self.kecamatanCtrl = data.nama
                                getKelurahan(parent: data.regionID)
                            }) {
                                Text("\(data.nama)")
                                    .frame(width: UIScreen.main.bounds.width)
                            }
                        }
                    } label: {
                        Image(systemName: "chevron.down")
                            .padding()
                            .foregroundColor(.gray)
                    }
                }
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color.gray, lineWidth: 1))
            .padding(.horizontal, 20)
        }
        .padding(.bottom, 10)
    }
    
    var formKelurahan: some View {
        VStack(alignment: .leading) {
            
            Text("Desa/Kelurahan")
                .foregroundColor(Color("gray"))
                .fontWeight(.semibold)
                .padding(.horizontal, 20)
            
            VStack {
                HStack {
                    VStack(alignment: .leading) {
                        Text(kelurahanCtrl)
                            .foregroundColor(kelurahanCtrl.isEmpty ? .gray : .black)
                    }
                    .padding()
                    
                    Spacer()
                    
                    Menu {
                        ForEach(self.listKelurahan, id: \.regionID) { data in
                            Button(action: {
                                self.kelurahanCtrl = data.nama
                            }) {
                                Text("\(data.nama)")
                                    .frame(width: UIScreen.main.bounds.width)
                            }
                        }
                    } label: {
                        Image(systemName: "chevron.down")
                            .padding()
                            .foregroundColor(.gray)
                    }
                }
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color.gray, lineWidth: 1))
            .padding(.horizontal, 20)
        }
        .padding(.bottom, 10)
    }
    
    var alert: some View {
        VStack {
            HStack {
                Spacer()
                Button(action: {
                    self.registerVM.showAlert = false
                }, label: {
                    Image(systemName: "xmark")
                        .foregroundColor(Color("gray"))
                })
            }
            .padding(.horizontal, 20)
            .padding(.top, 20)
            .padding(.bottom, 20)
            
            Image("ic_warning")
                .resizable()
                .frame(width: 50, height: 50)
                .padding(.bottom, 10)
            
            Text("\(self.registerVM.message)")
                .font(.subheadline)
                .fontWeight(.semibold)
                .foregroundColor(Color("gray"))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 20)
            
            Spacer()
            
            VStack {
                HStack {
                    Text("")
                    
                    Spacer()
                }
            }
            .background(Color("red"))
        }
        .frame(width: getWidth() - 35, height: 230)
        .background(Color("white"))
        .cornerRadius(20)
    }
}

struct DaftarDataDiriScreen_Previews: PreviewProvider {
    static var previews: some View {
        DaftarDataDiriScreen()
    }
}

extension DaftarDataDiriScreen {
    
    var disableButton: Bool {
        nikCtrl.count < 16 || jenisKelaminCtrl.isEmpty || alamatCtrl.isEmpty || provinceCtrl.isEmpty || kabupatenKotaCtrl.isEmpty || kecamatanCtrl.isEmpty || kelurahanCtrl.isEmpty || !isChecked || self.isLoading
    }
    
    func getProvince() {
        self.isLoading = true
        self.mendagriVM.getProvince { success in
            
            if success {
                self.isLoading = false
                self.listProvince = self.mendagriVM.provinceList
            }
            
            if !success {
                self.isLoading = false
            }
            
        }
    }
    
    func getKabupatenKota(parent: String) {
        self.isLoading = true
        self.mendagriVM.getKabupatenKota(parent: parent) { success in
            
            if success {
                self.isLoading = false
                self.listKabupatenKota = self.mendagriVM.kabupatenKotaList
            }
            
            if !success {
                self.isLoading = false
            }
            
        }
    }
    
    func getKecamatan(parent: String) {
        self.isLoading = true
        self.mendagriVM.getKecamatan(parent: parent) { success in
            
            if success {
                self.isLoading = false
                self.listKecamatan = self.mendagriVM.kecamatanList
            }
            
            if !success {
                self.isLoading = false
            }
            
        }
    }
    
    func getKelurahan(parent: String) {
        self.isLoading = true
        self.mendagriVM.getKelurahan(parent: parent) { success in
            
            if success {
                self.isLoading = false
                self.listKelurahan = self.mendagriVM.kelurahanList
            }
            
            if !success {
                self.isLoading = false
            }
            
        }
    }
    
    func submitDaftar() {
        let formatter = DateFormatter()
        let gender = jenisKelaminCtrl == "Perempuan" ? "P" : "L"
        
        self.daftarModel.nik = nikCtrl
        self.daftarModel.namaLengkap = namaLengkapCtrl
        self.daftarModel.jenisKelamin = gender
        self.daftarModel.tanggalLahir = formatter.string(from: date!)
        self.daftarModel.alamat = alamatCtrl
        self.daftarModel.provinsi = provinceCtrl
        self.daftarModel.kabupatenKota = kabupatenKotaCtrl
        self.daftarModel.kecamatan = kecamatanCtrl
        self.daftarModel.kelurahan = kelurahanCtrl
        self.daftarModel.flagUmk = "Y"
        self.daftarModel.jenisIdentitas = "01"
        self.daftarModel.noKk = ""
        self.daftarModel.daerahId = "3174091005"
        self.daftarModel.rtrw = ""
        self.daftarModel.statusPerkawinan = ""
        self.daftarModel.kewarganegaraan = "ID"
        self.daftarModel.agama = ""
        
        self.isLoading = true
        self.registerVM.registerPelakuUsaha(dataRegister: self.daftarModel) { success in
            
            if success {
                self.isLoading = false
                self.nextRoute = true
            }
            
            if !success {
                self.isLoading = false
            }
            
        }
    }
    
}
