//
//  LoginService.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import Foundation

class LoginService {
    
    private init() {}
    static let shared = LoginService()
    
    // MARK:- POST
    func postLogin(
        username: String,
        password: String,
        completion: @escaping(Result<AuthLoginResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlAuthLogin() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        let body: [String: Any] = [
            "username": username,
            "password": password
        ]
        
        var request = URLRequest(url)
        request.httpMethod = "POST"
        
        do {
            // MARK : serialize model data
            let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
            _ = String(data: jsonData, encoding: String.Encoding.ascii)
            
            request.httpBody = jsonData
        } catch let error {
            print(error.localizedDescription)
            completion(.failure(.parser(string: "ERROR PARSE BODY")))
        }
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(AuthLoginResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(AuthLoginResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
}
