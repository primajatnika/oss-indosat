//
//  RegisterService.swift
//  exmpl
//
//  Created by Prima Jatnika on 17/11/21.
//

import Foundation

class RegisterService {
    
    private init() {}
    static let shared = RegisterService()
    
    // MARK:- POST
    func postRegistrasiPelakuUsaha(
        dataRegister: DaftarBindingModel,
        completion: @escaping(Result<StatusOtpResponseModel, ErrorResult>) -> Void) {
            
            guard let url = URL.urlRegisterPelakuUsaha() else {
                return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
            }
            
            print(dataRegister.otpToken)
            print(dataRegister.flagUmk)
            print(dataRegister.jenisPelakuUsaha)
            print(dataRegister.jenisIdentitas)
            print(dataRegister.telepon)
            print(dataRegister.email)
            print(dataRegister.tanggalLahir)
            
            let body: [String: Any] = [
                "registrasi": [
                    "flag_umk": dataRegister.flagUmk,
                    "jenis_pelaku_usaha": dataRegister.jenisPelakuUsaha,
                    "jenis_identitas": dataRegister.jenisIdentitas,
                    "no_kk": dataRegister.noKk,
                    "daerah_id": dataRegister.daerahId,
                    "rtrw": dataRegister.rtrw,
                    "status_perkawinan": dataRegister.statusPerkawinan,
                    "pekerjaan": dataRegister.pekerjaan,
                    "kewarganegaraan": dataRegister.kewarganegaraan,
                    "nomor_identitas": dataRegister.nik,
                    "nama": dataRegister.namaLengkap,
                    "jenis_kelamin": dataRegister.jenisKelamin,
                    "tempat_lahir": dataRegister.tempatLahir,
                    "alamat": dataRegister.alamat,
                    "agama": dataRegister.agama,
                    "tanggal_lahir": dataRegister.tanggalLahir,
                    "telp": dataRegister.telepon,
                    "email": dataRegister.email,
                    "newPassword": dataRegister.password,
                    "confirmPassword": dataRegister.password,
                    "token_otp": "Bearer " + dataRegister.otpToken
                ]
            ]
            
            let jsonData = try? JSONSerialization.data(withJSONObject: body)
            
            var request = URLRequest(url)
            request.httpMethod = "POST"
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = jsonData
            print("URL ABSOLUTE : \(url.absoluteURL)")

            URLSession.shared.dataTask(with: request) { data, response, error in
                
                print(data, String(data: data!, encoding: .utf8) ?? "*unknown encoding*")
                print("response: \(String(describing: response))")

                if let rc = response as? HTTPURLResponse {
                    print("RC: \(rc.statusCode)")
                    
                    if (rc.statusCode == 200) {
                        if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                            completion(Result.success(value))
                        }
                    } else {
                        if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                            completion(Result.success(value))
                        }
                    }
                    
                    if (rc.statusCode == 504) {
                        completion(Result.failure(.code(code: 504)))
                    }
                }
            }.resume()
            
        }
}
