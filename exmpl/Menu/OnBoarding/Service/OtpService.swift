//
//  OtpService.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import Foundation

class OtpService {
    
    private init() {}
    static let shared = OtpService()
    
    // MARK:- POST
    func postOtp(
        email: String,
        noTelepon: String,
        sendWhatsapp: Bool,
        completion: @escaping(Result<StatusOtpResponseModel, ErrorResult>) -> Void) {
            
            guard let url = URL.urlGetOtp() else {
                return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
            }
            
            let body: [String: Any] = [
                "email": email,
                "no_telp": noTelepon,
                "sendWhatsapp": sendWhatsapp
            ]
            
            var request = URLRequest(url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            do {
                // MARK : serialize model data
                let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
                _ = String(data: jsonData, encoding: String.Encoding.ascii)
                
                request.httpBody = jsonData
            } catch let error {
                print(error.localizedDescription)
                completion(.failure(.parser(string: "ERROR PARSE BODY")))
            }
            
            URLSession.shared.dataTask(with: request) { data, response, error in
                
                print("response: \(String(describing: response))")
                
                if error == nil {
                    let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    if let json = jsonData as? [String: Any] {
                        print(json)
                    }
                }
                
                if let rc = response as? HTTPURLResponse {
                    print("RC: \(rc.statusCode)")
                    
                    if (rc.statusCode == 200) {
                        if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                            completion(Result.success(value))
                        }
                    } else {
                        if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                            completion(Result.success(value))
                        }
                    }
                }
            }.resume()
            
        }
    
    // MARK:- POST
    func postVerifOtp(
        email: String,
        otp: String,
        verifyWhatsapp: String,
        completion: @escaping(Result<StatusOtpResponseModel, ErrorResult>) -> Void) {
            
            guard let url = URL.urlVerifOtp() else {
                return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
            }
            
            let body: [String: Any] = [
                "email": email,
                "otp": otp,
                "verifyWhatsapp": verifyWhatsapp
            ]
            
            var request = URLRequest(url)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            
            do {
                // MARK : serialize model data
                let jsonData = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
                _ = String(data: jsonData, encoding: String.Encoding.ascii)
                
                request.httpBody = jsonData
            } catch let error {
                print(error.localizedDescription)
                completion(.failure(.parser(string: "ERROR PARSE BODY")))
            }
            
            URLSession.shared.dataTask(with: request) { data, response, error in
                
                print("response: \(String(describing: response))")
                
                if error == nil {
                    let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                    if let json = jsonData as? [String: Any] {
                        print(json)
                    }
                }
                
                if let rc = response as? HTTPURLResponse {
                    print("RC: \(rc.statusCode)")
                    
                    if (rc.statusCode == 200) {
                        if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                            completion(Result.success(value))
                        }
                    } else {
                        if let value = try? JSONDecoder().decode(StatusOtpResponseModel.self, from: data!) {
                            completion(Result.success(value))
                        }
                    }
                }
            }.resume()
            
        }
}
