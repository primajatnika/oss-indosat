//
//  MendagriService.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import Foundation

class MendagriService {
    
    private init() {}
    static let shared = MendagriService()
    
    // MARK:- GET
    func getProvince(completion: @escaping(Result<ListMendagriResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlGetProvince() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        var request = URLRequest(url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListMendagriResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListMendagriResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- GET
    func getKabupatenKota(parent: String, completion: @escaping(Result<ListMendagriResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlGetKabupatenKota() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        var request = URLRequest(url.appending("parent", value: parent))
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListMendagriResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListMendagriResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- GET
    func getKecamatan(parent: String, completion: @escaping(Result<ListMendagriResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlGetKecamatan() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        var request = URLRequest(url.appending("parent", value: parent))
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListMendagriResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListMendagriResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
    
    // MARK:- GET
    func getKelurahan(parent: String, completion: @escaping(Result<ListMendagriResponseModel, ErrorResult>) -> Void) {
        
        guard let url = URL.urlGetKelurahan() else {
            return completion(Result.failure(ErrorResult.network(string: "BAD URL")))
        }
        
        var request = URLRequest(url.appending("parent", value: parent))
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            
            print("response: \(String(describing: response))")
            
            if error == nil {
                let jsonData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                if let json = jsonData as? [String: Any] {
                    print(json)
                }
            }
            
            if let rc = response as? HTTPURLResponse {
                print("RC: \(rc.statusCode)")
                
                if (rc.statusCode == 200) {
                    if let value = try? JSONDecoder().decode(ListMendagriResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                } else {
                    if let value = try? JSONDecoder().decode(ListMendagriResponseModel.self, from: data!) {
                        completion(Result.success(value))
                    }
                }
            }
        }.resume()
        
    }
}
