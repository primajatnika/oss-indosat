//
//  LoginViewModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import Foundation
import Combine

class LoginViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    @Published var message: String = ""
    
    @Published var access_token: String = ""
}

extension LoginViewModel {
    
    func login(username: String, password: String, completion: @escaping (Bool) -> Void) {
        
        print("Username | Password")
        print(username, password)
        self.isLoading = true
        
        LoginService.shared.postLogin(username: username, password: password) { result in
            
            switch result {
            case .success(let response):
                
                if (response.status == 200) {
                    DispatchQueue.main.async {
                        self.isLoading = false
                    }
                    
                    self.access_token = response.data?.accessToken ?? ""
                    print(self.access_token)
                    
                    completion(true)
                } else {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.showAlert = true
                        self.message = response.message
                    }
                    
                    completion(false)
                }
                
                break
            
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
            
            }
            
        }
    }
    
}
