//
//  RegisterViewModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 17/11/21.
//

import Foundation

class RegisterViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    @Published var message: String = ""
    
    @Published var isNikAlready: Bool = false
}

extension RegisterViewModel {
    
    func registerPelakuUsaha(dataRegister: DaftarBindingModel, completion: @escaping (Bool) -> Void) {
        
        self.isLoading = true
        
        RegisterService.shared.postRegistrasiPelakuUsaha(dataRegister: dataRegister) { result in
            
            switch result {
            case .success(let response):
                
                if (response.kode == 200) {
                    DispatchQueue.main.async {
                        self.isLoading = false
                    }
                    
                    completion(true)
                } else if (response.kode == 406) {
                    DispatchQueue.main.async {
                        self.isLoading = false
                    }
                    
                    self.isNikAlready = true
                    
                    completion(false)
                } else {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.showAlert = true
                        self.message = response.keterangan ?? ""
                    }
                    
                    completion(false)
                }
                
                break
            
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
            
            }
        }
    }
    
}
