//
//  MendagriViewModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import Foundation

class MendagriViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    @Published var message: String = ""
    
    @Published var provinceList = ListMendagriResponseModel()
    @Published var kabupatenKotaList = ListMendagriResponseModel()
    @Published var kecamatanList = ListMendagriResponseModel()
    @Published var kelurahanList = ListMendagriResponseModel()
}

extension MendagriViewModel {
    
    func getProvince(completion: @escaping (Bool) -> Void) {
        self.isLoading = true
        
        MendagriService.shared.getProvince { result in
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.provinceList = response
                completion(true)
                
                break
                
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
                
            }
        }
    }
    
    func getKabupatenKota(parent: String, completion: @escaping (Bool) -> Void) {
        self.isLoading = true
        
        MendagriService.shared.getKabupatenKota(parent: parent) { result in
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.kabupatenKotaList = response
                completion(true)
                
                break
                
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
                
            }
        }
    }
    
    func getKecamatan(parent: String, completion: @escaping (Bool) -> Void) {
        self.isLoading = true
        
        MendagriService.shared.getKecamatan(parent: parent) { result in
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.kecamatanList = response
                completion(true)
                
                break
                
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
                
            }
        }
    }
    
    func getKelurahan(parent: String, completion: @escaping (Bool) -> Void) {
        self.isLoading = true
        
        MendagriService.shared.getKelurahan(parent: parent) { result in
            switch result {
            case .success(let response):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                }
                
                self.kelurahanList = response
                completion(true)
                
                break
                
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
                
            }
        }
    }
    
}
