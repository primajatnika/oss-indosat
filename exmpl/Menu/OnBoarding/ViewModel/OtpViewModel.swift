//
//  OtpViewModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import Foundation

class OtpViewModel: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var showAlert: Bool = false
    @Published var message: String = ""
    
    @Published var otpToken: String = ""
    @Published var isOtpInvalid: Bool = false
}

extension OtpViewModel {
    
    func getOtp(email: String, noTelepon: String, sendWhatsapp: Bool, completion: @escaping (Bool) -> Void) {
        
        print("email | notelepon | sendWhatsapp")
        print(email, noTelepon, sendWhatsapp)
        self.isLoading = true
        
        OtpService.shared.postOtp(email: email, noTelepon: noTelepon, sendWhatsapp: sendWhatsapp) { result in
            
            switch result {
            case .success(let response):
                
                if (response.kode == 200) {
                    DispatchQueue.main.async {
                        self.isLoading = false
                    }
                    
                    completion(true)
                } else {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.showAlert = true
                        self.message = response.desc ?? ""
                    }
                    
                    completion(false)
                }
                
                break
                
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
                
            }
        }
    }
    
    func verifOtp(email: String, otp: String, verifyWhatsapp: String, completion: @escaping (Bool) -> Void) {
        
        print("email | otp | verifyWhatsapp")
        print(email, otp, verifyWhatsapp)
        self.isLoading = true
        
        OtpService.shared.postVerifOtp(email: email, otp: otp, verifyWhatsapp: verifyWhatsapp) { result in
            
            switch result {
            case .success(let response):
                
                if (response.kode == 200) {
                    DispatchQueue.main.async {
                        self.isLoading = false
                    }
                    
                    self.otpToken = response.data?.newToken ?? ""
                    
                    completion(true)
                } else {
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.showAlert = true
                        self.message = response.desc ?? ""
                        self.isOtpInvalid = true
                    }
                    
                    completion(false)
                }
                
                break
                
            case .failure(let error):
                
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.showAlert = true
                    self.message = "\(error)"
                }
                
                print(error)
                completion(false)
                break
                
            }
        }
        
    }
}
