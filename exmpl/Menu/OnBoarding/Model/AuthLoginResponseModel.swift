//
//  AuthLoginResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import Foundation

// MARK: - AuthLoginResponseModel
struct AuthLoginResponseModel: Codable {
    let status: Int
    let message: String
    let data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    let accessToken, expiredAToken, refreshToken, expiredRToken: String
    let uri: URI

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case expiredAToken = "expired_aToken"
        case refreshToken = "refresh_token"
        case expiredRToken = "expired_rToken"
        case uri
    }
}

// MARK: - URI
struct URI: Codable {
    let validateToken, updateToken, introspecToken, revokeToken: String
    let userinfoToken: String
    let redirect: String
    let getMenu: String

    enum CodingKeys: String, CodingKey {
        case validateToken = "validate_token"
        case updateToken = "update_token"
        case introspecToken = "introspec_token"
        case revokeToken = "revoke_token"
        case userinfoToken = "userinfo_token"
        case redirect, getMenu
    }
}

