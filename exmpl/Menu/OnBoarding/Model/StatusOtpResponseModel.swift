//
//  StatusOtpResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import Foundation

// MARK: - StatusOtpResponseModel
struct StatusOtpResponseModel: Codable {
    let kode: Int
    let desc: String?
    let keterangan: String?
    let data: DataToken?
}

// MARK: - DataClass
struct DataToken: Codable {
    let newToken: String?
    let id_profile: Int?
}

