//
//  DaftarBindingModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import Foundation

class DaftarBindingModel: ObservableObject {
    @Published var otpToken = ""
    @Published var namaLengkap = ""
    @Published var email = ""
    @Published var telepon = ""
    @Published var jenisPelakuUsaha = ""
    @Published var jenisIdentitas = ""
    @Published var password = ""
    @Published var nik = ""
    @Published var jenisKelamin = ""
    @Published var tanggalLahir = ""
    @Published var agama = ""
    @Published var alamat = ""
    @Published var provinsi = ""
    @Published var provinsiId = ""
    @Published var kabupatenKota = ""
    @Published var kabupatenKotaId = ""
    @Published var kecamatan = ""
    @Published var kecamatanId = ""
    @Published var kelurahan = ""
    @Published var kelurahanId = ""
    @Published var daerahId = ""
    @Published var flagUmk = ""
    @Published var noKk = ""
    @Published var rtrw = ""
    @Published var statusPerkawinan = ""
    @Published var pekerjaan = ""
    @Published var kewarganegaraan = ""
    @Published var tempatLahir = ""
    
    static let shared = DaftarBindingModel()
}
