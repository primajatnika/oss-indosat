//
//  ListMendagriResponseModel.swift
//  exmpl
//
//  Created by Prima Jatnika on 15/11/21.
//

import Foundation

// MARK: - ListMendagriResponseModelElement
struct ListMendagriResponseModelElement: Codable {
    let regionID, nama: String

    enum CodingKeys: String, CodingKey {
        case regionID = "region_id"
        case nama
    }
}

typealias ListMendagriResponseModel = [ListMendagriResponseModelElement]
