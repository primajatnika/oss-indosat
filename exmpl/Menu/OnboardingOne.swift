//
//  OnboardingOne.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI

struct OnboardingOne: View {
    
    let onNext: ()-> Void
    
    var body: some View {
        HStack {
            
            Image(systemName: "chevron.left")
                .padding(.leading, 15)
                .foregroundColor(Color("blue"))
                .hidden()
            
            VStack(spacing: 20) {
                
                Image("slide1")
                
                Text("Online Single\nSubmission (OSS)")
                    .font(.title)
                    .foregroundColor(Color("red"))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 20)
                
                Text("Menyediakan layanan perizinan berusaha untuk pelaku Usaha Mikro dan pelacakan pemrosesan perizinan berusaha untuk semua\njenis pelaku usaha.")
                    .font(.subheadline)
                    .foregroundColor(Color("gray"))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 20)
                
            }
            
            Button(action: onNext, label: {
                Image(systemName: "chevron.right")
                    .padding(.trailing, 15)
                    .foregroundColor(Color("gray"))
            })
        }
    }
}

struct OnboardingOne_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingOne(onNext: {})
    }
}
