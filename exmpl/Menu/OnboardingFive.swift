//
//  OnboardingFive.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI

struct OnboardingFive: View {
    
    let onPrev: ()-> Void
    
    var body: some View {
        HStack {
            
            Button(action: onPrev, label: {
                Image(systemName: "chevron.left")
                    .padding(.leading, 15)
                    .foregroundColor(Color("gray"))
            })
            
            VStack(spacing: 20) {
                
                Image("slide5")
                
                Text("Bagi pelaku usaha yang sudah memiliki akun dan NIB di Sistem OSS 1.1,\nsilahkan masuk dengan username dan password lama. Selanjutnya lakukan penggantian akun.")
                    .font(.subheadline)
                    .foregroundColor(Color("gray"))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 20)

            }
            
            Image(systemName: "chevron.right")
                .padding(.trailing, 15)
                .foregroundColor(Color.black.opacity(0.7))
                .hidden()
        }
    }
}

struct OnboardingFive_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingFive(onPrev: {})
    }
}
