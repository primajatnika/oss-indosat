//
//  OnboardingThree.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI

struct OnboardingThree: View {
    
    let onNext: ()-> Void
    let onPrev: ()-> Void
    
    var body: some View {
        
        HStack {
            Button(action: onPrev, label: {
                Image(systemName: "chevron.left")
                    .padding(.leading, 15)
                    .foregroundColor(Color("gray"))
            })
            
            VStack(spacing: 20) {
                
                Image("slide3")
                
                Text("Melacak Pemrosesan")
                    .font(.title)
                    .foregroundColor(Color("red"))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 20)
                
                Text("Semua pelaku usaha bisa melihat perkembangan pemrosesan\nperizinan berusaha.")
                    .font(.subheadline)
                    .foregroundColor(Color("gray"))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 20)
                
            }
            
            Button(action: onNext, label: {
                Image(systemName: "chevron.right")
                    .padding(.trailing, 15)
                    .foregroundColor(Color("gray"))
            })
        }
    }
}

struct OnboardingThree_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingThree(onNext: {}, onPrev: {})
    }
}
