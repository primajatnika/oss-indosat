//
//  LabelTextFieldWithIcon.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI

struct LabelTextFieldWithIcon: View {
    
    @Binding var value: String
    var placeHolder: String
    var iconName: String
    let onEditingChanged: (Bool)->Void
    let onCommit: ()->Void
    
    var isValid: Bool = true
    
    var body: some View {
        VStack (alignment: .leading) {
            
            HStack {
                Image(systemName: iconName)
                    .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 0))
                    .foregroundColor(.gray)
                
                TextField(placeHolder, text: $value,onEditingChanged: onEditingChanged, onCommit: onCommit)
                    .frame(height: 40)
                    .font(Font.system(size: 14))
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color("gray"), lineWidth: 1))
        }
        .padding(.horizontal, 20)
    }
}

struct LabelTextFieldWithIcon_Previews: PreviewProvider {
    static var previews: some View {
        LabelTextFieldWithIcon(value: Binding.constant(""), placeHolder: "Text Field Name", iconName: "person") { (Bool) in
            print("on edit")
        } onCommit: {
            print("on commit")
        }
    }
}
