//
//  Collapsible.swift
//  exmpl
//
//  Created by Prima Jatnika on 23/11/21.
//

import Foundation
import SwiftUI

struct Collapsible<Content: View>: View {
    @State var label: () -> Text
    @State var content: () -> Content
    
    @State private var collapsed: Bool = true
    
    var body: some View {
        VStack {
            Button(
                action: { self.collapsed.toggle() },
                label: {
                    HStack {
                        self.label()
                            .font(.subheadline)
                            .fontWeight(.semibold)
                            .foregroundColor(Color("gray"))
                            .fixedSize(horizontal: false, vertical: true)
                            .padding()
                        
                        Spacer()
                        
                        Image(systemName: self.collapsed ? "chevron.down" : "chevron.up")
                            .foregroundColor(.secondary)
                            .padding(.trailing, 10)
                    }
                    .background(Color.white)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke(Color(.white), lineWidth: 1)
                    )
                    .cornerRadius(10)
                    .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
                }
            )
            .buttonStyle(PlainButtonStyle())
            
            VStack {
                self.content()
            }
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: collapsed ? 0 : .none)
            .clipped()
        }
    }
}
