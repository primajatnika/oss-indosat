//
//  ModalOverlay.swift
//  exmpl
//
//  Created by Prima Jatnika on 17/11/21.
//

import Foundation
import SwiftUI

struct ModalOverlay: View {
    var color = Color.black.opacity(0.6)
    var tapAction: (() -> Void)? = nil
    
    var body: some View {
        color.onTapGesture { self.tapAction?() }
    }
}
