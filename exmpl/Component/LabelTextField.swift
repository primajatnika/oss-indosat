//
//  LabelTextField.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI

struct LabelTextField: View {
    
    @Binding var value: String
    var placeHolder: String
    let onEditingChanged: (Bool)->Void
    let onCommit: ()->Void
    
    var isValid: Bool = true
    
    var body: some View {
        VStack (alignment: .leading) {
            
            HStack {
                TextField(placeHolder, text: $value,onEditingChanged: onEditingChanged, onCommit: onCommit)
                    .frame(height: 40)
                    .font(Font.system(size: 14))
                    .padding(.horizontal, 10)
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color("gray"), lineWidth: 1))
        }
        .padding(.horizontal, 20)
    }
}

struct LabelTextField_Previews: PreviewProvider {
    static var previews: some View {
        LabelTextField(value: Binding.constant(""), placeHolder: "Text Field Name") { (Bool) in
            print("on edit")
        } onCommit: {
            print("on commit")
        }
    }
}
