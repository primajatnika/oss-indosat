//
//  LabelPasswordFieldWithIcon.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI

struct LabelPasswordFieldWithIcon: View {
    
    @Binding var value: String
    var placeHolder: String
    var iconName: String
    let onEditingChanged: (Bool)->Void
    let onCommit: ()->Void
    let onForgotPassword: ()-> Void
    
    @State var showPassword: Bool = false
    
    var body: some View {
        VStack (alignment: .leading) {
            
            HStack {
                Image(systemName: iconName)
                    .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 0))
                    .foregroundColor(.gray)
                
                if showPassword {
                    TextField(placeHolder, text: $value, onEditingChanged: onEditingChanged, onCommit: onCommit)
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                } else {
                    SecureField(placeHolder, text: $value, onCommit: onCommit)
                        .frame(height: 40)
                        .font(Font.system(size: 14))
                }
                
                Button(action: {
                    showPassword.toggle()
                }, label: {
                    Image(systemName: showPassword ? "eye.fill" : "eye.slash")
                        .foregroundColor(Color.gray)
                })
                    .padding(.horizontal)
                
            }
            .frame(height: 40)
            .cornerRadius(10)
            .background(
                RoundedRectangle(cornerRadius: 8)
                    .strokeBorder(Color("gray"), lineWidth: 1))
        }
        .padding(.horizontal, 20)
    }
}

struct LabelPasswordFieldWithIcon_Previews: PreviewProvider {
    static var previews: some View {
        LabelPasswordFieldWithIcon(value: .constant(""), placeHolder: "Demo", iconName: "lock", onEditingChanged: { (Bool) in }, onCommit: {}, onForgotPassword: {})
    }
}
