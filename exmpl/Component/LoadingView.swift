//
//  LoadingView.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI
import Indicators

struct LoadingView: View {
    var body: some View {
        LinearWaitingIndicator()
            .animated(true)
            .foregroundColor(.green)
            .frame(height: 1)
    }
}

struct LoadingView_Previews: PreviewProvider {
    static var previews: some View {
        LoadingView()
    }
}
