//
//  RoundedIconWithLabel.swift
//  exmpl
//
//  Created by Prima Jatnika on 12/11/21.
//

import SwiftUI

struct RoundedIconWithLabel: View {
    
    var imageName: String
    var label: String
    
    var body: some View {
        VStack {
            
            Image(imageName)
                .resizable()
                .frame(width: 50, height: 50)
            
            Text(label)
                .font(.system(size: 12))
                .foregroundColor(Color("gray"))
                .multilineTextAlignment(.center)
            
        }
        .frame(width: 90, height: 100)
        .background(Color.white)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.white), lineWidth: 1)
        )
        .cornerRadius(10)
        .shadow(color: Color(.gray).opacity(0.15), radius: 5, x: 0, y: 3)
    }
}

struct RoundedIconWithLabel_Previews: PreviewProvider {
    static var previews: some View {
        RoundedIconWithLabel(imageName: "ic_perizinan", label: "Perizinan")
    }
}
